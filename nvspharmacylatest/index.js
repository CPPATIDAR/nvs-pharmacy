import React,{ Component } from 'react';
import {YellowBox,AppRegistry} from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
YellowBox.ignoreWarnings([
  "Warning: isMounted(...) is deprecated",
  "Module RCTImageLoader",
  "Warning: Failed prop type",
  "TabNavigator is deprecated",
  "Warning: Failed child context type",'Class RCTCxxModule',
  "Warning: Can't call setState (or forceUpdate) on an unmounted component. This is a no-op, but it indicates a memory leak in your application. To fix, cancel all subscriptions and asynchronous tasks in the componentWillUnmount method." 
]);
import SlideMenu from './application/config/SideMenu'
import {Dimensions} from 'react-native';
import {DrawerNavigator} from 'react-navigation'
import { Drawer } from './application/config/route';

const drawer = DrawerNavigator({
  Drawer: {
    screen: Drawer,
    }
  }, {
    contentComponent: SlideMenu,
    drawerPosition: 'right',
    drawerWidth: Dimensions.get('window').width,  
});

AppRegistry.registerComponent('nvspharmacy', () => drawer);