package com.nvspharmacy;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class DataResponse {
    @SerializedName("status")
    @Expose
    public int status;
    @SerializedName("message")
    @Expose
    public String message;
}
