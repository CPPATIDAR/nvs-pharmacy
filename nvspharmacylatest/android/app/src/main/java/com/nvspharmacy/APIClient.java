package com.nvspharmacy;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {
    private static Retrofit retrofit = null;

    public static Retrofit getClient() {

        retrofit = new Retrofit.Builder()
                .baseUrl("https://nvspharmacy.co.uk/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }
}
