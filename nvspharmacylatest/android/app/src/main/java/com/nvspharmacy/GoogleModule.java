package com.nvspharmacy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.identity.intents.model.UserAddress;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.CardInfo;
import com.google.android.gms.wallet.CardRequirements;
import com.google.android.gms.wallet.IsReadyToPayRequest;
import com.google.android.gms.wallet.PaymentData;
import com.google.android.gms.wallet.PaymentDataRequest;
import com.google.android.gms.wallet.PaymentMethodTokenizationParameters;
import com.google.android.gms.wallet.PaymentsClient;
import com.google.android.gms.wallet.TransactionInfo;
import com.google.android.gms.wallet.Wallet;
import com.google.android.gms.wallet.WalletConstants;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Optional;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class GoogleModule extends ReactContextBaseJavaModule {
    private final PaymentsClient mPaymentsClient;
    /** A constant integer you define to track a request for payment data activity */
    private static final int LOAD_PAYMENT_DATA_REQUEST_CODE = 42;
    private Gson gson;

    public GoogleModule(ReactApplicationContext reactContext) {
        super(reactContext);
        reactContext = reactContext;
        // initialize a Google Pay API client for an environment suitable for testing
        mPaymentsClient = Wallet.getPaymentsClient(
                        reactContext,
                        new Wallet.WalletOptions.Builder()
                                .setEnvironment(WalletConstants.ENVIRONMENT_TEST)
                                .build());

        reactContext.addActivityEventListener(mActivityEventListener);

    }

    private void possiblyShowGooglePayButton() {
        final Optional<JSONObject> isReadyToPayJson = GooglePay.getIsReadyToPayRequest();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (!isReadyToPayJson.isPresent()) {
                return;
            }
        }
        try {
           IsReadyToPayRequest request = IsReadyToPayRequest.newBuilder()
                    .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_CARD)
                    .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD)
                    .build();
            if (request == null) {
                return;
            }
            Task<Boolean> task = mPaymentsClient.isReadyToPay(request);
            Log.d("task====",task+"");
            task.addOnCompleteListener(
                    new OnCompleteListener<Boolean>() {
                        @Override
                        public void onComplete(@NonNull Task<Boolean> task) {
                            try {
                                boolean result = task.getResult(ApiException.class);
                                Log.d("result====",result+"");
                                if(result)
                                {
                                    requestPayment();
                                }
                            } catch (ApiException exception) {
                                // handle developer errors
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public String getName() {
        return "GoogleModule";
    }

    @ReactMethod
    public void googlemodule() {
        possiblyShowGooglePayButton();
    }

    private PaymentMethodTokenizationParameters createTokenizationParameters() {
        return PaymentMethodTokenizationParameters.newBuilder()
                .setPaymentMethodTokenizationType(WalletConstants.PAYMENT_METHOD_TOKENIZATION_TYPE_PAYMENT_GATEWAY)
                .addParameter("gateway", "worldpay")
                .addParameter("gatewayMerchantId","218edf02-94cf-44de-a576-06760baa7033")
                .build();
    }

    public void requestPayment() {
        Optional<JSONObject> paymentDataRequestJson = GooglePay.getPaymentDataRequest();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (!paymentDataRequestJson.isPresent()) {
                return;
            }
        }
        try {
         PaymentDataRequest request = createPaymentDataRequest();
            if (request != null)
            {AutoResolveHelper.resolveTask(mPaymentsClient.loadPaymentData(request),getCurrentActivity(),LOAD_PAYMENT_DATA_REQUEST_CODE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private PaymentDataRequest createPaymentDataRequest() {
        PaymentDataRequest.Builder request =
                PaymentDataRequest.newBuilder()
                        .setTransactionInfo(
                                TransactionInfo.newBuilder()
                                        .setTotalPriceStatus(WalletConstants.TOTAL_PRICE_STATUS_FINAL)
                                        .setTotalPrice("10.00")
                                        .setCurrencyCode("USD")
                                        .build())
                        .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_CARD)
                        .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD)
                        .setCardRequirements(
                                CardRequirements.newBuilder()
                                        .addAllowedCardNetworks(Arrays.asList(
                                                WalletConstants.CARD_NETWORK_AMEX,
                                                WalletConstants.CARD_NETWORK_DISCOVER,
                                                WalletConstants.CARD_NETWORK_VISA,
                                                WalletConstants.CARD_NETWORK_MASTERCARD))
                                        .build());

        request.setPaymentMethodTokenizationParameters(createTokenizationParameters());
        return request.build();
    }
    private final ActivityEventListener mActivityEventListener = new BaseActivityEventListener() {
        @Override
        public void onActivityResult(final Activity activity, int requestCode, int resultCode, Intent data) {
            Log.d("requestCode==",requestCode+"==="+resultCode+"==="+LOAD_PAYMENT_DATA_REQUEST_CODE);
            // retrieve the error code, if available
            switch (requestCode) {
                // value passed in AutoResolveHelper
                case LOAD_PAYMENT_DATA_REQUEST_CODE:
                    switch (resultCode) {
                        case Activity.RESULT_OK:
                            PaymentData paymentData = PaymentData.getFromIntent(data);
                            // You can get some data on the user's card, such as the brand and last 4 digits
                            CardInfo info = paymentData.getCardInfo();
                            // You can also pull the user address from the PaymentData object.
                            UserAddress address = paymentData.getShippingAddress();
                            // This is the raw JSON string version of your Stripe token.
                            String rawToken = paymentData.getPaymentMethodToken().getToken();
                            String json = paymentData.toString();
                            Call<DataResponse> call = APIClient.getClient().create(ApiInterface.class).saveDataResponse(rawToken);
                            call.enqueue(new Callback<DataResponse>() {
                                @Override
                                public void onResponse(Call<DataResponse> call, Response<DataResponse> response) {

                                    DataResponse responsePojo = response.body();
                                    Log.d("RESPONCE+++++",responsePojo.status+""+responsePojo.message);
                                    if (responsePojo.status==1) {
                                        Toast.makeText(activity, "" + responsePojo.message, Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(activity, "" + responsePojo.message, Toast.LENGTH_SHORT).show();
                                    }
                                }
                                @Override
                                public void onFailure(Call<DataResponse> call, Throwable t) {
                                    Toast.makeText(activity, "" + t.getMessage(), Toast.LENGTH_SHORT).show();

                                }
                            });
                            Log.d("RESULT_OK++++===",paymentData.toString()+"=="+info+"=="+address+"=="+rawToken);
                            break;
                        case Activity.RESULT_CANCELED:
                            break;
                        case AutoResolveHelper.RESULT_ERROR:
                            Status status = AutoResolveHelper.getStatusFromIntent(data);
                            Log.d("RESULT_ERROR===",status+"");
                            // Log the status for debugging.
                            // Generally, there is no need to show an error to the user.
                            // The Google Pay payment sheet will present any account errors.
                            break;
                        default:
                            // Do nothing.
                    }
                    break;
                default:
                    // Do nothing.
            }
        }
    };
}
