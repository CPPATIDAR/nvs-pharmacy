package com.nvspharmacy;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiInterface {
    @FormUrlEncoded
    @POST("webservice.php?function_name=response_gpay")
    Call<DataResponse> saveDataResponse(@Field("data") String data);
}
