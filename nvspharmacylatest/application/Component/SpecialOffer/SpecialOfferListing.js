import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Image,NetInfo, TouchableOpacity,TextInput,Text,View,FlatList,
  ActivityIndicator,Alert,BackHandler,AsyncStorage} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Constant from '../../../Constant/Constant'
import Toast from 'react-native-simple-toast';
import Service from '../../config/Service';
import TopArrow from 'react-native-vector-icons/FontAwesome'

const nonFavImage = require('../../../resource/img/heart.png');
const favImage = require('../../../resource/img/fill_heart.png');

export class SpecialOfferListing extends Component {
    constructor(props) {
      super(props);
      this.state = {
        customer_id:"",
        data: [],
        isLoading: false,
      };
    }

   async componentWillMount() {
      var customer_id = await AsyncStorage.getItem("customer_id");
      this.setState({customer_id:customer_id});
      this.setState({ isLoading: true });
      //check Internet connectivity 
        NetInfo.getConnectionInfo().then((conectionInfo)=>
        {
          if(conectionInfo.type!="none")
          {
            //webservice calling
            Service.getdataFromService(Constant.getSpecialOffer).then((responseJson)=>
            {
              if(responseJson.length==0)
              {
                Toast.show(Constant.StringText.DATANOTAVAILABLE,Toast.SHORT)
                this.setState({isLoading:false});
              }
              else if(responseJson.length>0){
                this.setState({ data: this.state.data.length === 0?responseJson : [...this.state.data, ...responseJson] ,isLoading:false});
              } 
            });
          }
          else{
            Toast.show(Constant.StringText.CHECKINTERNETCONNECTIVITY,Toast.SHORT);  
          }
        })
         
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    //hardware back button
    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
  
    //harware mobile back button
    handleBackButton = () => {
    this.props.navigation.navigate('Home');
      return true;
    } 

   //render fav heart image
   renderImage = (product_id) => {
    var imgSource = this.state["showNonFavImage"+product_id] ? favImage : nonFavImage;
    return (<Image source={ imgSource }/>);
    }

     //click heart image
    hearclick(product_id){
      if(this.state.customer_id==null || this.state.customer_id=="")
      {
        Toast.show(Constant.StringText.NEEDLOGIN,Toast.SHORT); 
      }else{
        if(!this.state["showNonFavImage"+product_id]==true)
        {
          //add to wishlist service
          this.setState({isLoading:true});
          Service.deletAddProduct(Constant.addwishListitem,this.state.customer_id,product_id).then((responseJson)=>{
            this.setState({isLoading:false});
            Toast.show(responseJson.message,Toast.SHORT);
            if(responseJson.status==1)
            {
              this.setState({["showNonFavImage"+product_id]:true});
            }
          })
        }else{
          Alert.alert( '','Are you sure want this item remove from wishlist. ',
          [
            {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
            {text: 'OK', onPress: () =>
            {
              //delete from wishlist service
              this.setState({isLoading:true});
              Service.deletAddProduct(Constant.deletewishlistitem,this.state.customer_id,product_id).then((responseJson)=>{
                this.setState({isLoading:false});
                Toast.show(responseJson.message,Toast.SHORT);
                if(responseJson.status==1)
                {
                  this.setState({["showNonFavImage"+product_id]:false});
                }
              })
            }},
          ],
          { cancelable: false }
          ) 
        }
      }
    }
    //navigate another page with param
    navigateAnother = (SCREEN,data) => {
      if(data.product_id!==undefined && typeof data.product_id!="undefined"){
        this.props.navigation.navigate(SCREEN,data);
      }
    } 

    //render item for listview
    renderItem(data) {
      let { item, index } = data;    
      return (
        <View style={Constant.styles.productItemBlock}>
            <TouchableOpacity  onPress={() =>this.hearclick(item.product_id)} style={Constant.styles.heartImageView}>
            {this.renderImage(item.product_id)}
            </TouchableOpacity>
          <TouchableOpacity  onPress={() => {this.navigateAnother('ProductDetail',{product_id:item.product_id})}}>
          <View style={Constant.styles.productImageView}>
            <Image source={{uri:item.image}} style={Constant.styles.productImageStyle}/>
          </View>
          <View style={Constant.styles.productItemStyle}> 
                <Text numberOfLines={2} style={Constant.styles.blacknormalleftText}>{item.name.toUpperCase()}</Text>
                <Text style={Constant.styles.listSmallText}>{Constant.StringText.SIZE}{item.size}</Text>
                <View style={{flex: 1,flexDirection: 'row',alignSelf:'flex-start'}}> 
                {item.special_price==null?
                  <View style={Constant.styles.listRow}> 
                    <Text style={Constant.styles.listSmallText}>{Constant.StringText.NOW}{parseFloat(item.price).toFixed(2)}</Text>
                  </View>
                  :
                  <View style={Constant.styles.listRow}> 
                    <Text style={Constant.styles.listSmallRedText}>{Constant.StringText.NOW}{parseFloat(item.special_price).toFixed(2)}</Text>
                    <Text style={Constant.styles.crossText}>{Constant.StringText.WAS}{parseFloat(item.price).toFixed(2)}</Text>
                  </View>
                }
               </View>
          </View>
          </TouchableOpacity >
        </View>
      ) 
    }

    render() {
      return( 
        this.state.isLoading?
        <View style={Constant.styles.indicatoreViewsTyle}> 
            <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
        </View>
        :
          <View style={Constant.styles.container}>
              <FlatList 
                    keyExtractor = {( item, index ) => index }
                    data={this.state.data}
                    renderItem={this.renderItem.bind(this)}
                    numColumns={2}
                    ref='ListView_Reference'
              />
            <TouchableOpacity style={Constant.styles.stickybutton} onPress={this.scrollToTop}>
                <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
              </TouchableOpacity>
          </View>
       );
    }
}