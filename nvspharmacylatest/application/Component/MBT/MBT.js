import React, { Component } from 'react';
import {View,ActivityIndicator,ScrollView,StyleSheet,Dimensions,Text,NetInfo,BackHandler,Image
  ,WebView,TouchableOpacity} from 'react-native';
import Constant from '../../../Constant/Constant';
import HTMLView from 'react-native-htmlview';
import Toast from 'react-native-simple-toast';
import Service from '../../config/Service';
import TopArrow from 'react-native-vector-icons/FontAwesome'

export class MBT extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title:"",
      content:"",
      isLoading: false,
      error: null,
    
    };
  }
  componentWillMount() {   
    //calling webservice fetch data  
   this.setState({ isLoading: true });
   //check Internet connectivity 
   NetInfo.getConnectionInfo().then((conectionInfo)=>
   {
     if(conectionInfo.type!="none")
     {
       //webservice calling
       Service.getPagesdata(Constant.getPagecontent,"mbt").then((responseJson)=>
       {
        this.setState({title:responseJson.data.title,content:responseJson.data.content,isLoading: false});
       });
     }
     else{
       Toast.show(Constant.StringText.CHECKINTERNETCONNECTIVITY,Toast.SHORT);  
     }
   })
   BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
//handle back hardware button
handleBackButton = () => {
  this.props.navigation.navigate('Home');
  return true;
} 

  render() {
    return( 
      this.state.isLoading?
      <View style={Constant.styles.indicatoreViewsTyle}> 
          <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
      </View>
      :
              <View style={Constant.styles.container}>
                <ScrollView ref={(scroller) => {this.scroller = scroller}}>
                     <View style={Constant.styles.containerwithp10}>
                        <Text style={Constant.styles.blackboltext}>{this.state.title}</Text>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.content} renderNode={this.renderNode}></HTMLView>
                     </View>
                </ScrollView>
              <TouchableOpacity style={Constant.styles.stickybutton} onPress={this.scrollToTop}>
                <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
              </TouchableOpacity>
              </View>
          );
    }

    //top of screen
    scrollToTop = () => {
      this.scroller.scrollTo({x: 0, y: 0});
    };

    renderNode(node, index, siblings, parent, defaultRenderer) {
      if (node.name == 'img') {
          const a = node.attribs;
          return ( <Image style={{width:Dimensions.get('window').width,margin:5,height:200}} resizeMode={'center'} key={index} source={{uri: a.src}}/> );
      }
      else if (node.name == 'iframe') {
        const a = node.attribs;
        const iframeHtml = `<iframe src="${a.src}"></iframe>`;
        return (this.renderView
        );
      }
    }

    renderView()
    {
      <View key={index} style={{width: Number(a.width), height: Number(a.height)}}>
      <WebView source={{html: iframeHtml}} />
    </View>
    }

    //for html text because this is not take common style
    styles = StyleSheet.create({
        a: {
          fontWeight:'300',
          color: '#006d50', // pink links
        },
    })
}