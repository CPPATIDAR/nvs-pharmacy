import React, { Component } from 'react';
import {View,ActivityIndicator,Image,ScrollView,StyleSheet,Text,NetInfo,BackHandler,TouchableOpacity,Dimensions} from 'react-native';
import Constant from '../../../Constant/Constant';
import HTMLView from 'react-native-htmlview';
import Toast from 'react-native-simple-toast';
import Service from '../../config/Service';
import TopArrow from 'react-native-vector-icons/FontAwesome'

export class GPhCMHRA extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title:"",
      content:"",
      isLoading: false,
      error: null,
    
    };
  }
  componentWillMount() {   
    //calling webservice fetch data  
   this.setState({ isLoading: true });
   //check Internet connectivity 
   NetInfo.getConnectionInfo().then((conectionInfo)=>
   {
     if(conectionInfo.type!="none")
     {
       //webservice calling
       Service.getPagesdata(Constant.getPagecontent,"gphc-mhra").then((responseJson)=>
       {
        this.setState({title:responseJson.data.title,content:responseJson.data.content,isLoading: false});
       });
     }
     else{
       Toast.show(Constant.StringText.CHECKINTERNETCONNECTIVITY,Toast.SHORT);  
     }
   })
   BackHandler.addEventListener('hardwareBackPress', this.handleBackButton.bind(this));
  }

    //hardware back button
    componentWillUnmount() {
    //handle tab click listener  remove
     BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton.bind(this));
    }

     //harware mobile back button
    handleBackButton(){
    if (this.props.navigation.state.routeName == 'GPhCMHRA') {
      this.props.navigation.navigate('Information');
      return true;
    } 
  }

  render() {
    return( 
      this.state.isLoading?
      <View style={Constant.styles.indicatoreViewsTyle}> 
          <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
      </View>
      :
      <View style={Constant.styles.container}>
        <ScrollView ref={(scroller) => {this.scroller = scroller}}>
          <View style={Constant.styles.containerwithp10}>
            <Text style={Constant.styles.blackboltext}>{this.state.title}</Text>
            <HTMLView stylesheet={this.styles} renderNode={this.renderNode} textComponentProps={{ style:{color:'#000000'}}} value={this.state.content}></HTMLView>
          </View>
        </ScrollView>
        <TouchableOpacity style={Constant.styles.stickybutton} onPress={this.scrollToTop}>
          <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
        </TouchableOpacity>
      </View>
      );
    }

    renderNode(node, index, siblings, parent, defaultRenderer){
      if (node.name == 'img') {
        const a = node.attribs;
        return (
        <Image style={{width:200,height:200}} resizeMode={'center'} key={index} source={{uri: a.src}}/>
      );
      }
    }
    //top of screen
    scrollToTop = () => {
      this.scroller.scrollTo({x: 0, y: 0});
    };
    //for html text because this is not take common style
    styles = StyleSheet.create({
        a: {
          fontWeight:'300',
          color: '#006d50', // pink links
        },
    })
}