import React, { Component } from 'react';
import {View,ScrollView,Text,TouchableOpacity,BackHandler} from 'react-native';
import Constant from '../../../Constant/Constant';
import Icon from 'react-native-vector-icons/Ionicons'
import TopArrow from 'react-native-vector-icons/FontAwesome'

export class Testimonial extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      error: null,
    };
  }

  componentWillMount() {     
     BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  //hardware back button
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  //harware mobile back button
  handleBackButton(){
    if (this.props.navigation.state.routeName == 'Testimonial') {
      this.props.navigation.navigate('Information');
      return true;
    } 
  }

  //navigateToanother
  navigateToanother(SCREEN){
    this.props.navigation.navigate(SCREEN);
  }

  render() {
          return (
            <View style={Constant.styles.container}>
              <ScrollView style={Constant.styles.container} ref={(scroller) => {this.scroller = scroller}}>
                <View style={[Constant.styles.containerwithp10]}>                       
                </View>
              </ScrollView>
              <TouchableOpacity style={Constant.styles.stickybutton} onPress={this.scrollToTop}>
          <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
        </TouchableOpacity>
            </View>
          );
    }

    //top of screen
    scrollToTop = () => {
      this.scroller.scrollTo({x: 0, y: 0});
    };
}