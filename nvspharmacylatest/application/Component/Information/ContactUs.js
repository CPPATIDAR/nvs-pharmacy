import React, { Component } from 'react';
import {View,ScrollView,Text,TextInput,TouchableOpacity,ActivityIndicator,AsyncStorage,
StyleSheet,BackHandler,NetInfo} from 'react-native';
import Constant from '../../../Constant/Constant';
import Toast from 'react-native-simple-toast';
import HTMLView from 'react-native-htmlview';
import Service from '../../config/Service';
import TopArrow from 'react-native-vector-icons/FontAwesome'

export class ContactUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      error: null,
      name:"",
      email:"",
      telephone:"",
      comment:"",
      customer_id:"",
      content:"",
      title:"",
    };
  }

 async componentWillMount() {   
   //Session value
    var customer_id = await AsyncStorage.getItem("customer_id");
    var customer_firstname = await AsyncStorage.getItem("customer_firstname");
    var customer_lastname = await AsyncStorage.getItem("customer_lastname");
    var customer_email = await AsyncStorage.getItem("customer_email");
    this.setState({customer_id:customer_id});
    this.setState({name:(customer_firstname+" "+customer_lastname)});
    this.setState({email:customer_email});
    this.setState({isLoading:true});
    if(!customer_id==""||!customer_id==null)
    {
        this.setState({name:(customer_firstname+" "+customer_lastname),email:customer_email});
    }else{
      this.setState({name:" ",email:""});
    }
     //check Internet connectivity 
     NetInfo.getConnectionInfo().then((conectionInfo)=>
    {
      if(conectionInfo.type!="none")
      {
        //webservice calling
        Service.getdataFromService(Constant.contactus).then((responseJson)=>
        {
          this.setState({title: responseJson.contact_address.title,content: responseJson.contact_address.content, isLoading: false});
        });
      }
      else{
        Toast.show(Constant.StringText.CHECKINTERNETCONNECTIVITY,Toast.SHORT);  
      }
    });
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton.bind(this));
    }
    
    //hardware back button
    componentWillUnmount() {
    //handle tab click listener  remove
     BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton.bind(this));
    }

     //harware mobile back button
    handleBackButton(){
      if (this.props.navigation.state.routeName == 'ContactUs') {
        this.props.navigation.navigate('Information');
        return true;
      } 
    }

  //navigate to another with validation
  NavigateAnother = (SCREEN) =>{
      if(this.state.name=="")
      {
          Toast.show(Constant.StringText.ENTERNAME,Toast.SHORT);
      }else if(this.state.email=="")
      {
        Toast.show(Constant.StringText.ENTEREMAIL,Toast.SHORT);
      }else if(this.state.telephone=="")
      {
        Toast.show(Constant.StringText.ENTERTELEPHONE,Toast.SHORT);
      }else if(this.state.comment=="")
      {
        Toast.show(Constant.StringText.ENTERCOMMENT,Toast.SHORT);
      }else{
          //calling webservice submit feedback data  
          this.setState({ isLoading: true });
          Service.submitFeedback(Constant.contact_submit,this.state.name,this.state.email,this.state.telephone,this.state.comment).then((responseJson)=>{
              Toast.show(responseJson.message,Toast.SHORT);
              this.setState({isLoading: false});
              if(responseJson.status==1)
              {
                this.props.navigation.navigate(SCREEN);
              }
          })
      }
  } 

  render() {
    return( 
      this.state.isLoading?
      <View style={Constant.styles.indicatoreViewsTyle}> 
          <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
      </View>
      :
      <View style={Constant.styles.containerwithp10}>
            <ScrollView ref={(scroller) => {this.scroller = scroller}}>
              <View style={Constant.styles.containerwithp10}>
                        <View>
                        <Text style={Constant.styles.blackboltext}>{this.state.title}</Text>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.content}/>
                        </View>
                        <View style={Constant.styles.lightGreenPanelAcc}> 
                        <Text style={Constant.styles.whitenormalleftext}>{Constant.StringText.CONTACTINFORMATION}</Text>
                        </View>

                        <View flexDirection='row'><Text style={[Constant.styles.listSmallText]}>{Constant.StringText.NAME}</Text>
                           <Text style={[Constant.styles.listSmallRedText]}>{Constant.StringText.ASTERISK}</Text></View>
                        <TextInput onChangeText={name => this.setState({name})} value={this.state.name} placeholder={Constant.StringText.NAME} underlineColorAndroid='transparent'style={[Constant.styles.boxwithborder]}/>
                        <View flexDirection='row'><Text style={[Constant.styles.listSmallText]}>{Constant.StringText.EMAILADDRESS}</Text>
                           <Text style={[Constant.styles.listSmallRedText]}>{Constant.StringText.ASTERISK}</Text></View>
                        <TextInput onChangeText={email => this.setState({email})} value={this.state.email} placeholder={Constant.StringText.EMAIL} underlineColorAndroid='transparent'style={[Constant.styles.boxwithborder]}/>
                        <View flexDirection='row'><Text style={[Constant.styles.listSmallText]}>{Constant.StringText.TELEPHONE}</Text>
                           <Text style={[Constant.styles.listSmallRedText]}>{Constant.StringText.ASTERISK}</Text></View>
                        <TextInput onChangeText={telephone => this.setState({telephone})} placeholder={Constant.StringText.TELEPHONE} underlineColorAndroid='transparent'style={[Constant.styles.boxwithborder]}/>
                        <View flexDirection='row'><Text style={[Constant.styles.listSmallText]}>{Constant.StringText.COMMENT}</Text>
                           <Text style={[Constant.styles.listSmallRedText]}>{Constant.StringText.ASTERISK}</Text></View>
                        <TextInput onChangeText={comment => this.setState({comment})} placeholder={Constant.StringText.COMMENT} underlineColorAndroid='transparent'style={[Constant.styles.boxwithborder]}/>
                        <View style={Constant.styles.m10Top}></View>
                        <TouchableOpacity style={[Constant.styles.orangebutton]} onPress={()=>this.NavigateAnother('Home')}>
                            <Text style={[Constant.styles.whitebuttontext]}>{Constant.StringText.SUBMIT}</Text>
                        </TouchableOpacity>
                       
              </View>
            </ScrollView>
          <TouchableOpacity style={Constant.styles.stickybutton} onPress={this.scrollToTop}>
            <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
          </TouchableOpacity>
        </View>
      );
    }
    //top of screen
    scrollToTop = () => {
      this.scroller.scrollTo({x: 0, y: 0});
    };
      //for html text because this is not take common style
      styles = StyleSheet.create({
        a: {
          fontWeight:'300',
          color: '#006d50', // pink links
        },
    })
}