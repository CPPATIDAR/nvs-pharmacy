import React, { Component } from 'react';
import {View,Text,ScrollView,BackHandler,ActivityIndicator,NetInfo,TouchableOpacity,Image,StyleSheet} from 'react-native';
import Constant from '../../../Constant/Constant';
import Toast from 'react-native-simple-toast';
import HTMLView from 'react-native-htmlview';
import Service from '../../config/Service';
import TopArrow from 'react-native-vector-icons/FontAwesome'

export class AboutUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title:"", 
      content:"",
      isLoading: false,
      error: null
    };
  } 

  componentWillMount() {   
    this.setState({ isLoading: true });
    //check Internet connectivity 
    NetInfo.getConnectionInfo().then((conectionInfo)=>
    {
      if(conectionInfo.type!="none")
      {
        //webservice calling
        //Constant.getPagecontent+"about-store"
        Service.getPagesdata(Constant.getPagecontent,"about-store").then((responseJson)=>
        {
          this.setState({title:responseJson.data.title,content:responseJson.data.content,isLoading: false});
        });
      }
      else{ 
        Toast.show(Constant.StringText.CHECKINTERNETCONNECTIVITY,Toast.SHORT);  
      }
    })
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton.bind(this));
    }

      //hardware back button
      componentWillUnmount() {
      //handle tab click listener  remove
       BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton.bind(this));
      }
  
       //harware mobile back button
      handleBackButton(){
      if (this.props.navigation.state.routeName == 'AboutUs') {
        this.props.navigation.navigate('Information');
        return true;
      } 
    }
  
  render() {
    return( 
      this.state.isLoading?
      <View style={Constant.styles.indicatoreViewsTyle}> 
          <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
      </View>
      :
       <View style={Constant.styles.container}>
            <ScrollView ref={(scroller) => {this.scroller = scroller}}>
                  <View style={Constant.styles.containerwithp10}>
                    <View>
                      <Text style={Constant.styles.blackboltext}>{this.state.title}</Text>
                     <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.content}></HTMLView>
                     </View>
                     <View style={Constant.styles.homeBottomView}>
                        <TouchableOpacity style={Constant.styles.socialView} onPress={()=>{Constant.openUrl(Constant.StringText.FACEBOOKLINK)}}>
                          <Image style={Constant.styles.socialImage} source={Constant.facebookIcon}/>
                          <Text style={Constant.styles.listSmallText}>{Constant.StringText.FACEBOOK}</Text>
                        </TouchableOpacity>
                        <Image style={Constant.styles.h74} source={Constant.verticaldotImage}/>
                        <TouchableOpacity style={Constant.styles.socialView} onPress={()=>{Constant.openUrl(Constant.StringText.TWITTERLINK)}}>
                          <Image style={Constant.styles.socialImage} source={Constant.twitterIcon} />
                          <Text style={Constant.styles.listSmallText}>{Constant.StringText.TWITTER}</Text>
                        </TouchableOpacity>
                        <Image style={Constant.styles.h74} source={Constant.verticaldotImage}/>
                        <TouchableOpacity style={Constant.styles.socialView} onPress={()=>{Constant.openUrl(Constant.StringText.GOOGLELINK)}}>
                          <Image style={Constant.styles.socialImage} source={Constant.google_plus} />
                          <Text style={Constant.styles.listSmallText}>{Constant.StringText.GOOGEL}</Text>
                        </TouchableOpacity>
                    </View>
                  </View>
            </ScrollView>
          <TouchableOpacity style={Constant.styles.stickybutton} onPress={this.scrollToTop}>
            <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
          </TouchableOpacity>
        </View>
      );
    }

    //top of screen
    scrollToTop = () => {
      this.scroller.scrollTo({x: 0, y: 0});
    };
    //for html text because this is not take common style
    styles = StyleSheet.create({
      a: {
        fontWeight:'300',
        color: '#006d50', // pink links
      },
    })
}