import React, { Component } from 'react';
import {View,ScrollView,Text,TextInput,TouchableOpacity,BackHandler} from 'react-native';
import Constant from '../../../Constant/Constant';
import Icon from 'react-native-vector-icons/Ionicons'

export class Information extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      error: null,
    };
  }

  componentWillMount() {     
     BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  //hardware back button
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  //harware mobile back button
  handleBackButton = () => {
  this.props.navigation.navigate('Home');
    return true;
  } 

  //navigateToanother
  navigateToanother(SCREEN){
    this.props.navigation.navigate(SCREEN);
  }
  render() {
          return (
              <ScrollView style={Constant.styles.container}>
                <View style={[Constant.styles.containerwithp10]}>
                        <TouchableOpacity style={Constant.styles.listitemBlock} onPress={()=>this.navigateToanother('AboutUs')}>
                            <Text style={Constant.styles.listitemNameParent}>{Constant.StringText.ABOUTUS}</Text>
                            <Icon name="ios-arrow-forward" size={28} color="#000000"/>
                        </TouchableOpacity>
                        <TouchableOpacity style={Constant.styles.listitemBlock} onPress={()=>this.navigateToanother('StoreLocation')}>
                            <Text style={Constant.styles.listitemNameParent}>{Constant.StringText.STORELOCATION}</Text>
                            <Icon name="ios-arrow-forward" size={28} color="#000000"/>
                        </TouchableOpacity>
                        <TouchableOpacity style={Constant.styles.listitemBlock} onPress={()=>this.navigateToanother('Howtoorder')}>
                            <Text style={Constant.styles.listitemNameParent}>{Constant.StringText.HOWTOORDER}</Text>
                            <Icon name="ios-arrow-forward" size={28} color="#000000"/>
                        </TouchableOpacity>
                        <TouchableOpacity style={Constant.styles.listitemBlock} onPress={()=>this.navigateToanother('Testimonial')}>
                            <Text style={Constant.styles.listitemNameParent}>{Constant.StringText.TESTIMONIAL}</Text>
                            <Icon name="ios-arrow-forward" size={28} color="#000000"/>
                        </TouchableOpacity>
                        <TouchableOpacity style={Constant.styles.listitemBlock} onPress={()=>this.navigateToanother('Shippingdelivery')}>
                            <Text style={Constant.styles.listitemNameParent}>{Constant.StringText.SHIPPINGDELIVERY}</Text>
                            <Icon name="ios-arrow-forward" size={28} color="#000000"/>
                        </TouchableOpacity>
                        <TouchableOpacity style={Constant.styles.listitemBlock} onPress={()=>this.navigateToanother('ContactUs')}>
                            <Text style={Constant.styles.listitemNameParent}>{Constant.StringText.CONTACTUS}</Text>
                            <Icon name="ios-arrow-forward" size={28} color="#000000"/>
                        </TouchableOpacity>
                        <TouchableOpacity style={Constant.styles.listitemBlock} onPress={()=>this.navigateToanother('GPhCMHRA')}>
                            <Text style={Constant.styles.listitemNameParent}>{Constant.StringText.GPHCMHRA}</Text>
                            <Icon name="ios-arrow-forward" size={28} color="#000000"/>
                        </TouchableOpacity>
                       
                </View>
              </ScrollView>
          );
    }
}