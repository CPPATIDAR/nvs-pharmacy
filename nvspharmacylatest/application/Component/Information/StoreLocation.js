import React, { Component } from 'react';
import {View,ActivityIndicator,ScrollView,StyleSheet,Text,NetInfo,BackHandler,TouchableOpacity,WebView} from 'react-native';
import Constant from '../../../Constant/Constant';
import HTMLView from 'react-native-htmlview';
import Toast from 'react-native-simple-toast';
import Service from '../../config/Service';
import TopArrow from 'react-native-vector-icons/FontAwesome'

export class StoreLocation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title:"",
      content:"",
      isLoading: false,
      error: null,
    
    };
  }
  componentWillMount() {   
    //calling webservice fetch data  
   this.setState({ isLoading: true });
   //check Internet connectivity 
   NetInfo.getConnectionInfo().then((conectionInfo)=>
   {
     if(conectionInfo.type!="none")
     {
       //webservice calling
       Service.getPagesdata(Constant.getPagecontent,"store-location").then((responseJson)=>
       {
        this.setState({title:responseJson.data.title,content:responseJson.data.content,isLoading: false});
       });
     }
     else{
       Toast.show(Constant.StringText.CHECKINTERNETCONNECTIVITY,Toast.SHORT);  
     }
   })
   BackHandler.addEventListener('hardwareBackPress', this.handleBackButton.bind(this));
  }

    //hardware back button
    componentWillUnmount() {
    //handle tab click listener  remove
     BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton.bind(this));
    }

     //harware mobile back button
    handleBackButton(){
    if (this.props.navigation.state.routeName == 'StoreLocation') {
      this.props.navigation.navigate('Information');
      return true;
    } 
  }


  render() {
    return( 
      this.state.isLoading?
      <View style={Constant.styles.indicatoreViewsTyle}> 
          <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
      </View>
      :
              <View style={Constant.styles.container}>
                <ScrollView ref={(scroller) => {this.scroller = scroller}}>
                     <View style={Constant.styles.containerwithp10}>
                        <Text style={Constant.styles.blackboltext}>{this.state.title}</Text>
                        <HTMLView stylesheet={this.styles} value={this.state.content} textComponentProps={{ style:{color:'#000000'}}}/>
                     </View>
                </ScrollView>
                <TouchableOpacity style={Constant.styles.stickybutton} onPress={this.scrollToTop}>
          <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
        </TouchableOpacity>
              </View>
          );
    }

    renderNode(node, index, siblings, parent, defaultRenderer){
      if (node.name == 'iframe') {
        const a = node.attribs;
        const iframeHtml = `<iframe width:300, height: 300 src=${a.src}></iframe>`;
        return (
          <WebView key={index} source={{html: iframeHtml}} style={{width:300, height: 300}} />
        );
      }
    }

    renderView=()=>
    {
      <WebView 
      key={index} source={{html: iframeHtml}} style={{width:300, height: 300}} />
    }
    //top of screen
    scrollToTop = () => {
      this.scroller.scrollTo({x: 0, y: 0});
    };
    //for html text because this is not take common style
    styles = StyleSheet.create({
        a: {
          fontWeight:'300',
          color: '#006d50', // pink links
        },
    })
}