import React, { Component } from 'react';
import {View,ScrollView,Text,TextInput,TouchableOpacity,BackHandler,NetInfo,ActivityIndicator,StyleSheet,
AsyncStorage} from 'react-native';
import Constant from '../../../Constant/Constant';
import Service from '../../config/Service';
import TopArrow from 'react-native-vector-icons/FontAwesome'
import ImageView from 'react-native-image-progress';
import HTMLView from 'react-native-htmlview';

export class BlogDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource:[],
      isLoading: false,
      error: null,
      blog_id:"",
      post_image:"",
      post_title:"",
      post_content:"",
      post_date:"",
    };
  }
 
  async componentWillMount() {     
    this.props.navigation.addListener('willFocus',this.handleKey);  
    var blog_id = await AsyncStorage.getItem("ID");
    this.setState({isLoading:true,blog_id:blog_id});
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  //hardware back button
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

    //handle tab event
    handleKey = e => {
    if(e.state.routeName=="BlogDetail")
    { 
       //check Internet connectivity 
       NetInfo.getConnectionInfo().then((conectionInfo)=>
      {
        if(conectionInfo.type!="none")
        {
          //webservice calling
          Service.getBlogdetail(Constant.getpost_details,this.state.blog_id).
           then((responseJson)=>
          {
            this.setState({ post_image:responseJson.data.post_image,
            post_title:responseJson.data.post_title,
            post_date:responseJson.data.post_date,
            post_content:responseJson.data.post_content,isLoading: false});
          });
        }
        else{
          Toast.show(Constant.StringText.CHECKINTERNETCONNECTIVITY,Toast.SHORT);  
        }
      })
    }
    } 

  //harware mobile back button
  handleBackButton = () => {
  this.props.navigation.navigate('Home');
    return true;
  } 

  //navigateToanother
  navigateToanother(SCREEN){
    this.props.navigation.navigate(SCREEN);
  }
  render() {
    return( 
        this.state.isLoading?
        <View style={Constant.styles.indicatoreViewsTyle}> 
            <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
        </View>
        :
        <View style={Constant.styles.container}>
          <ScrollView style={Constant.styles.container} ref={(scroller) => {this.scroller = scroller}}>   
            <View style={Constant.styles.containerwithp10}>
              <Text style={Constant.styles.greenCenterText}>{this.state.post_date}</Text>
                <Text style={Constant.styles.blackboltext}>{this.state.post_title}</Text>
                {this.state.post_image==""?
                null:
                    <ImageView
                    style={{width:null,height:150}}
                    indicator={ImageView.Pie}
                    indicatorProps={{size: 40,color: '#006d50'}}
                    resizeMode="contain"
                     source={{uri:this.state.post_image}}/>
                }
               <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.post_content}></HTMLView>
            </View>
            </ScrollView>
          <TouchableOpacity style={Constant.styles.stickybutton} onPress={this.scrollToTop}>
             <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
          </TouchableOpacity>
        </View>
        );
    }
    //for html text because this is not take common style
    styles = StyleSheet.create({
      a: {
        fontWeight:'300',
        color: '#006d50', // pink links
      },
  })
    //top of listview
     scrollToTop =()=>{
      this.scroller.scrollTo({x: 0, y: 0});
     }
}