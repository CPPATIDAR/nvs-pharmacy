import React, { Component } from 'react';
import {View,ScrollView,Text,TextInput,TouchableOpacity,BackHandler,NetInfo,ActivityIndicator,
    FlatList,Image,AsyncStorage} from 'react-native';
import Constant from '../../../Constant/Constant';
import Icon from 'react-native-vector-icons/Ionicons'
import Service from '../../config/Service';
import TopArrow from 'react-native-vector-icons/FontAwesome'
import ImageView from 'react-native-image-progress';

export class Blog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource:[],
      isLoading: false,
      error: null,
    };
  }

  componentWillMount() {     
    this.props.navigation.addListener('willFocus',this.handleKey);  
    this.setState({ isLoading: true });
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  //hardware back button
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

    //handle tab event
    handleKey = e => {
    if(e.state.routeName=="Blog")
    {
       //check Internet connectivity 
       NetInfo.getConnectionInfo().then((conectionInfo)=>
      {
        if(conectionInfo.type!="none")
        {
          //webservice calling
          Service.getdataFromService(Constant.getallWPposts).
          then((responseJson)=>
          {
            this.setState({dataSource: responseJson.data,isLoading: false});
          });
        }
        else{
          Toast.show(Constant.StringText.CHECKINTERNETCONNECTIVITY,Toast.SHORT);  
        }
      })
    }
    } 

  //harware mobile back button
  handleBackButton = () => {
  this.props.navigation.navigate('Home');
    return true;
  } 

  //navigateToanother
  navigateToanother(SCREEN){
    this.props.navigation.navigate(SCREEN);
  }
  render() {
    return( 
        this.state.isLoading?
        <View style={Constant.styles.indicatoreViewsTyle}> 
            <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
        </View>
        :
         <View style={Constant.styles.containerwithp10}>
                  <FlatList 
                      keyExtractor = {( item, index ) => index }
                      data={this.state.dataSource}
                      renderItem={this.renderItem.bind(this)}
                      ref='ListView_Reference'
                  />
                <TouchableOpacity style={Constant.styles.stickybutton} onPress={this.scrollToTop}>
                  <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
                </TouchableOpacity>
            </View>
            );
    }
 
    //render listitem
    renderItem(data) {
        let { item, index } = data;    
        return (
        <View>
        <TouchableOpacity key={index} onPress={()=>this.navigateAnother('BlogDetail',item.ID)}>
            <View style={{alignSelf:'center'}}>
                <Text style={Constant.styles.greenCenterText}>{item.post_date}</Text>
                <Text style={Constant.styles.blackboltext}>{item.post_title}</Text>
                {item.post_image!=""?
                 <Image
                 style={{width:null,height:150}}
                /*  indicator={ImageView.Pie}
                 indicatorProps={{size: 40,color: '#006d50'}} */
                 resizeMode="cover"
                 source={{uri:item.post_image}}/>
                 :
                 null
                }
                <Text style={Constant.styles.listSmallText}>{item.post_short_content}</Text>     
            </View>
        </TouchableOpacity>
            <Text onPress={()=>this.navigateAnother('BlogDetail',item.ID)} style={Constant.styles.greenTextWithline}>{Constant.StringText.READMORE}</Text>
            <View style={Constant.styles.viewStyle}></View>
        </View>
        
        ); 
    }

    //navigate to another with param
  async navigateAnother (SCREEN,data) {
    if(data!==undefined && typeof data!="undefined"){
      try {
        await AsyncStorage.setItem('ID', data+"");
      } catch (error) {
        // Error saving data
        console.log(error)
      }
      this.props.navigation.navigate(SCREEN,data);
    }
  } 

     //top of listview
     scrollToTop =()=>{
        this.refs.ListView_Reference.scrollToOffset({ offset: 0, animated: true });
     }
}