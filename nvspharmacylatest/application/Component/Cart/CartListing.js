import React, { Component } from 'react';
import {FlatList,Image,Text,View,StyleSheet,ScrollView,TouchableHighlight,BackHandler,TouchableOpacity,
ActivityIndicator,AsyncStorage,RefreshControl,Alert} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Constant from '../../../Constant/Constant';
import CrossIcon from 'react-native-vector-icons/Entypo';
import Toast from 'react-native-simple-toast';
import Service from '../../config/Service';
import ImageView from 'react-native-image-progress';
import TopArrow from 'react-native-vector-icons/FontAwesome'

export default class CartListing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      customer_id:"",
      resultskey:"",
      items_count:"",
      items_qty:"",
      subtotal:"",
      grandtotal:"",
      dataSource:[],
      isLoading: false,
      error: null,
      quantity:{},
      refreshing: false,
      card_id:"",
      loadingImage : false,
      rowtotal:{},
    };
  }
 
//handle tab event
 handleKey = async(e) => 
 {
    if(e.state.routeName=="CartListing")
    {
      var customer_id = await AsyncStorage.getItem("customer_id");
      var card_id = await AsyncStorage.getItem("card_id");
      this.setState({customer_id:customer_id,card_id:card_id,isLoading:true});
      var array = await AsyncStorage.getItem("array");
      if(customer_id==""||customer_id==null)
      {
        this.setState({isLoading:false,refreshing:false});
        if(array==null||JSON.parse(array).length==0)
        {
          this.setState({dataSource:""});
        }
        else
        {
            let sum = a => a.reduce((x, y) => x + y);
            let totalAmount = sum(JSON.parse(array).map(x => Number(x.rowtotal)));
            let totalquantity = sum(JSON.parse(array).map(x =>Number(x.quantity)));
            this.setState({
              dataSource:JSON.parse(array),
              items_count: JSON.parse(array).length,
              items_qty:totalquantity,
              subtotal:totalAmount,
              grandtotal:totalAmount
            });
        }
        this.props.navigation.setParams({cartcout:this.state.dataSource.length+""});
        await AsyncStorage.setItem("cartcout",this.state.dataSource.length+"");
      }
      else{
        this.getCartListing();
      }
    }
  } 
  /**
     * Call _fetchData after component has been mounted
  */
  async componentWillMount() {
    this.props.navigation.addListener('willFocus',this.handleKey);
    var cartcout = await AsyncStorage.getItem("cartcout"); 
    this.props.navigation.setParams({cartcout:cartcout});
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  //get cart listing
  getCartListing=()=>{
    //webservice to get item list
    Service.getCartListing(Constant.getCartDetail,this.state.card_id,this.state.customer_id).then(async(responseJson)=>{
      this.setState({isLoading:false});
      this.props.navigation.setParams({cartcout:responseJson.result.data.length+""});
      await AsyncStorage.setItem("cartcout",responseJson.result.data.length+"");
      if(responseJson.result.status==1)
      {
        if(responseJson.result.data.length!=0)
        {
          this.setState({dataSource:responseJson.result.data,
              items_count: responseJson.result.items_count,
              items_qty:responseJson.result.items_qty,
              subtotal:responseJson.result.subtotal,
              grandtotal:responseJson.result.grandtotal,
              refreshing:false,
            })
        }
      }
      else{
        this.setState({dataSource:"",
          items_count: "",
          items_qty:"",
          subtotal:"",
          grandtotal:"",
          refreshing:false
        });
      }
    })
  }

  //handle back hardware button
  handleBackButton = () => {
    this.props.navigation.navigate('Home');
    return true;
  } 
  //delete product from list
  _delete=(product_id)=> {
    Alert.alert(
      '',
      'Are you sure want delete this item. ',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'OK', onPress:async() => 
        {
          if(this.state.customer_id==""||this.state.customer_id==null)
          {
            var dataSource= this.state.dataSource.filter(item => item.product_id !== product_id);
            this.setState({dataSource});
            if(this.state.dataSource.length!=0)
            {
              let sum = a => a.reduce((x, y) => x + y);
              let totalAmount = sum(this.state.dataSource.map(x => Number(x.rowtotal)));
              let totalquantity = sum(this.state.dataSource.map(x =>Number(x.quantity)));
              this.setState({
                dataSource:this.state.dataSource,
                items_count: this.state.dataSource.length,
                items_qty:totalquantity,
                subtotal:totalAmount,
                grandtotal:totalAmount,
              });
            }
            else{
              this.setState({
                dataSource:"",
                items_count: "",
                items_qty:"",
                subtotal:"",
                grandtotal:"",
              });
            }
            this.props.navigation.setParams({cartcout:this.state.dataSource.length+""});
            await AsyncStorage.setItem("cartcout",this.state.dataSource.length+"");
            await AsyncStorage.setItem("array",JSON.stringify(dataSource));
          }
          else{
            this.setState({isLoading:true});
            //delete product from cart
            Service.deletCardProduct(Constant.deleteItemsincart,this.state.card_id,product_id).then((responseJson)=>{
                this.setState({isLoading:false });
                Toast.show(responseJson.message,Toast.SHORT);
                if(responseJson.status==1)
                {
                  var dataSource = [...this.state.dataSource]
                  let index = dataSource.indexOf(product_id);
                  dataSource.splice(index, 1);
                  this.setState({dataSource});
                  this.getCartListing();
                }
            })
          }
        }
        },
      ],
      { cancelable: false }
    ) 
  }

  //increase item quantity
  IncrementItem = (id) => {
    this.state.quantity[id]=(this.state.quantity[id]>=1)?++this.state.quantity[id]:1;
    var updateQuantity=[];
    this.state.dataSource.map(data=>{
        if(data.product_id==id){
          data.quantity=this.state.quantity[id];
          if(data.special_price==0||data.special_price==null)
          {
            data.rowtotal=data.price*this.state.quantity[id];
          }else{
            data.rowtotal=data.special_price*this.state.quantity[id];
          }
        }
        updateQuantity=[...updateQuantity,data];
    });
    let sum = a => a.reduce((x, y) => x + y);
    let totalAmount = sum(updateQuantity.map(x => Number(x.rowtotal)));
    this.setState({dataSource:updateQuantity,subtotal:totalAmount});
  }

  //decrease item quantity          
  DecreaseItem = (id) => {
    this.state.quantity[id]=(this.state.quantity[id]>1)?--this.state.quantity[id]:1;
    var updateQuantity=[];
    this.state.dataSource.map(data=>{
        if(data.product_id==id){
          data.quantity=this.state.quantity[id];
          if(data.special_price==0||data.special_price==null)
          {
            data.rowtotal=data.price*this.state.quantity[id];
          }else{
            data.rowtotal=data.special_price*this.state.quantity[id];
          }
        }
        updateQuantity=[...updateQuantity,data];
    });
    let sum = a => a.reduce((x, y) => x + y);
    let totalAmount = sum(updateQuantity.map(x => Number(x.rowtotal)));
    this.setState({dataSource:updateQuantity,subtotal:totalAmount});
  }
  //clear all item from cart
  clearCart=async()=>{
    Alert.alert(
      '',
      'Are you sure want delete this item. ',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'OK', onPress:async() => 
        {
          this.props.navigation.setParams({cartcout:""});
          await AsyncStorage.setItem("cartcout","");
          if(this.state.customer_id==""||this.state.customer_id==null)
            {
              this.setState({dataSource:[],
              items_count: "",
              items_qty:"",
              subtotal:"",
              grandtotal:"",
              refreshing:false,isLoading:true
              })
              await AsyncStorage.setItem("array","");
              this.setState({isLoading:false});
            }
          else{
           this.setState({isLoading:true});
          //clear productlist from cart
          Service.clearCart(Constant.clearcart,this.state.customer_id,this.state.card_id)
          .then((responseJson)=>{
              this.setState({isLoading:false });
              Toast.show(responseJson.message,Toast.SHORT);
              if(responseJson.status==1)
              {
                this.setState({dataSource:[],
                  items_count: "",
                  items_qty:"",
                  subtotal:"",
                  grandtotal:"",
                  refreshing:false
                  })
              }
          }) 
          }
        }
        },
      ],
      { cancelable: false }
    )
  }

  //edit quantity of item in cart
  editCart=async()=>{
    if(this.state.customer_id==""||this.state.customer_id==null)
    {
    let sum = a => a.reduce((x, y) => x + y);
    let totalAmount = sum(this.state.dataSource.map(x =>Number(x.rowtotal)));
    let totalquantity = sum(this.state.dataSource.map(x =>Number(x.quantity)));
      this.setState({
        dataSource:this.state.dataSource,
        items_count: this.state.dataSource.length,
        items_qty:totalquantity,
        subtotal:totalAmount,
        grandtotal:totalAmount,
        isLoading:true
      });
      await AsyncStorage.setItem("array",JSON.stringify(this.state.dataSource));
      this.setState({isLoading:false});
    }
    else{
      this.setState({isLoading:true});
      //edit productlist from cart
      Service.editcart(Constant.updatecart,this.state.customer_id,this.state.card_id,JSON.stringify(this.state.dataSource)).then((responseJson)=>{
          this.setState({isLoading:false });
          Toast.show(responseJson.message,Toast.SHORT);
          if(responseJson.status==1)
          {
            this.getCartListing();
          }
      }) 
    }
  }

  //navigate another page with param
  navigateProductDetail = (SCREEN,data) => {
    if(data.product_id!==undefined && typeof data.product_id!="undefined"){
      this.props.navigation.navigate(SCREEN,data);
    }
  } 

  //navigate another page with check customer_id 
  nextScreen = () => {
    if(!this.state.customer_id==null||!this.state.customer_id=="")
    {
      if(!this.state.dataSource.length==0)
      {
        this.props.navigation.navigate('CheckOut2');
      }else{
        Toast.show(Constant.StringText.NODATAAVAILBLE,Toast.SHORT);
      }
    }
    else{
      this.props.navigation.navigate('Account');
    }
  } 

  //list seperator
  renderSeparator = () => {return (<View style={Constant.styles.separator}/>);};

  //render item for listview
  renderItem(data) {
    let { item, index } = data;
    this.state.quantity[item.product_id] = parseInt(item.quantity);
    this.state.rowtotal[item.product_id] = item.rowtotal;
    return (
      <View style={Constant.styles.listcontainer}>
        <TouchableOpacity activeOpacity={0.5} style={Constant.styles.listImageView} onPress={() => {this.navigateProductDetail('ProductDetail',{product_id:item.product_id,quantity:item.quantity})}}>
          <ImageView style={Constant.styles.listImage} 
            indicator={ImageView.Pie}
            indicatorProps={{size: 'small',color: '#006d50'}}
            source={{uri:item.image}} />
        </TouchableOpacity>
        <View style={Constant.styles.m2Left}>
            <Text style={Constant.styles.listSmallText}>{item.name}</Text>
             {item.configurable_options.length!=0||item.configurable_options!=""?
              item.configurable_options.map(function(item,i){
                  return(
                  <View style={Constant.styles.listRow} key={i}> 
                    <Text style={Constant.styles.listSmallText}>{item.label}: </Text>
                    <Text style={Constant.styles.listSmallText}>{item.value}</Text>
                  </View>
                  );
              })
              :null} 
            <View style={Constant.styles.listRow}> 
              {item.size!=null?
                <Text style={Constant.styles.listSmallText}>{Constant.StringText.SIZE}{item.size}</Text>
              :null}
            </View>
            <Text style={Constant.styles.listSmallText}>{Constant.StringText.QTY}{item.quantity}</Text>
            {item.special_price==null||item.special_price==0?
            <View style={Constant.styles.listRow}> 
              <Text style={Constant.styles.listSmallText}>{Constant.StringText.NOW}{parseFloat(item.price).toFixed(2)}</Text>
            </View>
            :
            <View style={Constant.styles.listRow}> 
              <Text style={Constant.styles.listSmallRedText}>{Constant.StringText.NOW}{parseFloat(item.special_price).toFixed(2)}</Text>
              <Text style={Constant.styles.crossText}>{Constant.StringText.WAS}{parseFloat(item.price).toFixed(2)}</Text>
            </View>
            }
            <View>
              <View style={Constant.styles.listRow}> 
                <View>
                    <TouchableOpacity activeOpacity={0.5} onPress={()=>this.DecreaseItem(item.product_id)} style={{marginRight:2}}>
                    <Image style={Constant.styles.allMinusImagesize} source={Constant.decreaseqty}/>
                    </TouchableOpacity> 
                </View>
                <View>
                  <Text style={Constant.styles.listSmallText}>{this.state.quantity[item.product_id]}</Text> 
                </View>
                <View> 
                    <TouchableOpacity activeOpacity={0.5} onPress={()=>this.IncrementItem(item.product_id)} style={{marginLeft:2}}>
                    <Image style={Constant.styles.allMinusImagesize} source={Constant.increaseqty}/>
                    </TouchableOpacity>
                </View>
              </View>
              <View style={{flex:1,flexDirection:'row',alignSelf:'flex-end'}}>
                <Text style={Constant.styles.blackboldlefttext}>{Constant.StringText.SUBTOTAL}</Text>
                <Text style={Constant.styles.blacknormalleftText}>{Constant.Currency} {parseFloat(this.state.rowtotal[item.product_id]).toFixed(2)}</Text>
              </View>
            </View> 
        </View> 
          <TouchableHighlight style={{alignSelf:'flex-start'}} onPress={()=>{this._delete(item.product_id)}}>
            <CrossIcon name={"cross"} size={25} color={'#000000'}/>
          </TouchableHighlight>
      </View>
    ) 
  }
    /**
     * Renders the list
     */
  render(){
    return( 
      this.state.isLoading?
      <View style={Constant.styles.indicatoreViewsTyle}> 
          <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
      </View>
      :
      <View style={Constant.styles.container}>
        {this.state.dataSource.length==0?
          <View  style={{height: '100%',justifyContent: 'center',alignItems: 'center'}}>
          <Image source={require('../../../resource/img/cartempty.jpg')} style={{alignSelf:'center',justifyContent:'center',alignContent:'center',alignItems:'center'}}></Image>
          </View>:
         <View style={Constant.styles.cartView}>
         <View style={Constant.styles.p5CenterRow}>
           <Text style={Constant.styles.blacknormalleftText}>{this.state.items_count} {Constant.StringText.ITEMS}</Text>
           <Text style={Constant.styles.blacknormalrightText}>{Constant.StringText.TOTAL}</Text><Text style={Constant.styles.greennormelrightText}> {Constant.Currency}{parseFloat(this.state.subtotal).toFixed(2)}</Text>
         </View>
         <View style={Constant.styles.p5CenterRow}>
           <TouchableHighlight style={Constant.styles.orangebutton} onPress={()=>this.nextScreen()}> 
             <Text style={Constant.styles.whitebuttontext}>{Constant.StringText.SECURECHECKOUT}</Text>
           </TouchableHighlight>
         </View>
         <View style={{flexDirection:'row'}}>
           <View style={Constant.styles.buttonCartList}>
             <Text style={Constant.styles.blacknormalcenterText} activeOpacity={0.5} onPress={()=>this.clearCart()}>
             {Constant.StringText.CLEARCART}</Text>
           </View>
           <View style={Constant.styles.buttonCartList}>
             <Text style={Constant.styles.blacknormalcenterText} activeOpacity={0.5} onPress={()=>this.editCart()}>
             {Constant.StringText.UPDATECART}</Text>
           </View>
         </View>
         <View style={{flex:1}}>
           <FlatList
             extraData={this.state}
             keyExtractor = {( item, index ) => index}
             data={this.state.dataSource}
             renderItem={this.renderItem.bind(this)}
             ItemSeparatorComponent={this.renderSeparator}
             ref='ListView_Reference'
             refreshControl={
               <RefreshControl
                 refreshing={this.state.refreshing}
                 onRefresh={this._onRefresh.bind(this)}
               />
             }
           />
        
         </View>
         <TouchableOpacity style={Constant.styles.stickybutton} onPress={this.scrollToTop}>
             <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
         </TouchableOpacity>
     </View>
    }
      </View>
      );
  }
  //top of listview
  scrollToTop =()=>{
    this.refs.ListView_Reference.scrollToOffset({ offset: 0, animated: true });
 }
  //refershing flatlistview
  _onRefresh = () => {
    if(!this.state.customer_id==""||!this.state.customer_id==null)
    {
      this.setState({refreshing: true});
      this.getCartListing();
    }else{
      this.setState({refreshing: false});
    }
  }
}
