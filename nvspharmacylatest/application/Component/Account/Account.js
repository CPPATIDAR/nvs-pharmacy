import React, { Component } from 'react';
import {TextInput,Text,TouchableHighlight,Image,ImageBackground,ScrollView,BackHandler,View,Picker,
AsyncStorage,FlatList,Alert,ActivityIndicator,TouchableOpacity,Platform,StyleSheet,KeyboardAvoidingView} from 'react-native';
import Panel from '../CommonPanel/Panel'
import { StackNavigator } from 'react-navigation'
import Constant from '../../../Constant/Constant'
import CrossIcon from 'react-native-vector-icons/Entypo'
import EditICon from 'react-native-vector-icons/FontAwesome';
import DeletIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import Service from '../../config/Service';
import Toast from 'react-native-simple-toast';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import RNPickerSelect from 'react-native-picker-select'; 
import TopArrow from 'react-native-vector-icons/FontAwesome'
import CheckBox from 'react-native-checkbox';


 
export default class Account extends Component 
{
    constructor(props) {
        super(props);
        this.state = {
          customer_id:"",
          isLoading: false,
          billing_address:[],
          shipping_address:[],
          recentlyvieweditems:[],
          wishlistitems:[],
          additionaladdress:[],
          myorders:[],
          customer_email:"",
          customer_firstname:"",
          customer_lastname:"",
          radioValue:"signin",
          email:"",
          password:"",
          firstname:"",
          lastname:"",
          telephone:"",
          emailadress:"",
          address:"",
          city:"",
          country:"",
          state:"",
          postalcode:"",
          creatpasswrd:"",
          laterusecheck:"",
          deliveraddresscheck:"",
          countryArray:[],
          countryValue: "",
          cartarray:"",
          newscheckbox:false,
          newsletter_subscribe:"",
        };
    } 

   //handle tab event
    handleKey = async(e) => {
        if(e.state.routeName=="Account")
        {
            var customer_id = await AsyncStorage.getItem("customer_id");
            var array = await AsyncStorage.getItem("array");
            this.setState({customer_id:customer_id,cartarray:array});
            if(!customer_id==null||!customer_id=="")
            {
                this.setState({isLoading: true});
                this.getUserProfile();
            }
        }else{
            this.props.navigation.navigate('Account');
        }
    } 

    async componentWillMount() {
        this.props.navigation.addListener('willFocus',this.handleKey);
        var cartcout = await AsyncStorage.getItem("cartcout"); 
        this.props.navigation.setParams({cartcout:cartcout});
        this.getCountry();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
 
    //get country 
    getCountry(){
        Service.getdataFromService(Constant.countryList).then((responseJson)=>{
            var countryOptions = [];
            Object.keys(responseJson[0]).forEach(function(key) {
              countryOptions.push({'value':key,'label':responseJson[0][key]}); 
            });
            this.setState({countryArray:countryOptions});
        });
    }

     //register account
     register(){
        if(this.state.firstname=="")
        {
             Toast.show(Constant.StringText.ENTERFIRSTNAME,Toast.SHORT);
        }else if(this.state.lastname=="")
        {
              Toast.show(Constant.StringText.ENTERLASTNAME,Toast.SHORT);
        }else if(this.state.telephone=="")
        {
            Toast.show(Constant.StringText.ENTERLASTNAME,Toast.SHORT);
        }else if(this.state.emailadress=="")
        { 
            Toast.show(Constant.StringText.ENTEREMAIL,Toast.SHORT);
        }else if(this.state.address=="")
        {
            Toast.show(Constant.StringText.ENTERADDRESS,Toast.SHORT);
        }else if(this.state.city==""){
            Toast.show(Constant.StringText.ENTERCITY,Toast.SHORT);
        }else if(this.state.countryValue==""||this.state.countryValue==undefined&&this.state.countryValue==0)
        {
            Toast.show(Constant.StringText.SELECTCOUNTRY,Toast.SHORT);
        }else if(this.state.state=="")
        {
            Toast.show(Constant.StringText.STATEPROVIENCE,Toast.SHORT);
        }else if(this.state.postalcode=="")
        {
            Toast.show(Constant.StringText.POSTALCODE,Toast.SHORT);
        }else if(this.state.creatpasswrd=="")
        {
            Toast.show(Constant.StringText.ENTERPASSWORD,Toast.SHORT);
        }else{
        this.setState({isLoading: true});
        Service.signupUser(Constant.resgisterWithcard,this.state.firstname,this.state.telephone,this.state.lastname
        ,this.state.emailadress,this.state.address,this.state.city,this.state.postalcode,this.state.countryValue,
        this.state.creatpasswrd,this.state.state,this.state.cartarray)
        .then(async(responseJson)=>{
            this.setState({isLoading: false});
            Toast.show(responseJson.message,Toast.SHORT);
            if(responseJson.status==1)
            {
              //session store data
              await AsyncStorage.setItem("customer_id",responseJson.userdata.customer_id);
              await AsyncStorage.setItem("customer_firstname",responseJson.userdata.customer_firstname);
              await AsyncStorage.setItem("customer_lastname",responseJson.userdata.customer_lastname);
              await AsyncStorage.setItem("card_id",responseJson.userdata.card_id);
              await AsyncStorage.setItem("array","");
              this.props.navigation.navigate('Home');
            }
        })
        }
    }
   //handle android hardware back button
   handleBackButton = () => {
     this.props.navigation.navigate('Home');
      return true;
    } 
     //login calling webservice
     login(){
        if(this.state.email=="")
        {
             Toast.show(Constant.StringText.ENTEREMAIL,Toast.SHORT);
        }else if(this.state.password=="")
        {
              Toast.show(Constant.StringText.ENTERPASSWORD,Toast.SHORT);
        }else{
         this.setState({isLoading: true});
         Service.loginUser(Constant.loginservice,this.state.email,this.state.password,this.state.cartarray).then(async(responseJson)=>{
             this.setState({isLoading: false});
             Toast.show(responseJson.message,Toast.SHORT);
             if(responseJson.status==1){
               try{
                   //session store data
                 await AsyncStorage.setItem("customer_id",responseJson.userdata.customer_id);
                 await AsyncStorage.setItem("customer_firstname",responseJson.userdata.customer_firstname);
                 await AsyncStorage.setItem("customer_lastname",responseJson.userdata.customer_lastname);
                 await AsyncStorage.setItem("customer_email",responseJson.userdata.customer_email);
                 await AsyncStorage.setItem("card_id",responseJson.userdata.card_id);
                 await AsyncStorage.setItem("array","");
                 this.props.navigation.navigate('Home');
               }catch(error)
               {
                   Toast.show(error,Toast.SHORT);
               }
             }
         })
        }
     }
    //list seperator
    renderSeparator = () => {
        return ( <View style={Constant.styles.separator}/>);
    };
 
    render() {
        return (
            this.state.isLoading?
            <View style={Constant.styles.indicatoreViewsTyle}> 
                <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
            </View>
            :
            <KeyboardAvoidingView style={{width:'100%',height:'100%'}} keyboardVerticalOffset={Constant.offset} contentContainerStyle={{height:'100%',width:'100%'}} behavior={'padding'} enabled>
            <View style={{flex:1,backgroundColor:'#ffffff'}}>      
                {(this.state.customer_id==null||this.state.customer_id=="")?
                <ScrollView  ref={(scroller) => {this.scroller = scroller}}>
                <View>
                <View style={Constant.styles.m10Top}></View>
                <Text style={Constant.styles.blackboltext}>{Constant.StringText.WELCOME}</Text>
                <View style={Constant.styles.m10Top}></View>
                <View style={Constant.styles.addressborderbox}>
                <RadioGroup selectedIndex={1} onSelect = {(index, value) => this.setState({radioValue:value})} size={15} thickness={2} color='lightgrey'>
                   <RadioButton value={'signup'} style={{flex:1}} color='#006d50'>
                    <Text style={Constant.styles.radioText}>{Constant.StringText.CREATANACCOUNT}</Text>
               
                    {this.state.radioValue=="signup"?
                   /* signup form */
                    <View style={Constant.styles.signinFormView}>
                     <Text style={Constant.styles.blackboldlefttext}>{Constant.StringText.CREATENEWCCOUNT}</Text>
                    <View style={{flexDirection:'row',flex:1,width:'100%',marginTop:5}}>
                        <View style={Constant.styles.filedViewsignup}>
                        <View style={{flexDirection:'row'}}>
                            <Text style={Constant.styles.listSmallText}>{Constant.StringText.FIRSTNAME}</Text><Text style={Constant.styles.redregulartext}>*</Text>
                        </View>
                        <TextInput onChangeText={firstname => this.setState({firstname})} placeholder={Constant.StringText.FIRSTNAME} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                        <View style={{flexDirection:'row'}}>
                            <Text style={Constant.styles.listSmallText}>{Constant.StringText.TELEPHONE}</Text><Text style={Constant.styles.redregulartext}>*</Text>
                        </View>
                        <TextInput keyboardType={'phone-pad'} onChangeText={telephone => this.setState({telephone})} placeholder={Constant.StringText.TELEPHONE} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                        </View>
                        <View style={{flex:1,flexDirection:'column'}}>
                        <View style={{flexDirection:'row'}}>
                            <Text style={Constant.styles.listSmallText}>{Constant.StringText.LASTNAME}</Text><Text style={Constant.styles.redregulartext}>*</Text>
                        </View>
                        <TextInput onChangeText={lastname => this.setState({lastname})} placeholder={Constant.StringText.LASTNAME} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                        <View style={{flexDirection:'row'}}>
                            <Text style={Constant.styles.listSmallText}>{Constant.StringText.EMAILADDRESS}</Text><Text style={Constant.styles.redregulartext}>*</Text>
                        </View>
                        <TextInput onChangeText={emailadress => this.setState({emailadress})} placeholder={Constant.StringText.EMAILADDRESS} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                        </View>
                    </View>
                    <View style={{flexDirection:'row'}}>
                    <Text style={Constant.styles.listSmallText}>{Constant.StringText.ADDRESS}</Text><Text style={Constant.styles.redregulartext}>*</Text>
                    </View>
                    <TextInput onChangeText={address => this.setState({address})} placeholder={Constant.StringText.ADDRESS} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                    <View style={{flexDirection:'row'}}>
                    <View style={Constant.styles.filedViewsignup}>
                        <View style={{flexDirection:'row'}}>
                            <Text style={Constant.styles.listSmallText}>{Constant.StringText.CITY}</Text><Text style={Constant.styles.redregulartext}>*</Text>
                        </View>
                        <TextInput onChangeText={city => this.setState({city})} placeholder={Constant.StringText.CITY} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                        <View style={{flexDirection:'row'}}>
                            <Text style={Constant.styles.listSmallText}>{Constant.StringText.STATEPROVIENCE}</Text><Text style={Constant.styles.redregulartext}>*</Text>
                        </View>
                        <TextInput onChangeText={state => this.setState({state})} placeholder={Constant.StringText.STATEPROVIENCE} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                        </View>
                        <View style={{flex:1,flexDirection:'column'}}>
                        <View style={{flexDirection:'row'}}>
                            <Text style={Constant.styles.listSmallText}>{Constant.StringText.COUNTRY}</Text><Text style={Constant.styles.redregulartext}>*</Text>
                        </View>
                        { Platform.OS=='ios'?
                         <View>
                            <RNPickerSelect placeholder={{label: 'Select country',value:"0",}}  
                                items={this.state.countryArray}                                
                                onValueChange={(value) => {this.setState({countryValue: value});   
                                    }}                                
                                style={Constant.pickerStylebox}                                 
                                value={this.state.countryValue}                                                     
                            />
                         </View>
                         :
                         <View style={Constant.styles.boxwithborder}>
                            <Picker
                                selectedValue={this.state.countryValue}
                                mode='dialog'
                                style={Constant.styles.colorPickerStyle}
                                onValueChange={(itemValue, itemIndex) => this.setState({countryValue: itemValue})}>
                                <Picker.Item label={this.state.country} value="0" />
                                {this.state.countryArray.map(function(item,key){
                                    return(
                                    <Picker.Item style={Constant.styles.listSmallText} key={key}
                                     label={item.label} value={item.value}/>
                                    );
                                    })
                                }
                            </Picker>
                         </View>
                        }
                        <View style={{flexDirection:'row'}}>
                            <Text style={Constant.styles.listSmallText}>{Constant.StringText.POSTALCODE}</Text><Text style={Constant.styles.redregulartext}>*</Text>
                        </View>
                        <TextInput onChangeText={postalcode => this.setState({postalcode})} placeholder={Constant.StringText.POSTALCODE} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                       </View>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <Text style={Constant.styles.listSmallText}>{Constant.StringText.PASSWORD}</Text><Text style={Constant.styles.redregulartext}>*</Text>
                        </View>
                        <TextInput onChangeText={creatpasswrd => this.setState({creatpasswrd})} secureTextEntry={true} placeholder={Constant.StringText.PASSWORD} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                       <View style={Constant.styles.m10Top}></View>
                        <TouchableHighlight style={Constant.styles.orangebutton} onPress={()=>this.register()}>
                          <Text style={Constant.styles.whitebuttontext}>{Constant.StringText.CREATANACCOUNT}</Text>
                        </TouchableHighlight>
                    </View>
                    :null}
                    </RadioButton>
                    <RadioButton value={'signin'} color='#006d50'>
                    <Text style={Constant.styles.radioText}>{Constant.StringText.SIGNIN}</Text>
                    {this.state.radioValue=="signin"?
                    /* login form */
                    <View style={Constant.styles.signinFormView}>
                    <Text style={Constant.styles.blackboldlefttext}>{Constant.StringText.IFYOUHAVEANACCOUNT}</Text>
                    <View flexDirection='row' style={{marginTop:5}}><Text style={Constant.styles.listSmallText}>{Constant.StringText.EMAILADDRESS}</Text>
                        <Text style={Constant.styles.listSmallRedText}>{Constant.StringText.ASTERISK}</Text></View>
                    <TextInput onChangeText={email => this.setState({email})} placeholder={Constant.StringText.EMAILPLACHOLDER} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                    <View flexDirection='row'><Text style={Constant.styles.listSmallText}>{Constant.StringText.PASSWORD}</Text>
                        <Text style={Constant.styles.listSmallRedText}>{Constant.StringText.ASTERISK}</Text></View>
                    <TextInput onChangeText={password => this.setState({password})} secureTextEntry={true} placeholder={Constant.StringText.PASSWORD} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                    <View style={Constant.styles.m10Top}></View>
                    <TouchableHighlight style={Constant.styles.orangebutton} onPress={()=>this.login()}>
                        <Text style={Constant.styles.whitebuttontext}>{Constant.StringText.SECUREINSECURLY}</Text>
                    </TouchableHighlight>
                    <View style={Constant.styles.m10Top}></View>
                    <Text style={Constant.styles.greenTextWithline} onPress={()=>this.functionToOpenSecondActivity('ForgotPassword')}>{Constant.StringText.FORGOTPASSWORD}</Text>
                    </View>:null}
                    </RadioButton>
                </RadioGroup>
                </View>
                </View>
                </ScrollView>
                :
                /* when customer loggedin /availble */
                <ScrollView  ref={(scroller) => {this.scroller = scroller}} style={{flex:1}}>
                    <View style={Constant.styles.ViewAcc}>
                        <Text style={Constant.styles.blackboltext}>{Constant.StringText.MYDASHBOARD}</Text>
                        <Text style={Constant.styles.darkgreenText}>Hello {this.state.customer_firstname} {this.state.customer_lastname},</Text>
                        <Text style={Constant.styles.listSmallText}>{Constant.StringText.DASHBOARDTEXT}</Text>
                        <View style={Constant.styles.lightGreenPanelAcc}>
                        <Text style={Constant.styles.whitenormalleftext}>{Constant.StringText.CONTACTINFORMATION}</Text>
                        <Text style={Constant.styles.whitebuttontext} onPress={()=>this.functionToOpenSecondActivity('EditAccountInform')}>{Constant.StringText.EDIT}</Text>
                        </View>
                        <View style={Constant.styles.m10Top}/>
                        <View style={{flexDirection:'row'}}>
                            <Text style={Constant.styles.listSmallText}>{this.state.customer_firstname}</Text>
                            <Text style={Constant.styles.listSmallText}>{this.state.customer_lastname}</Text>
                        </View>
                        <Text style={Constant.styles.listSmallText}>{this.state.customer_email}</Text>
                        <Text style={Constant.styles.greenTextWithline} onPress={()=>{this.functionToOpenSecondActivity('EditAccountInform')}}>{Constant.StringText.CHANGEPASSWORD}</Text>
                        <View style={{flexDirection:'column',flex:1}}>
                            <Panel title="My Address">
                                <TouchableHighlight style={Constant.styles.darkGreenbutton} onPress={()=>this.addressActivity('AddAddress',{data:"",type:"default"})}>
                                        <Text style={Constant.styles.whitebuttontext}>{Constant.StringText.ADDNEWADDRESS.toUpperCase()}</Text>
                                </TouchableHighlight>
                                {this.state.additionaladdress.length!=0?
                                    <View style={{flexDirection:'column'}}>
                                        {this.state.additionaladdress.map((item,i)=>{
                                                return(
                                                <View key={i} style={Constant.styles.addressborderbox}>
                                                    <View style={{flexDirection:'row'}}>
                                                        <View style={{flex:1,flexDirection:'row'}}>
                                                            <Text style={Constant.styles.listSmallText}>{i+1}. {item.firstname} {item.lastname}</Text>
                                                            <Text style={Constant.styles.tagTextView}>Default</Text>
                                                        </View>
                                                        <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end'}}>
                                                            <TouchableHighlight onPress={()=>this.addressActivity('AddAddress',{data:item,type:"default"})}>
                                                                <EditICon name={"edit"} size={25} color={'#98c954'}/>
                                                            </TouchableHighlight>
                                                            <Text style={Constant.styles.listSmallText}>|</Text>
                                                            <TouchableHighlight onPress={()=>{this.deletAddress(item.address_id)}}>
                                                                <DeletIcon name={"delete-forever"} size={25} color={'#000000'}/>
                                                            </TouchableHighlight>
                                                        </View>
                                                    </View>
                                                    <Text style={Constant.styles.listSmallText}>{item.street}</Text>
                                                    <Text style={Constant.styles.listSmallText}>{item.city},{item.region},{item.postcode}</Text>
                                                    <Text style={Constant.styles.listSmallText}>{item.country}</Text>
                                                    <Text style={Constant.styles.listSmallText}>T: {item.telephone}</Text>
                                                </View>
                                                );
                                            })
                                        }
                                    
                                    </View>
                                :null}
                                {this.state.billing_address.length!=0?
                                    <View style={{flexDirection:'column'}}>
                                        {this.state.billing_address.map((item,i)=>{
                                                return(
                                                <View key={i} style={Constant.styles.addressborderbox}> 
                                                    <View style={{flexDirection:'row'}}>
                                                        <View style={{flex:1,flexDirection:'row'}}>
                                                            <Text style={Constant.styles.listSmallText}>{i+1}. {item.firstname} {item.lastname}</Text>
                                                            <Text style={Constant.styles.tagTextView}>Billing</Text>
                                                        </View>
                                                        <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end'}}>
                                                            <TouchableHighlight onPress={()=>this.addressActivity('AddAddress',{data:item,type:"billing"})}>
                                                                <EditICon name={"edit"} size={25} color={'#98c954'}/>
                                                            </TouchableHighlight>
                                                            <Text style={Constant.styles.listSmallText}>|</Text>
                                                            <TouchableHighlight onPress={()=>{this.deletAddress(item.address_id)}}>
                                                                <DeletIcon name={"delete-forever"} size={25} color={'#000000'}/>
                                                            </TouchableHighlight>
                                                        </View>
                                                    </View>
                                                    <Text style={Constant.styles.listSmallText}>{item.street}</Text>
                                                    <Text style={Constant.styles.listSmallText}>{item.city},{item.region},{item.postcode}</Text>
                                                    <Text style={Constant.styles.listSmallText}>{item.country}</Text>
                                                    <Text style={Constant.styles.listSmallText}>T: {item.telephone}</Text>
                                                </View>
                                                );
                                            })
                                        }
                                </View>
                                :null}
                                {this.state.shipping_address.length!=0?
                                <View style={{flexDirection:'column'}}>
                                    {this.state.shipping_address.map((item,i)=>{
                                            return(
                                            <View key={i} style={Constant.styles.addressborderbox}>
                                                <View style={{flexDirection:'row'}}>
                                                    <View style={{flex:1,flexDirection:'row'}}>
                                                        <Text style={Constant.styles.listSmallText}>{i+1}. {item.firstname} {item.lastname}</Text>
                                                        <Text style={Constant.styles.tagTextView}>Shipping</Text>
                                                    </View>
                                                    <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end'}}>
                                                        <TouchableHighlight onPress={()=>this.addressActivity('AddAddress',{data:item,type:"shipping"})}>
                                                            <EditICon name={"edit"} size={25} color={'#98c954'}/>
                                                        </TouchableHighlight>
                                                        <Text style={Constant.styles.listSmallText}>|</Text>
                                                        <TouchableHighlight onPress={()=>{this.deletAddress(item.address_id)}}>
                                                            <DeletIcon name={"delete-forever"} size={25} color={'#000000'}/>
                                                        </TouchableHighlight>
                                                    </View>
                                                </View>
                                                <Text style={Constant.styles.listSmallText}>{item.street}</Text>
                                                <Text style={Constant.styles.listSmallText}>{item.city},{item.region},{item.postcode}</Text>
                                                <Text style={Constant.styles.listSmallText}>{item.country}</Text>
                                                <Text style={Constant.styles.listSmallText}>T: {item.telephone}</Text>
                                            </View>
                                            );
                                        })
                                    }
                                    
                                </View>
                                :null}
                        </Panel> 
                        <Panel title={Constant.StringText.MYORDER}>
                            {this.state.myorders.length!=0?
                            <View style={{flex:1}}>
                                <FlatList
                                keyExtractor = {( item, index ) => index }
                                data={this.state.myorders} 
                                renderItem={this.renderItemOrder.bind(this)}
                                ItemSeparatorComponent={this.renderSeparator}
                                />
                            </View>
                            : <Text style={Constant.styles.blacknormalcenterText}>{Constant.StringText.NODATAAVAILBLE}</Text>
                            }
                        </Panel>
                        <Panel title={Constant.StringText.MYPRODUCTREVIEW}>
                            {this.state.recentlyvieweditems.length!=0?
                            <View style={{flex:1}}>
                                <FlatList
                                keyExtractor = {( item, index ) => index }
                                data={this.state.recentlyvieweditems}
                                renderItem={this.renderItemRecent.bind(this)}
                                ItemSeparatorComponent={this.renderSeparator}
                                />
                            </View>
                            : <Text style={Constant.styles.blacknormalcenterText}>{Constant.StringText.NODATAAVAILBLE}</Text>
                            }
                        </Panel>
                        <Panel title={Constant.StringText.MYWISHLIST}>
                            {this.state.wishlistitems.length!=0?
                            <FlatList
                                keyExtractor = {( item, index ) => index }
                                data={this.state.wishlistitems}
                                renderItem={this.renderItemWishlist.bind(this)}
                                ItemSeparatorComponent={this.renderSeparator}/>
                            
                            : <Text style={Constant.styles.blacknormalcenterText}>{Constant.StringText.NODATAAVAILBLE}</Text>
                            }
                        </Panel> 
                        <Panel title={Constant.StringText.NEWSLETTERSUBSCRIPTION}>
                        <CheckBox onChange={(e) =>this.setState({newscheckbox:e})} label={Constant.StringText.GENERALSUBSCRIPTION} labelStyle={Constant.styles.listitemNameParent}/>
                            <TouchableHighlight style={Constant.styles.darkGreenbutton} onPress={()=>this.newssubscription()}>
                                <Text style={Constant.styles.whitebuttontext}>{Constant.StringText.SAVE}</Text>
                            </TouchableHighlight>
                        </Panel>
                       
                        <Panel title={Constant.StringText.DELETEACCOUNT}>
                            <Text style={Constant.styles.listSmallText}>{Constant.StringText.DELETEACCOUNTTEXT}</Text>
                            <TouchableHighlight style={Constant.styles.darkGreenbutton} onPress={()=>this.deleteAccount()}>
                                <Text style={Constant.styles.whitebuttontext}>{Constant.StringText.DELETMYACCOUNT.toUpperCase()}</Text>
                            </TouchableHighlight>
                        </Panel>
                        </View>
                    </View>
                </ScrollView>}
            <TouchableOpacity style={Constant.styles.stickybuttonstart} onPress={this.scrollToTop}>
              <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
            </TouchableOpacity>
            </View>
            </KeyboardAvoidingView>
        );
    }
        //top of screen
        scrollToTop = () => {
            this.scroller.scrollTo({x: 0, y: 0});
        };
    //render item for listview wishlist
    renderItemWishlist(data) {
        let { item, index } = data;  
        return (
          <View style={Constant.styles.listcontainer}>
              <TouchableOpacity activeOpacity={0.7} style={Constant.styles.listImageView} onPress={() => {this.productDetailActivity('ProductDetail',{product_id:item.product_id,quantity:item.quantity})}}>
                <Image source={{uri:item.image}} style={Constant.styles.listImage} />
              </TouchableOpacity>
            <View style={Constant.styles.m2Left}> 
                <Text style={Constant.styles.listSmallText}>{item.name.toUpperCase()}</Text>
                <View style={Constant.styles.listRow}> 
                  {item.size!=null?
                    <Text style={Constant.styles.listSmallText}>{Constant.StringText.SIZE}{item.size}</Text>
                  :null}
                  <Text style={Constant.styles.listSmallText}>{Constant.StringText.QTY}{item.quantity}</Text>
                </View>
                
                {item.special_price==null?
                <View style={Constant.styles.listRow}> 
                  <Text style={Constant.styles.listSmallText}>{Constant.StringText.NOW}{parseFloat(item.price).toFixed(2)}</Text>
                </View>
                :
                <View style={Constant.styles.listRow}> 
                  <Text style={Constant.styles.listSmallRedText}>{Constant.StringText.NOW}{parseFloat(item.special_price).toFixed(2)}</Text>
                  <Text style={Constant.styles.crossText}>{Constant.StringText.WAS}{parseFloat(item.price).toFixed(2)}</Text>
                </View>
                }
            </View> 
              <TouchableHighlight style={{alignSelf:'flex-start'}} onPress={()=>{this.deletProduct("wishlist",item.product_id)}}>
                <CrossIcon name={"cross"} size={25} color={'#000000'}/>
              </TouchableHighlight>
          </View>
         ) 
    }

    

    //render item for listview recent
    renderItemRecent(data) {
        let { item, index } = data;  
        return (
          <View style={Constant.styles.listcontainer}>
              <TouchableOpacity activeOpacity={0.7} style={Constant.styles.listImageView} onPress={() => {this.productDetailActivity('ProductDetail',{product_id:item.product_id,quantity:item.quantity})}}>
                <Image source={{uri:item.image}} style={Constant.styles.listImage} />
              </TouchableOpacity>
            <View style={Constant.styles.m2Left}> 
                <Text style={Constant.styles.listSmallText}>{item.name.toUpperCase()}</Text>
                <View style={Constant.styles.listRow}> 
                  {item.size!=null?
                    <Text style={Constant.styles.listSmallText}>{Constant.StringText.SIZE}{item.size}</Text>
                  :null}
                  <Text style={Constant.styles.listSmallText}>{Constant.StringText.QTY}{item.quantity}</Text>
                </View>
                
                {item.special_price==null?
                <View style={Constant.styles.listRow}> 
                  <Text style={Constant.styles.listSmallText}>{Constant.StringText.NOW}{parseFloat(item.price).toFixed(2)}</Text>
                </View>
                :
                <View style={Constant.styles.listRow}> 
                  <Text style={Constant.styles.listSmallRedText}>{Constant.StringText.NOW}{parseFloat(item.special_price).toFixed(2)}</Text>
                  <Text style={Constant.styles.crossText}>{Constant.StringText.WAS}{parseFloat(item.price).toFixed(2)}</Text>
                </View>
                }
            </View> 
              <TouchableHighlight style={{alignSelf:'flex-start'}} onPress={()=>{this.deletProduct("recentview",item.product_id)}}>
                <CrossIcon name={"cross"} size={25} color={'#000000'}/>
              </TouchableHighlight>
          </View>
         ) 
    }

    //render item for listview order
    renderItemOrder(data) {
        let { item, index } = data;  
        return (
            <TouchableOpacity onPress={()=>this.orderActivity('Orderdetail',{order_id:item.order_id})}>
            <View style={Constant.styles.container}>
               <View style={{flexDirection:'row'}}>
                  <View style={{flexDirection:'row',flex:1,alignItems:'flex-start'}}><Text style={Constant.styles.blackboldlefttext}>{Constant.StringText.ORDERID} {item.order_id}</Text></View>
                  <View style={{flexDirection:'row',flex:1}}><Text style={Constant.styles.greennormelrightText}>{item.status}</Text></View>
                </View>
                <Text style={Constant.styles.listSmallText}>{Constant.StringText.DATE} {item.date}</Text>
                <Text style={Constant.styles.listSmallText}>{Constant.StringText.SHIPTO} {item.ship_to}</Text>
                <Text style={Constant.styles.listSmallText}>{Constant.StringText.ORDERTOTAL} {Constant.Currency}{parseFloat(item.order_total).toFixed(2)}</Text>   
            </View>
            </TouchableOpacity>
        ) 
    } 
    //navigate to another activity
    functionToOpenSecondActivity = (SCREEN) =>
    {
    this.props.navigation.navigate(SCREEN);
    }

    //navigate to address activity
    addressActivity = async(SCREEN,data)=>
    {
        if(data=="")
        {
            await AsyncStorage.setItem("dataaddress","");
        }
        await AsyncStorage.setItem("dataaddress",JSON.stringify(data.data)); 
        await AsyncStorage.setItem("type",JSON.stringify(data.type));       
        this.props.navigation.navigate(SCREEN);
    }

    orderActivity=async(SCREEN,data)=>{
        await AsyncStorage.setItem("order_id",data.order_id); 
        this.props.navigation.navigate(SCREEN);   
    }

    //navigate another product detail with param
    productDetailActivity = (SCREEN,data) => {
        if(data.product_id!==undefined && typeof data.product_id!="undefined"){
        this.props.navigation.navigate(SCREEN,data);
        }
        else if(data!==undefined && typeof data!="undefined")
        {
            this.props.navigation.navigate(SCREEN,data);
        }
    } 

    //get user profile
    getUserProfile(){
        Service.getUserDetail(Constant.getUserProfile,this.state.customer_id).then((responseJson)=>
        {
            this.setState({isLoading: false});
            if(responseJson.status==1)
            {
                try{
                    this.setState({
                        billing_address:responseJson.billing_address,
                        shipping_address:responseJson.shipping_address,
                        additionaladdress:responseJson.additionaladdress,
                        wishlistitems:responseJson.wishlistitems,
                        customer_email:responseJson.userdata.customer_email,
                        customer_firstname:responseJson.userdata.customer_firstname,
                        customer_lastname:responseJson.userdata.customer_lastname,
                        recentlyvieweditems:responseJson.recentlyvieweditems,
                        newsletter_subscribe:responseJson.newsletter_subscribe,
                        myorders:responseJson.myorders })
                }catch(error)
                { 
                    Toast.show(error,Toast.SHORT); 
                }
            }else
            {
                Toast.show(responseJson.message,Toast.SHORT);
            }
        });
    }
    //delete address
    deletAddress(address_id)
    {
         Alert.alert(
        'Alert!',
        'Are you sure want delete this address. ',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'OK', onPress: () =>
          {
            this.setState({isLoading: true});
            Service.deletAddress(Constant.deletecustomer_address,this.state.customer_id,address_id)
            .then((responseJson)=>{
              this.setState({isLoading: false});
              Toast.show(responseJson.message,Toast.SHORT);
              if(responseJson.status==1)
              {
                  this.getUserProfile();
              }
            })
          }
         },
        ],
        { cancelable: false }
      ) 
    }
 
    newssubscription()
    {
        this.setState({isLoading: true});
        if(this.state.newscheckbox==true)
        {
            Service.cutomernewssubscribe(Constant.customer_emailsubscribe,this.state.customer_id,1)
            .then((responseJson)=>{
                this.setState({isLoading: false});
                Toast.show(responseJson.message,Toast.SHORT);
                if(responseJson.status==1)
                {

                }
              })
        }else{
            this.setState({isLoading: true});
            Service.cutomernewssubscribe(Constant.customer_emailsubscribe,this.state.customer_id,0)
            .then((responseJson)=>{
                this.setState({isLoading: false});
                Toast.show(responseJson.message,Toast.SHORT);
                if(responseJson.status==1)
                {

                }
              })
        }
    }
    //delete account
    deleteAccount()
    {
        Alert.alert(
            'Alert!',
            'Are you sure want delete your Account. ',
            [
              {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
              {text: 'OK', onPress: () =>
                {
                this.setState({isLoading: true});
                Service.getUserDetail(Constant.deleteAccount,this.state.customer_id).then(async(responseJson)=>
                {
                    this.setState({isLoading: false});
                    Toast.show(responseJson.message,Toast.SHORT);
                    if(responseJson.status==1)
                    {
                        await AsyncStorage.setItem("customer_id","");
                        await AsyncStorage.setItem("customer_firstname","");
                        await AsyncStorage.setItem("customer_lastname","");
                        await AsyncStorage.setItem("customer_email","");
                        await AsyncStorage.setItem("card_id","");
                        this.props.navigation.navigate('Home');
                    }
                });
                } 
            },
            ],
            { cancelable: false }
          ) 
    }
    //delete product from list
    deletProduct=(name,product_id)=> {
        Alert.alert(
        'Alert!',
        'Are you sure want delete this item. ',
        [
            {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
            {text: 'OK', onPress: () =>
                {
                    this.setState({isLoading: true});
                    if(name=="wishlist")
                    {
                        Service.deletAddProduct(Constant.deletewishlistitem,this.state.customer_id,product_id).then(async(responseJson)=>
                        {
                            Toast.show(responseJson.message,Toast.SHORT);
                            if(responseJson.status==1)
                            {
                                var wishlistitems = [...this.state.wishlistitems]
                                let index = wishlistitems.indexOf(product_id);
                                wishlistitems.splice(index, 1);
                                this.setState({ wishlistitems,isLoading:false });
                                this.getUserProfile();
                            }
                        });
                    }else if(name=="recentview")
                    {
                        Service.deletAddProduct(Constant.deletrecentviewitem,this.state.customer_id,product_id).then(async(responseJson)=>
                        {
                            Toast.show(responseJson.message,Toast.SHORT);
                            if(responseJson.status==1)
                            {
                                var recentlyvieweditems = [...this.state.recentlyvieweditems]
                                let index = recentlyvieweditems.indexOf(product_id);
                                recentlyvieweditems.splice(index, 1);
                                this.setState({ recentlyvieweditems,isLoading:false });
                                this.getUserProfile();
                            }
                        });
                    }
                   
                } 
            },
        ],
        { cancelable: false }
        ) 
    }
}