import React, { Component } from 'react';
import {TouchableOpacity,TextInput,Text,View,ScrollView,TouchableHighlight,Picker,AsyncStorage,
    KeyboardAvoidingView,ActivityIndicator,Platform,StyleSheet,BackHandler} from 'react-native';
import Constant from '../../../Constant/Constant'
import CheckBox from 'react-native-checkbox';
import Toast from 'react-native-simple-toast';
import Service from '../../config/Service';
import RNPickerSelect from 'react-native-picker-select'; 
import TopArrow from 'react-native-vector-icons/FontAwesome'


export default class AddAddress extends Component {
    constructor(props) {
        super(props);
        this.inputRefs={};
        this.state = {
            address_id:"",
            firstname:"",
            lastname:"",
            telephone:"",
            street:"",
            company:"",
            fax:"",
            state:"",
            city:"",
            postalcode:"",
            countryArray:[],
            countryValue: "",
            country:"",
            biilingvalue:"0",
            shiipingvalue:"0",
            customer_id:"",
            isLoading: false,
            addressType:"",
        }
    }

    async componentWillMount(){
       this.getCountry();
       var customer_id = await AsyncStorage.getItem("customer_id");
       var addressType = await AsyncStorage.getItem("type");
       var data = await AsyncStorage.getItem("dataaddress");
       var fname = await AsyncStorage.getItem("customer_firstname");
       var lname = await AsyncStorage.getItem("customer_lastname");
       this.setState({
        customer_id:customer_id,
        addressType:addressType.replace(/"/g, ""),
        address_id:JSON.parse(data).address_id,
        firstname:JSON.parse(data).firstname,
        lastname:JSON.parse(data).lastname,
        telephone:JSON.parse(data).telephone,
        street:JSON.parse(data).street,
        city:JSON.parse(data).city,
        state:JSON.parse(data).region,
        postalcode:JSON.parse(data).postcode,
        country:JSON.parse(data).country,
        });
        if(JSON.parse(data)=="")
        {
            this.setState({firstname:fname,lastname:lname,country:""});
        }
        if(this.state.addressType=="billing")
        {
            this.setState({biilingvalue:1});
        }else if(this.state.addressType=="shipping")
        {
            this.setState({shiipingvalue:1});
        }
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
 
    //hardware back button
    componentWillUnmount() {
    //handle tab click listener  remove
     BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
   }
      
    //harware mobile back button
    handleBackButton = () => {
    this.props.navigation.goBack('Account');
        return true;
    } 

    //get Country list
    getCountry(){
        this.setState({isLoading:true});
        Service.getdataFromService(Constant.countryList).then((responseJson)=>{
            this.setState({isLoading:false});
            var countryOptions = [];
            Object.keys(responseJson[0]).forEach(function(key) {
              countryOptions.push({'value':key,'label':responseJson[0][key]}); 
            });
            this.setState({countryArray:countryOptions});
        });
    }

    render() {
        return( 
        this.state.isLoading?
        <View style={Constant.styles.indicatoreViewsTyle}> 
            <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
        </View>
        :
        <KeyboardAvoidingView style={{width:'100%',height:'100%'}} keyboardVerticalOffset={Constant.offset} contentContainerStyle={{height:'100%',width:'100%'}} behavior={'padding'} enabled>
        <View style={Constant.styles.containerwithp10}>
        <ScrollView ref={(scroller) => {this.scroller = scroller}}>
        <View style={Constant.styles.signinFormView}>
                <View style={Constant.styles.lightGreenPanelAcc}>
                    <Text style={Constant.styles.whitenormalleftext}>{Constant.StringText.CONTACTINFORMATION}</Text>
                </View>
                <View style={{flexDirection:'row'}}>
                    <View style={Constant.styles.filedViewsignup}>
                        <View style={{flexDirection:'row'}}>
                            <Text style={Constant.styles.listSmallText}>{Constant.StringText.FIRSTNAME}</Text><Text style={Constant.styles.redregulartext}>*</Text>
                        </View>
                        <TextInput onChangeText={firstname => this.setState({firstname})} value={this.state.firstname} placeholder={Constant.StringText.FIRSTNAME} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                    </View>
                    <View style={{flex:1,flexDirection:'column'}}>
                        <View style={{flexDirection:'row'}}>
                            <Text style={Constant.styles.listSmallText}>{Constant.StringText.LASTNAME}</Text><Text style={Constant.styles.redregulartext}>*</Text>
                        </View>
                        <TextInput onChangeText={lastname => this.setState({lastname})} value={this.state.lastname} placeholder={Constant.StringText.LASTNAME} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                    </View>
                </View>
                <View style={{flex:1,flexDirection:'column'}}>
                    <View style={{flexDirection:'row'}}>
                        <Text style={Constant.styles.listSmallText}>{Constant.StringText.COMPANY}</Text>
                    </View>
                    <TextInput onChangeText={company => this.setState({company})} placeholder={Constant.StringText.COMPANY} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
            </View>
            <View style={{flexDirection:'row'}}>
                    <View style={Constant.styles.filedViewsignup}>
                        <View style={{flexDirection:'row'}}>
                            <Text style={Constant.styles.listSmallText}>{Constant.StringText.TELEPHONE}</Text><Text style={Constant.styles.redregulartext}>*</Text>
                        </View>
                        <TextInput keyboardType={'phone-pad'} onChangeText={telephone => this.setState({telephone})} value={this.state.telephone} placeholder={Constant.StringText.TELEPHONE} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                    </View>
                    <View style={{flex:1,flexDirection:'column'}}>
                        <View style={{flexDirection:'row'}}>
                            <Text style={Constant.styles.listSmallText}>{Constant.StringText.FAX}</Text>
                        </View>
                        <TextInput onChangeText={fax => this.setState({fax})} placeholder={Constant.StringText.FAX} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                    </View>
                </View>
            <View style={Constant.styles.m10Top}/>
            <View style={Constant.styles.lightGreenPanelAcc}>
                <Text style={Constant.styles.whitenormalleftext}>{Constant.StringText.ADDRESS}</Text>
            </View>
            <View style={{flexDirection:'column'}}>
            <View style={{flexDirection:'row'}}>
                    <Text style={Constant.styles.listSmallText}>{Constant.StringText.STREETADDRESS}</Text><Text style={Constant.styles.redregulartext}>*</Text>
                </View>
                <TextInput placeholder="" onChangeText={street => this.setState({street})} value={this.state.street} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                <View style={{flexDirection:'row'}}>
                    <View style={{flex:1,flexDirection:'column',marginRight:10}}>
                        <View style={{flexDirection:'row'}}>
                            <Text style={Constant.styles.listSmallText}>{Constant.StringText.CITY}</Text><Text style={Constant.styles.redregulartext}>*</Text>
                        </View>
                        <TextInput placeholder="" onChangeText={city => this.setState({city})} value={this.state.city} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                    </View>
                    <View style={{flex:1,flexDirection:'column'}}>
                        <View style={{flexDirection:'row'}}>
                            <Text style={Constant.styles.listSmallText}>{Constant.StringText.STATEPROVIENCE}</Text><Text style={Constant.styles.redregulartext}>*</Text>
                        </View>
                        <TextInput placeholder="" onChangeText={state => this.setState({state})} value={this.state.state} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                    </View>
                </View>
                <View style={{flexDirection:'row'}}>
                    <View style={{flex:1,flexDirection:'column',marginRight:10}}>
                        <View style={{flexDirection:'row'}}>
                            <Text style={Constant.styles.listSmallText}>{Constant.StringText.PINCODE}</Text><Text style={Constant.styles.redregulartext}>*</Text>
                        </View>
                        <TextInput placeholder="" onChangeText={postalcode => this.setState({postalcode})} value={this.state.postalcode} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                    </View>
                    <View style={{flex:1,flexDirection:'column'}}>
                        <View style={{flexDirection:'row'}}>
                            <Text style={Constant.styles.listSmallText}>{Constant.StringText.COUNTRY}</Text><Text style={Constant.styles.redregulartext}>*</Text>
                        </View>
                        { Platform.OS=='ios'?
                         <View>
                            <RNPickerSelect placeholder={{label: 'Select country',value: "0",}}  
                                items={this.state.countryArray}                                
                                onValueChange={(value) => {this.setState({countryValue: value});   
                                    }}                                
                                style={Constant.pickerStylebox}                                 
                                value={this.state.countryValue}                                 
                                ref={(el) => {                                     
                                    this.inputRefs.picker = el;                                
                                }}                             
                            />
                         </View>
                         :
                         <View style={Constant.styles.boxwithborder}>
                            <Picker
                                selectedValue={this.state.countryValue}
                                mode='dialog'
                                style={Constant.styles.colorPickerStyle}
                                onValueChange={(itemValue, itemIndex) => this.setState({countryValue: itemValue})}>
                                <Picker.Item label={this.state.country} value="0" />
                                {this.state.countryArray.map(function(item,key){
                                    return(
                                    <Picker.Item style={Constant.styles.listSmallText} key={key}
                                     label={item.label} value={item.value}/>
                                    );
                                    })
                                }
                            </Picker>
                         </View>
                        }
                    </View>
                </View>
            </View>
                <View style={Constant.styles.viewStyle}></View>
               <View>{this.renderBottom()}</View>
                <View style={Constant.styles.m10Top}></View>
                <TouchableHighlight style={Constant.styles.orangebutton} onPress={()=> this.addAddress()}>
                    <Text style={Constant.styles.whitebuttontext}>{Constant.StringText.SAVE}</Text>
                </TouchableHighlight>
        </View>
        </ScrollView>
        <TouchableOpacity style={Constant.styles.stickybutton} onPress={this.scrollToTop}>
              <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
        </TouchableOpacity>
        </View>
        </KeyboardAvoidingView> 
        );
    }

    //top of screen
    scrollToTop = () => {
        this.scroller.scrollTo({x: 0, y: 0});
    };
   
    //for bottom checkbox
    renderBottom()
    {
       if(this.state.addressType=="default")
        {
            return(
                <View style={{flexDirection:'column'}}>
                    <CheckBox onChange={(e) => this.billingaddressChange(e)} label={Constant.StringText.USEMYDEFAULTBILLING} labelStyle={Constant.styles.listSmallText}/>
                    <CheckBox onChange={(e) => this.shippingaddressChange(e)} label={Constant.StringText.USEMYDEFAULTSHIPPING} labelStyle={Constant.styles.listSmallText}/>
                </View>
            )
        }else if(this.state.addressType=="billing")
        {
            //this.setState({biilingvalue:1});
            return(
                <View style={{flexDirection:'column'}}>
                    <Text style={Constant.styles.darkgreenText}>{Constant.StringText.DEFAULTBIILINGADD}</Text>
                    <CheckBox onChange={(e) => this.shippingaddressChange(e)} label={Constant.StringText.USEMYDEFAULTSHIPPING} labelStyle={Constant.styles.listSmallText}/>
                </View>
            )
        }else if(this.state.addressType=="shipping")
        {
           // this.setState({shiipingvalue:1});
            return(
                <View style={{flexDirection:'column'}}>
                    <Text style={Constant.styles.darkgreenText}>{Constant.StringText.DEFAULTSHIPPINGADD}</Text>
                    <CheckBox onChange={(e) => this.billingaddressChange(e)} label={Constant.StringText.USEMYDEFAULTBILLING} labelStyle={Constant.styles.listSmallText}/>
                </View>
            )
        }else{
            return(
                <View style={{flexDirection:'column'}}>
                    <CheckBox onChange={(e) => this.billingaddressChange(e)} label={Constant.StringText.USEMYDEFAULTBILLING} labelStyle={Constant.styles.listSmallText}/>
                    <CheckBox onChange={(e) => this.shippingaddressChange(e)} label={Constant.StringText.USEMYDEFAULTSHIPPING} labelStyle={Constant.styles.listSmallText}/>
                </View>
            )
        }
    }
    //for billing value
    billingaddressChange(e)
    {
        if(e==true)
        {
            this.setState({biilingvalue:1});
        }else{
            this.setState({biilingvalue:0});
        }
    }
    //for shiping value
    shippingaddressChange(e)
    {
        if(e==true)
        {
            this.setState({shiipingvalue:1});
        }else{
            this.setState({shiipingvalue:0});
        }
    }
    //add address according to address type
    addAddress()
    {
        //validation for fields
        if(this.state.firstname==""||this.state.firstname==undefined)
        {
            Toast.show(Constant.StringText.ENTERFIRSTNAME,Toast.SHORT);
        }else if(this.state.lastname==""||this.state.lastname==undefined)
        {
            Toast.show(Constant.StringText.ENTERLASTNAME,Toast.SHORT);
        }else if(this.state.telephone==""||this.state.telephone==undefined)
        {
            Toast.show(Constant.StringText.ENTERTELEPHONE,Toast.SHORT);
        }else if(this.state.street==""||this.state.street==undefined)
        {
            Toast.show(Constant.StringText.ENTERSTREET,Toast.SHORT);
        }else if(this.state.city==""||this.state.city==undefined)
        {
            Toast.show(Constant.StringText.ENTERCITY,Toast.SHORT);
        }else if(this.state.state==""||this.state.state==undefined)
        { 
            Toast.show(Constant.StringText.ENTERSTATE,Toast.SHORT);
        }else if(this.state.postalcode==""||this.state.postalcode==undefined)
        {
            Toast.show(Constant.StringText.ENTERPOSTALCODE,Toast.SHORT);
        }else if(this.state.countryValue==""||this.state.countryValue==undefined&&this.state.countryValue==0)
        {
            Toast.show(Constant.StringText.SELECTCOUNTRY,Toast.SHORT);
        }else
        {
            this.setState({isLoading: true});
            if(this.state.biilingvalue=="1"&&this.state.shiipingvalue=="0")
            {
                Service.addAddress(Constant.addAddress,this.state.customer_id,this.state.firstname,this.state.company,this.state.telephone,
                this.state.lastname,this.state.fax,this.state.street,this.state.city,this.state.state,this.state.postalcode,
                this.state.countryValue,this.state.biilingvalue,this.state.shiipingvalue,this.state.address_id)
                .then((responseJson)=>
                {
                    this.setState({isLoading: false});
                    Toast.show(responseJson.message,Toast.SHORT);
                    if(responseJson.status==1)
                    {
                        Toast.show(Constant.StringText.SUCESSFULLYADDEDADDRESS,Toast.SHORT);
                        this.props.navigation.navigate('Account');
                    }
                });
            }
            else if(this.state.shiipingvalue=="1"&&this.state.biilingvalue=="0")
            {
                Service.addAddress(Constant.addAddress,this.state.customer_id,this.state.firstname,this.state.company,this.state.telephone,
                this.state.lastname,this.state.fax,this.state.street,this.state.city,this.state.state,this.state.postalcode,
                this.state.countryValue,this.state.biilingvalue,this.state.shiipingvalue,this.state.address_id)
                .then((responseJson)=>
                {
                    this.setState({isLoading: false});
                    Toast.show(responseJson.message,Toast.SHORT);
                    if(responseJson.status==1)
                    {
                        Toast.show(Constant.StringText.SUCESSFULLYADDEDADDRESS,Toast.SHORT);
                        this.props.navigation.navigate('Account');
                    }
                });
            }
            else if(this.state.biilingvalue=="1"&&this.state.shiipingvalue=="1")
            {
                Service.addAddress(Constant.addAddress,this.state.customer_id,this.state.firstname,this.state.company,this.state.telephone,
                this.state.lastname,this.state.fax,this.state.street,this.state.city,this.state.state,this.state.postalcode,
                this.state.countryValue,this.state.biilingvalue,this.state.shiipingvalue,this.state.address_id)
                .then((responseJson)=>
                {
                    this.setState({isLoading: false});
                    Toast.show(responseJson.message,Toast.SHORT);
                    if(responseJson.status==1)
                    {
                        Toast.show(Constant.StringText.SUCESSFULLYADDEDADDRESS,Toast.SHORT);
                        this.props.navigation.navigate('Account');
                    }
                });
            }
            else if(this.state.biilingvalue=="0"&&this.state.shiipingvalue=="0") 
            {
                Service.addAddress(Constant.addAddress,this.state.customer_id,this.state.firstname,this.state.company,this.state.telephone,
                this.state.lastname,this.state.fax,this.state.street,this.state.city,this.state.state,this.state.postalcode,
                this.state.countryValue,this.state.biilingvalue,this.state.shiipingvalue,this.state.address_id)
                .then((responseJson)=>
                {
                    this.setState({isLoading: false});
                    Toast.show(responseJson.message,Toast.SHORT);
                    if(responseJson.status==1)
                    {
                        Toast.show(Constant.StringText.SUCESSFULLYADDEDADDRESS,Toast.SHORT);
                        this.props.navigation.navigate('Account');
                    }
                });
            }
        }
    }
}