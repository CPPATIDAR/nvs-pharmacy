import React, { Component } from 'react';
import {Text,TouchableHighlight,ActivityIndicator,ScrollView,TextInput,View,BackHandler} from 'react-native';
import Constant from '../../../Constant/Constant'
import Toast from 'react-native-simple-toast';
import Service from '../../config/Service';

export default class ForgotPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email:"",
        }
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
    //hardware back button
    componentWillUnmount() {
    //handle tab click listener  remove
     BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
  
    //harware mobile back button
    handleBackButton = () => {
      this.props.navigation.goBack('Account');
      return true;
    } 
 
    //open next screen
    functionToOpenSecondActivity = (SCREEN) =>
    {
        if(this.state.email=="")
        {
            Toast.show(Constant.StringText.ENTEREMAIL,Toast.SHORT);

        }else{
            //calling webservice fetch data  
            this.setState({ isLoading: true });
            Service.forgotPassword(Constant.forgotPassword,this.state.email).then((responseJson)=>
            {
                this.setState({isLoading: false});
                if(responseJson.status==1)
                {
                    alert(responseJson.message);
                    this.props.navigation.navigate('Account');
                }else{
                    Toast.show(responseJson.message,Toast.SHORT);
                }
            });
        }
    }

    render() {
        return( 
            this.state.isLoading?
            <View style={Constant.styles.indicatoreViewsTyle}> 
                <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
            </View>
            :
            <View style={Constant.styles.signinTitle}>
                    <Text style={Constant.styles.blackboltext}>{Constant.StringText.FORGOTPASSWORD}</Text>
                    <View style={Constant.styles.viewStyle}></View>
                <View style={{paddingLeft:15,paddingRight:15}}>   
                    <ScrollView>
                    <View style={Constant.styles.signinFormView}>
                    <Text style={Constant.styles.blackboldlefttext}>{Constant.StringText.RETRIVEYOURPASSWORDHERE}</Text>
                    <View flexDirection='row'>
                        <Text style={Constant.styles.listSmallText}>{Constant.StringText.EMAILADDRESS}</Text>
                        <Text style={Constant.styles.listSmallRedText}>{Constant.StringText.ASTERISK}</Text>
                    </View>
                    <TextInput onChangeText={email => this.setState({email})} placeholder={Constant.StringText.EMAILPLACHOLDER} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                    <View style={Constant.styles.m10Top}></View>
                    <TouchableHighlight style={Constant.styles.orangebutton} onPress={()=>this.functionToOpenSecondActivity('LoginSignup')}>
                        <Text style={Constant.styles.whitebuttontext}>{Constant.StringText.SUBMIT}</Text>
                    </TouchableHighlight>
                    </View>
                    </ScrollView>
                 </View>
            </View>
        );
    }
}