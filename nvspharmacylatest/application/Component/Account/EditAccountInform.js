import React, { Component } from 'react';
import {TouchableOpacity,Text,TouchableHighlight,Image,ScrollView,TextInput,View,AsyncStorage,
    KeyboardAvoidingView,ActivityIndicator,BackHandler,Platform} from 'react-native';
import CheckBox from 'react-native-checkbox';
import Constant from '../../../Constant/Constant';
import Toast from 'react-native-simple-toast';
import Service from '../../config/Service';
import TopArrow from 'react-native-vector-icons/FontAwesome'
var  checkpassword="0";


export default class EditAccountInform extends Component {
    constructor(props) {
        super(props);
        this.state = {
            customer_id:"",
            first_name:"",
            last_name:"",
            email:"",
            change_password:false,
            current_password:"",
            new_password:"",
            confirm_password:"",
            isLoading: false,
        }   
    }

    //fetch customer_id from session
    async componentWillMount() {
    var customer_id = await AsyncStorage.getItem("customer_id");
    var customer_firstname = await AsyncStorage.getItem("customer_firstname");
    var customer_lastname = await AsyncStorage.getItem("customer_lastname");
    var customer_email = await AsyncStorage.getItem("customer_email");
    this.setState({customer_id:customer_id});
    this.setState({first_name:customer_firstname});
    this.setState({last_name:customer_lastname});
    this.setState({email:customer_email});
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
   }

   componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
   }

    // handle hardware back button
    handleBackButton = () => {
        this.props.navigation.navigate('Account');
         return true;
    } 

    //save calling webservice
    save(){
        //validation
       if(this.state.first_name=="")
       {
            Toast.show(Constant.StringText.ENTERFIRSTNAME,Toast.SHORT);
       }else if(this.state.last_name=="")
       {
        Toast.show(Constant.StringText.ENTERLASTNAME,Toast.SHORT);
       }else if(this.state.email=="")
       {
            Toast.show(Constant.StringText.ENTEREMAIL,Toast.SHORT);
       }
       else if(this.state.change_password)
       {
            checkpassword="1";
            if(this.state.current_password=="")
            {
                Toast.show(Constant.StringText.ENTERCURRENTPASSWORD,Toast.SHORT);
            }else if(this.state.new_password=="")
            {
                Toast.show(Constant.StringText.ENTERNEWPASSWORD,Toast.SHORT);
            }else if(this.state.confirm_password=="")
            {
                Toast.show(Constant.StringText.ENTERCONFIRMPASSWORD,Toast.SHORT);
            }else if(this.state.confirm_password!=this.state.new_password)
            {
                Toast.show(Constant.StringText.PASSWORDNOTMATCH,Toast.SHORT);
            }else{
                this.editProfile(checkpassword);
            }
       }else {
            this.editProfile(checkpassword);
       }
    }

    //edit profile by this function
    editProfile(checkpassword)
    {
        this.setState({ isLoading: true });
        Service.editProfile(Constant.editProfile,this.state.customer_id,this.state.first_name,
        this.state.last_name,this.state.email,this.state.current_password,this.state.new_password,checkpassword).then((responseJson)=>
        {
            this.setState({isLoading: false})
            Toast.show(responseJson.message,Toast.SHORT);
            if(responseJson.status==1)
            {
                this.props.navigation.navigate('Account');
            }
        
        })
    }
    render() {
        return( 
            this.state.isLoading?
            <View style={Constant.styles.indicatoreViewsTyle}> 
                <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
            </View>
            :
            <KeyboardAvoidingView style={{width:'100%',height:'100%'}} keyboardVerticalOffset={Constant.offset} contentContainerStyle={{height:'100%',width:'100%'}} behavior={'padding'} enabled>
            <View style={Constant.styles.signinTitle}>
                    <ScrollView ref={(scroller) => {this.scroller = scroller}} style={Constant.styles.containerwithp10}>
                    <Text style={Constant.styles.blackboltext}>{Constant.StringText.EDITACCINFORM}</Text>
                    <View style={Constant.styles.viewStyle}></View>
                    <View style={Constant.styles.signinFormView}>
                    <View style={Constant.styles.lightGreenPanelAcc}><Text style={Constant.styles.whitenormalleftext}>{Constant.StringText.ACCINFORMATION}</Text></View>
                    <View flexDirection='row'><Text style={Constant.styles.listSmallText}>{Constant.StringText.FIRSTNAME}</Text>
                           <Text style={Constant.styles.listSmallRedText}>{Constant.StringText.ASTERISK}</Text></View>
                    <TextInput onChangeText={first_name => this.setState({first_name})} value={this.state.first_name} placeholder={Constant.StringText.FIRSTNAME} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                    <View flexDirection='row'><Text style={Constant.styles.listSmallText}>{Constant.StringText.LASTNAME}</Text>
                           <Text style={Constant.styles.listSmallRedText}>{Constant.StringText.ASTERISK}</Text></View>
                    <TextInput onChangeText={last_name => this.setState({last_name})}  value={this.state.last_name} placeholder={Constant.StringText.LASTNAME} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                    <View flexDirection='row'><Text style={Constant.styles.listSmallText}>{Constant.StringText.EMAILADDRESS}</Text>
                           <Text style={Constant.styles.listSmallRedText}>{Constant.StringText.ASTERISK}</Text></View>
                    <TextInput  value={this.state.email} placeholder={Constant.StringText.EMAILPLACHOLDER} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                    <View style={Constant.styles.m10Top}>
                    <CheckBox  onChange={change_password => this.setState({change_password})} label={Constant.StringText.CHANGEPASSWORD} labelStyle={Constant.styles.listSmallText}/>
                    </View>
                    {this.state.change_password==true?
                        <View style={Constant.styles.m10Top}>
                         <View style={Constant.styles.lightGreenPanelAcc}><Text style={Constant.styles.whitenormalleftext}>{Constant.StringText.CHANGEPASSWORD}</Text></View>
                         <View flexDirection='row'><Text style={Constant.styles.listSmallText}>{Constant.StringText.CURRENTPASSWORD}</Text>
                                <Text style={Constant.styles.listSmallRedText}>{Constant.StringText.ASTERISK}</Text></View>
                         <TextInput onChangeText={current_password => this.setState({current_password})} secureTextEntry={true}  placeholder={Constant.StringText.CURRENTPASSWORD} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                         <View flexDirection='row'><Text style={Constant.styles.listSmallText}>{Constant.StringText.NEWPASSWORD}</Text>
                                <Text style={Constant.styles.listSmallRedText}>{Constant.StringText.ASTERISK}</Text></View>
                         <TextInput onChangeText={new_password => this.setState({new_password})} secureTextEntry={true} placeholder={Constant.StringText.NEWPASSWORD} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                         <View flexDirection='row'><Text style={Constant.styles.listSmallText}>{Constant.StringText.CONFIRMNEWPASWORD}</Text>
                                <Text style={Constant.styles.listSmallRedText}>{Constant.StringText.ASTERISK}</Text></View>
                         <TextInput onChangeText={confirm_password => this.setState({confirm_password})} secureTextEntry={true} placeholder={Constant.StringText.CONFIRMNEWPASWORD} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                         </View>
                    :null}
                    <View style={Constant.styles.m10Top}/>
                    <TouchableHighlight style={Constant.styles.orangebutton} onPress={()=>this.save()}>
                        <Text style={Constant.styles.whitebuttontext}>{Constant.StringText.SAVE}</Text>
                    </TouchableHighlight>
                    </View>
                    </ScrollView>
                    <TouchableOpacity style={Constant.styles.stickybutton} onPress={this.scrollToTop}>
                        <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
                    </TouchableOpacity>
                 
            </View>
            </KeyboardAvoidingView>
        );
    }

    //top of screen
  scrollToTop = () => {
    this.scroller.scrollTo({x: 0, y: 0});
  };
}