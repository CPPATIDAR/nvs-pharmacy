import React, { Component } from 'react';
import {View,StyleSheet,Text,TextInput,TouchableOpacity,Image,BackHandler,ScrollView} from 'react-native';
import Constant from '../../../Constant/Constant'
import CheckBox from 'react-native-checkbox'

export class Setting extends Component {
render() {
        return (
            <View style={Constant.styles.backWhite}>
            <ScrollView style={Constant.styles.backWhite}>
           <View style={Constant.styles.settingView}>
           <View style={{flexDirection:'row'}}>
                <View style={{flexDirection:'column',flex:1}}>
                    <Text style={[Constant.styles.listSmallText]}>Notification</Text>
                    <Text style={[Constant.styles.listSmallText]}>This will not affect any order updates</Text>
                </View>
                <View style={Constant.styles.m10Top}>
                    <CheckBox label='' labelStyle={[Constant.styles.listSmallText]} labelSide="right"/>
                </View>
            </View>
            <View style={[Constant.styles.viewStyle]}></View>
            <View style={{flexDirection:'row'}}>
                <View style={{flexDirection:'column',flex:1}}>
                    <Text style={[Constant.styles.listSmallText]}>Optimized Experience</Text>
                    <Text style={[Constant.styles.listSmallText]}>For internet connection quality</Text>
                </View>
            </View>
            <View style={Constant.styles.settingPannelStyle}>
            <Text style={[Constant.styles.blacknormalleftText]}>Optimized Image Quality</Text>
            <CheckBox label='' labelStyle={[Constant.styles.listSmallText]} labelSide="left"/>
            </View>
            <View style={Constant.styles.settingPannelStyle}>
            <Text style={[Constant.styles.blacknormalleftText]}>Optimized Checkout Flow</Text>
            <CheckBox label='' labelStyle={[Constant.styles.listSmallText]} labelSide="left"/>
            </View>
            <View style={[Constant.styles.viewStyle]}></View>
            <View style={Constant.styles.settingPannelStyle}>
            <Text style={[Constant.styles.blacknormalleftText]}>Shake for Feedback</Text>
            <CheckBox label='' labelStyle={[Constant.styles.listSmallText]} labelSide="left"/>
            </View>
           </View>
           </ScrollView>
           </View>
        );
    }

    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton.bind(this));
      }

      //hardware back button
      componentWillUnmount() {
      //handle tab click listener  remove
       BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton.bind(this));
      }
  
       //harware mobile back button
      handleBackButton(){
      if (this.props.navigation.state.routeName == 'Setting') {
        this.props.navigation.navigate('Home');
        return true;
      } 
    }
}