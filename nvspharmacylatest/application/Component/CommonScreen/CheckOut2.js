import React, { Component } from 'react';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import {TouchableOpacity,Image,TextInput,Text,View,ScrollView,TouchableHighlight,ActivityIndicator,AsyncStorage,
 Platform,BackHandler,Picker} from 'react-native';
import Constant from '../../../Constant/Constant';
import CouponPanel from '../CommonPanel/CouponPanel';
import Toast from 'react-native-simple-toast';
import Service from '../../config/Service';
import RNPickerSelect from 'react-native-picker-select'; 
import { PaymentRequest } from 'react-native-payments/lib/js';
import TopArrow from 'react-native-vector-icons/FontAwesome'
var clientToken="";
import {NativeModules} from 'react-native';

export default class CheckOut2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      customer_id:"",
      isLoading: false,
      card_number:"",
      deliver_method_value:"",
      payment_method_value:"",
      coupon_code:"",
      deliver_method:"",
      card_type:[],
      month:[],
      year:[],
      product_list:[],
      all_addresses:[],
      subtotal:"",
      total:"",
      deliveryprice:"",
      card_id:"",
      credit_debit_card:"",
      credit_card_value:"",
      paypal_credit:"",
      paypal_credit_value:"",
      paypal:"",
      paypal_value:"",
      card_picker_vaule:"",
      month_picker_value:"",
      year_picker_value:"",
      address_id:"",
      checkedRadio:null,
      cvv_number:"",
      shipping_description:"",
      shipping_price:"",
      customer_name:"",
      payment_method:"",
      delivery_methods_message:"",
      coupon_code:""
    };
  } 
  /* Call _fetchData after component has been mounted */
  async componentWillMount() {
    var customer_id = await AsyncStorage.getItem("customer_id");
    var card_id = await AsyncStorage.getItem("card_id");
    var customer_name = await AsyncStorage.getItem("customer_firstname");
    this.setState({customer_id:customer_id,card_id:card_id,isLoading: true,customer_name:customer_name});
    this.getCheckout2Data();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  render() {
    return( 
      this.state.isLoading?
      <View style={Constant.styles.indicatoreViewsTyle}> 
          <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
      </View>
      :
        <View style={Constant.styles.container}>
          <ScrollView ref={(scroller) => {this.scroller = scroller}}>
             <View style={Constant.styles.containerwithp10}>
                  <View style= {{flexDirection: 'column'}}>
                    {/* address and name select */}
                    <Text style={Constant.styles.blackboldlefttext}>{Constant.StringText.NAMENADDRESS}</Text>
                    <View style={Constant.styles.m10Top}/>
                    <TouchableHighlight onPress={()=>this.props.navigation.navigate('AddAddress',{data:"",type:"default"})}>
                      <Text style={Constant.styles.greenboldrightText}>+{Constant.StringText.ADDNEWADDRESS.toUpperCase()}</Text>
                    </TouchableHighlight>
                    <View style={Constant.styles.m10Top}/>
                    { Platform.OS=='ios'?
                        <View>
                          <RNPickerSelect /* placeholder={{label: 'Please Select options.......',value: "0"}} */  
                            hideIcon={false}
                            items={this.state.all_addresses}  
                            onValueChange={(value) => {this.setState({pickerValue: value});}}     
                            style={Constant.addresspickerSelectStyles}                                 
                            value={this.state.pickerValue}   
                            onDonePress={()=>this.onAddressPickerValueChange}  
                            />
                        </View> 
                         :
                         <View style={Constant.styles.boxwithborder}>
                         <Picker 
                             selectedValue={this.state.address_id}
                             mode='dropdown'
                             style={Constant.styles.colorPickerStyle}
                             onValueChange={this.onAddressPickerValueChange}>
                             {/* <Picker.Item label="Please Select options......." value="0" /> */}
                             {this.state.all_addresses.map((item,i)=>{
                               return(
                                 <Picker.Item style={Constant.styles.listSmallText} key={i} label={item.label} value={item.value}/>
                               );})
                             }
                         </Picker>
                        </View>
                      } 
                    <View style={Constant.styles.m10Top}/>
                    <View style={Constant.styles.viewStyle}></View>
                    <Text style={Constant.styles.blackboldlefttext}>{Constant.StringText.DELIVERYMETHOD}</Text>
                    {this.state.deliver_method==""?
                    <Text style={Constant.styles.redregulartext}>{this.state.delivery_methods_message}</Text>
                    :
                    <View>
                      <Text style={Constant.styles.listSmallText}>{this.state.deliver_method.methods_name}</Text>
                      <Text style={Constant.styles.listSmallText}>{this.state.deliver_method.carrier} {Constant.Currency}{this.state.deliver_method.price}</Text>
                    </View>
                    }
                    <View style={Constant.styles.viewStyle}></View>
                    <Text style={Constant.styles.blackboldlefttext}>{Constant.StringText.PAYMENTMETHOD}</Text>
                     <RadioGroup onSelect = {(index, value) => this.setState({payment_method:value})} size={15} thickness={2} color='lightgrey'>
                      {/* For credit/Debit card */}
                      <RadioButton value={this.state.credit_card_value} color='#689f38'>
                       <Text style={Constant.styles.radiblackText}>{"Credit/Debit Cards"}</Text>
                        {this.state.payment_method=="worldpay_cc"?
                          <View style={Constant.styles.filedViewsignup}>
                           <View style= {{flexDirection:'row'}}><Text style={Constant.styles.listSmallText}>{Constant.StringText.CARDHOLDERNAME}</Text></View>
                              <TextInput placeholder={Constant.StringText.CARDHOLDERNAME} onChangeText={customer_name => this.setState({customer_name})} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                           <View style= {{flexDirection:'row'}}><Text style={Constant.styles.listSmallText}>{Constant.StringText.CARDNUMBER}</Text><Text style={Constant.styles.redregulartext}>*</Text></View>
                            <TextInput keyboardType={'numeric'} placeholder={Constant.StringText.CARDNUMBER} onChangeText={card_number => this.setState({card_number})} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                             <View style={Constant.styles.cardviewStyle}>
                                <View style={Constant.styles.filedViewsignup}>
                                {/* for month listing */}
                                <View style= {{flexDirection:'row'}}><Text style={Constant.styles.listSmallText}>{Constant.StringText.EXPIRYMONTH}</Text><Text style={Constant.styles.redregulartext}>*</Text></View>
                                { Platform.OS=='ios'?
                                    <View>
                                      <RNPickerSelect placeholder={{label: 'Select',value: "0",}}  
                                        items={this.state.month}                                
                                        onValueChange={(value) => {this.setState({month_picker_value: value});   
                                        }}                                
                                        style={Constant.monthpickerSelectStyles}                                 
                                        value={this.state.month_picker_value}   
                                        />
                                    </View> 
                                    :
                                    <View style={Constant.styles.boxwithborder}>
                                    <Picker
                                        selectedValue={this.state.month_picker_value}
                                        mode='dropdown'
                                        style={Constant.styles.colorPickerStyle}
                                        onValueChange={(itemValue, itemIndex) => this.setState({month_picker_value: itemValue})}>
                                        <Picker.Item label="Select" value="0" />
                                        {this.state.month.map((item,key)=>{
                                            return(
                                              <Picker.Item style={Constant.styles.listSmallText} key={key} label={item.label} value={item.value}/>
                                            );
                                          })
                                      }
                                    </Picker>
                                  </View>
                                  } 
                                
                                </View>
                                <View style={{flex:1,flexDirection:'column'}}>
                                 {/* for year listing */}
                                <View style= {{flexDirection:'row'}}><Text style={Constant.styles.listSmallText}>{Constant.StringText.EXPIRYYEAR}</Text><Text style={Constant.styles.redregulartext}>*</Text></View>
                                { Platform.OS=='ios'?
                                    <View>
                                      <RNPickerSelect placeholder={{label: 'Select',value: "0",}}  
                                        hideIcon={false}
                                        items={this.state.year}                                
                                        onValueChange={(value) => {this.setState({year_picker_value: value});   
                                        }}                                
                                        style={Constant.monthpickerSelectStyles}                                 
                                        value={this.state.year_picker_value}   
                                        />
                                    </View> 
                                    :
                                    <View style={Constant.styles.boxwithborder}>
                                    <Picker
                                        selectedValue={this.state.year_picker_value}
                                        mode='dropdown'
                                        style={Constant.styles.colorPickerStyle}
                                        onValueChange={(itemValue, itemIndex) => this.setState({year_picker_value: itemValue})}>
                                        <Picker.Item label="Select" value="0" />
                                        {this.state.year.map((item,key)=>{
                                            return(<Picker.Item style={Constant.styles.listSmallText} key={key} label={item.label.toString()} value={item.value}/>);
                                          })
                                        }
                                    </Picker>
                                    </View>
                                  } 
                                </View>
                                </View>
                                <View style= {{flexDirection:'row'}}><Text style={Constant.styles.listSmallText}>{Constant.StringText.CVVNUMBER}</Text><Text style={Constant.styles.redregulartext}>*</Text></View>
                              <TextInput keyboardType={'numeric'} placeholder={Constant.StringText.CVVNUMBER} onChangeText={cvv_number => this.setState({cvv_number})} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                              </View>
                      :null} 
                      </RadioButton>
                      {Platform.OS=='android'?
                       <RadioButton value={"googlepay"} color='#689f38' disabled={true}>
                       <Text style={Constant.styles.radiblackText}>Google Pay Coming Soon</Text>
                      </RadioButton>
                      :
                      <RadioButton value={"applepay"} color='#689f38'>
                      <Text style={Constant.styles.radiblackText}>Apple Pay</Text>
                     </RadioButton>}
                     
                    </RadioGroup>    
                  </View>
                <View style= {{flex:1, flexDirection:'column'}}>
                    <View style={Constant.styles.viewStyle}></View>
                    <CouponPanel title={Constant.StringText.DISCOUNTCODES}>
                      <View style={Constant.styles.panelViewColumn}>  
                        <View style={Constant.styles.m10Top}/>
                        {this.state.coupon_code==""||this.state.coupon_code==null?
                        <TextInput onChangeText={coupon_code => this.setState({coupon_code})} placeholder={Constant.StringText.ENTERCOUPONCODE} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                        :
                        <TextInput onChangeText={coupon_code => this.setState({coupon_code})} value={this.state.coupon_code} placeholder={Constant.StringText.ENTERCOUPONCODE} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>}
                        <View style={Constant.styles.m10Top}/>
                        <View style={{flexDirection:'row'}}>
                          <TouchableHighlight style={Constant.styles.applycode} onPress={()=>this.getCouponCode()}>
                            <Text style={Constant.styles.whitebuttontext}>{Constant.StringText.APPLY}</Text>
                          </TouchableHighlight>
                          {/* {!this.state.coupon_code==""||!this.state.coupon_code==null?
                           <TouchableHighlight style={Constant.styles.removecode} onPress={()=>alert("remove code")}>
                           <Text style={Constant.styles.whitebuttontext}>{Constant.StringText.REMOVE}</Text>
                           </TouchableHighlight>
                          :null} */}
                        </View>
                      </View>
                    </CouponPanel>
                    <View style={Constant.styles.viewStyle}></View>
                    <View style={Constant.styles.revieworderView}>
                         <Text style={Constant.styles.blackboltext}>{Constant.StringText.REVIEWYOURORDER}</Text>
                        <View style= {{flexDirection: 'row',padding: 5,backgroundColor: Constant.color.lightGreen}}>
                        <Text style={Constant.styles.whitenormalleftext}>{Constant.StringText.PRODUCTNAME}</Text>
                        <Text style={Constant.styles.whitenormalcentertext}>{Constant.StringText.QTY}</Text>
                        <Text style={Constant.styles.whitnormalrighttext}>{Constant.StringText.SUBTOTAL}</Text>
                        </View>
                        {/* product listing */}
                        {this.state.product_list.map((item,key)=>{
                            return(
                              <View key={key}>
                              <View  style={{flexDirection: 'row',padding: 5}}>
                                <Text style={Constant.styles.blacknormalleftText}>{item.name}</Text>
                                <Text style={Constant.styles.blacknormalcenterText}>{item.quantity}</Text>
                                {item.special_price==""||item.special_price==null
                                ?
                                <Text style={Constant.styles.blacknormalrightText}>{Constant.Currency}{parseFloat(item.price).toFixed(2)}</Text>
                                :
                                <Text style={Constant.styles.blacknormalrightText}>{Constant.Currency}{parseFloat(item.special_price).toFixed(2)}</Text>}
                              </View>
                              <View style={Constant.styles.viewStyle}></View>
                              </View>
                            );
                          })
                        }
                        <View style= {{flexDirection: 'row',padding: 5}}>
                        <Text style={Constant.styles.blacknormalleftText}>{Constant.StringText.SUBTOTAL}</Text>
                        <Text style={Constant.styles.blacknormalrightText}>{Constant.Currency}{parseFloat(this.state.subtotal).toFixed(2)}</Text>
                        </View>
                        {this.state.deliver_method==""?null:
                        <View>
                        <View style={Constant.styles.viewStyle}></View>
                        <View style= {{flexDirection: 'row',padding: 5}}>
                        <Text style={Constant.styles.blacknormalleftText}>{this.state.deliver_method.methods_name} {this.state.deliver_method.carrier}</Text>
                        <Text style={Constant.styles.blacknormalrightText}>{Constant.Currency}{parseFloat(this.state.deliver_method.price).toFixed(2)}</Text>
                        </View>
                        </View>
                        }
                        <View style={Constant.styles.viewStyle}></View>
                        <View style= {{flexDirection: 'row',padding: 5}}>
                        <Text style={Constant.styles.blackboldlefttext}>{Constant.StringText.GRANDTOTAL}</Text>
                        {this.state.deliver_method==""?
                        <Text style={Constant.styles.blackboldrighttext}>{Constant.Currency}{parseFloat(this.state.subtotal).toFixed(2)}</Text>
                        :
                        <Text style={Constant.styles.blackboldrighttext}>{Constant.Currency}{parseFloat(parseFloat(this.state.subtotal)+parseFloat(this.state.deliver_method.price)).toFixed(2)}</Text>
                        }
                        </View>  
                    </View>
                    <View style= {{flexDirection:'row',alignSelf:'center'}}>
                       <Text style={Constant.styles.listSmallText}>{Constant.StringText.FORGOTANITEM}</Text>
                       <TouchableHighlight onPress={()=> this.functionToOpenNextActivity('CartListing')}>
                       <Text style={Constant.styles.greenTextWithline}>{Constant.StringText.EDITYOURCART}</Text>
                       </TouchableHighlight>
                    </View>
                    <View style={Constant.styles.m10Top}/>
                    <TouchableHighlight style={Constant.styles.orangebutton} onPress={()=> this.submitValue()}>
                      <Text style={Constant.styles.whitebuttontext}>{Constant.StringText.PLACEORDERSECURLY}</Text>
                    </TouchableHighlight>
                  </View>
                  <View style={Constant.styles.checkout1LogoView}>
                    <Image source={Constant.checkout2Logo} style={Constant.styles.checkout1logoImage}/>
                  </View>
                </View>
            </ScrollView>
            <TouchableOpacity style={Constant.styles.stickybutton} onPress={this.scrollToTop}>
              <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
            </TouchableOpacity>
          </View>
    );
  }

  //top of screen
  scrollToTop = () => {
    this.scroller.scrollTo({x: 0, y: 0});
  };
  //get data for display 
  getCheckout2Data()
  {
    this.setState({ isLoading: true });
    Service.getCheckoutData(Constant.checkoutprocess,this.state.card_id,this.state.customer_id).then((responseJson)=>{
      if(responseJson.result.status==1)
      {
       this.setState({product_list:responseJson.result.data,
        deliver_method:responseJson.result.delivery_methods,
        delivery_methods_message:responseJson.result.delivery_methods_message,
        subtotal:responseJson.result.subtotal,
        total:responseJson.result.grandtotal,
        credit_debit_card:responseJson.result.payment_methods.worldpay_cc.label,
        credit_card_value:responseJson.result.payment_methods.worldpay_cc.value,
        isLoading:false});
        /* fetch list of cards */
        var cardOption = [];
        Object.keys(responseJson.result.credit_cards_name[0]).forEach(function(key) {
          cardOption.push({'id':key,'name':responseJson.result.credit_cards_name[0][key]}) 
        });
        this.setState({card_type:cardOption});
        var newItems = responseJson.result.all_addresses.map(item => ({
          label: item.value,
          value: item.address_id
        }));
        this.setState({all_addresses:newItems});
        var newItems = responseJson.result.credit_cards_monthname.map(item => ({
          label: item.value,
          value: item.name
        }));
        this.setState({month:newItems});
        var newItems = responseJson.result.credit_cards_year.map(item => ({
          label: item.value.toString(),
          value: item.name
        }));
        this.setState({year:newItems});
      }
      else{
        this.setState({isLoading:false});
      }
    });
  }

  //get address picker value
  onAddressPickerValueChange=(value, index)=>{
    this.setState({address_id: value},
      () => {
        // here is our callback that will be fired after state change.
        if(!value==0)
        {
          //webservice to get item list
          this.setState({ isLoading: true });
          Service.getCheckoutData(Constant.checkoutprocess2,this.state.card_id,this.state.customer_id).then((responseJson)=>{
            if(responseJson.result.status==1)
            {
             this.setState({product_list:responseJson.result.data,
              deliver_method:responseJson.result.delivery_methods,
              delivery_methods_message:responseJson.result.delivery_methods_message,
              subtotal:responseJson.result.subtotal,
              total:responseJson.result.grandtotal,
              credit_debit_card:responseJson.result.payment_methods.worldpay_cc.label,
              credit_card_value:responseJson.result.payment_methods.worldpay_cc.value,
              isLoading:false});
              /* fetch list of cards */
            var cardOption = [];
            Object.keys(responseJson.result.credit_cards_name[0]).forEach(function(key) {
              cardOption.push({'id':key,'name':responseJson.result.credit_cards_name[0][key]}) 
            });
            this.setState({card_type:cardOption});
            var newItems = responseJson.result.credit_cards_monthname.map(item => ({
              label: item.value,
              value: item.name
            }));
            this.setState({month:newItems});
            var newItems = responseJson.result.credit_cards_year.map(item => ({
              label: item.value.toString(),
              value: item.name
            }));
            this.setState({year:newItems});
            }
            else{
              this.setState({isLoading:false});
            }
          });
        }
      }
    );
  }

  //submit payment methode
   submitValue=()=>
   {
    const APPLE_METHOD_DATA =[{
      supportedMethods: ['apple-pay'],
      data: {
        merchantIdentifier: 'merchant.NVSPharmacy.co.uk.NVS',
        supportedNetworks: ['visa', 'mastercard', 'amex'],
        countryCode: 'US',
        currencyCode: 'USD'
      }
    }];

    const GOOGLE_METHOD_DATA = [{
      supportedMethods: ['android-pay'],
      data: {
        supportedNetworks: ['visa', 'mastercard', 'amex'],
        currencyCode: 'USD',
        environment: 'TEST', // defaults to production
        paymentMethodTokenizationParameters: {
          tokenizationType: 'GATEWAY_TOKEN',
          parameters: {
            gateway: 'braintree',//stripe
            publicKey: '3dw87sw4svfwhvwz'//pk_test_AbdnEoq4RsyiWQtskw4NudO9
          }
        }
      }
    }];

    const DETAILS = {
      id: 'basic-example',
      displayItems: [
        {
          label: 'Movie Ticket',
          amount: { currency: 'USD', value: '0.15' }
        }
      ],
       shippingOptions: [{
           id: 'economy',
           label: 'Economy Shipping',
           amount: { currency: 'USD', value: '0.15' },
           detail: 'Arrives in 3-5 days' // `detail` is specific to React Native Payments
      }],
      total: {
        label: 'Merchant Name',
        amount: { currency: 'USD', value: '0.15' }
      }
    };
    if(this.state.deliver_method=="")
    {
      Toast.show("There is no shipping method.",Toast.SHORT);
    }
    else if(this.state.payment_method=="")    
    {
      Toast.show("Please select any payment method for processing payment.",Toast.SHORT);    
    }    
    else if(this.state.payment_method=="applepay"){
      var paymentRequest = new PaymentRequest(APPLE_METHOD_DATA, DETAILS);       
      paymentRequest.show().then(paymentResponse => { 
        const { transactionIdentifier, paymentData } = paymentResponse.details;           
        paymentResponse.complete('success');          
        console.log(JSON.stringify(paymentResponse));          
        paymentRequest.abort();          
        if(!JSON.stringify(paymentResponse)==""){ 
         Service.getGooglePayResponce(Constant.save_responce,JSON.stringify(paymentResponse)).then((responseJson)=>{               
           alert(JSON.stringify(responseJson.message));             
          });           
        }
        else{             
          Toast.show("No Data",Toast.SHORT);           
        }  
      })           
    }
    else if(this.state.payment_method=="googlepay")
    {
      NativeModules.GoogleModule.googlemodule();
    // alert("Coming Soon")
     /*  var paymentRequest = new PaymentRequest(GOOGLE_METHOD_DATA, DETAILS);       
      paymentRequest.show().then(paymentResponse => {         
        paymentResponse.complete('success');          
        console.log(JSON.stringify(paymentResponse));          
        paymentRequest.abort();          
        if(!JSON.stringify(paymentResponse)==""){ 
         Service.getGooglePayResponce(Constant.save_responce,JSON.stringify(paymentResponse)).then((responseJson)=>{               
           alert(JSON.stringify(responseJson.message));             
          });           
        }
        else{             
          Toast.show("No Data",Toast.SHORT);           
        }  
      }).catch((error) => 
      alert(error)
      );;     */
    }
    else{
      if(this.state.customer_name=="")
      {
        Toast.show(Constant.StringText.ENTERCARDHOLDERNAME,Toast.SHORT);
      }else if(this.state.card_number=="")
      {
        Toast.show(Constant.StringText.ENTERCARDNUMDER,Toast.SHORT);
      }else if(this.state.month_picker_value.toString()=="")
      {
        Toast.show(Constant.StringText.SELECTMONTH,Toast.SHORT);
      }else if(this.state.year_picker_value.toString()=="")
      {
        Toast.show(Constant.StringText.SELECTYEAR,Toast.SHORT);
      }else if(this.state.cvv_number=="")
      {
        Toast.show(Constant.StringText.ENTERCVV,Toast.SHORT);
      }else{
        this.setState({ isLoading: true });
        Service.submitCardvalue(Constant.credit_cardValue,this.state.customer_id,this.state.card_id,this.state.total,"USD",
        this.state.card_number.toString(),this.state.year_picker_value.toString(),this.state.month_picker_value.toString(),
        this.state.cvv_number.toString(),this.state.customer_name)
        .then((responseJson)=>{
         this.setState({isLoading: false});
          Toast.show(responseJson.message,Toast.SHORT);
          if(responseJson.status==1)
          {
            this.props.navigation.navigate('Home');
          }
          
        });
      }
    }
  }
  
   //to next activity
    functionToOpenNextActivity = (SCREEN) =>
    {
      this.props.navigation.navigate(SCREEN);
    }
    //handle back hardware button
    handleBackButton = () => {
      this.props.navigation.goBack(null);
      return true;
    } 
    //apply coupon code
    getCouponCode=()=>{
    if(this.state.coupon_code=="")
    {
      Toast.show(Constant.StringText.ENTERCOUPONCODE,Toast.SHORT);
    }else{
       //webservice to get item list
    this.setState({ isLoading: true });
      Service.applyCouponCode(Constant.couponApply,this.state.coupon_code).then((responseJson)=>{
        this.setState({isLoading: false});
        Toast.show(responseJson.result.coupon_message,Toast.SHORT);
          if(responseJson.result.status==1)
          {
            if(responseJson.result.data.length!=0)
            {
              this.setState({
                subtotal:responseJson.result.subtotal,
                total:responseJson.result.grandtotal});
            }
          }else{
            Toast.show(Constant.StringText.DATANOTAVAILABLE,Toast.SHORT);
          }
      });
      }
    }
}