import React, { Component } from 'react';
import {Image,Text,View,ScrollView,TouchableHighlight,TouchableOpacity,FlatList,AsyncStorage,ActivityIndicator,BackHandler,TextInput,Alert} from 'react-native';
import Constant from '../../../Constant/Constant'
import CouponPanel from '../CommonPanel/CouponPanel'
import Icon from 'react-native-vector-icons/Entypo'
import Toast from 'react-native-simple-toast';
import Service from '../../config/Service';
import TopArrow from 'react-native-vector-icons/FontAwesome'

export default class  CheckOut1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      customer_id:"",
      resultskey:"",
      items_count:"",
      items_qty:"",
      subtotal:"",
      grandtotal:"",
      dataSource:[],
      isLoading: false,
      error: null,
      quantity:"",
      refreshing: false,
      coupon_code:"",
      card_id:"",
      status:"",
    };
  }
  //handle click event
  handleKey = async(e)=> {
    if(e.state.routeName=="Checkout1")
      {
        var customer_id = await AsyncStorage.getItem("customer_id");
        var card_id = await AsyncStorage.getItem("card_id");
        this.setState({customer_id:customer_id,card_id:card_id});
        //get all product in list
        this.getCartListing();
      }
  }
  /* Call _fetchData after component has been mounted*/
  async componentWillMount() {
    this.props.navigation.addListener('willFocus',this.handleKey);
    this.setState({ isLoading: true });
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  
  //handle back hardware button
  handleBackButton = () => {
    this.props.navigation.goBack(null);
    return true;
  } 

  //render item for listview
  renderItem(data) {
  let { item, index } = data; 
  return (
    <View style={Constant.styles.listcontainer}>
        <TouchableOpacity activeOpacity={0.7} style={Constant.styles.listImageView} onPress={() => {this.NavigateAnother('ProductDetail',{product_id:item.product_id,quantity:item.quantity})}}>
          <Image source={{uri:item.image}} style={Constant.styles.listImage} />
        </TouchableOpacity>
      <View style={Constant.styles.m2Left}> 
          <Text style={Constant.styles.listSmallText}>{item.name.toUpperCase()}</Text>
          <View style={Constant.styles.listRow}> 
            {item.size!=null?
              <Text style={Constant.styles.listSmallText}>{Constant.StringText.SIZE}{item.size}</Text>
            :null}
            <Text style={Constant.styles.listSmallText}>{Constant.StringText.QTY}{item.quantity}</Text>
          </View>
          {item.special_price==null?
          <View style={Constant.styles.listRow}> 
            <Text style={Constant.styles.listSmallText}>{Constant.StringText.NOW}{parseFloat(item.price).toFixed(2)}</Text>
          </View>
          :
          <View style={Constant.styles.listRow}> 
            <Text style={Constant.styles.listSmallRedText}>{Constant.StringText.NOW}{parseFloat(item.special_price).toFixed(2)}</Text>
            <Text style={Constant.styles.listSmallText}>{Constant.StringText.WAS}{parseFloat(item.price).toFixed(2)}</Text>
          </View>
          }
          <Text style={Constant.styles.blackboldlefttext}>{Constant.StringText.SUBTOTAL}</Text>
          <Text style={Constant.styles.blackboldlefttext}>{Constant.Currency} {parseFloat(item.rowtotal).toFixed(2)}</Text>
      </View> 
        <TouchableHighlight style={{alignSelf:'flex-start'}} onPress={()=>{this._delete(item.product_id,index)}}>
          <Icon name={"cross"} size={25} color={'#000000'}/>
        </TouchableHighlight>
    </View>
   ) 
  } 

  //list seperator
  renderSeparator = () => {return (<View style={Constant.styles.separator}/>);};

  /* Renders the list */
  render() {
    return( 
      this.state.isLoading?
      <View style={Constant.styles.indicatoreViewsTyle}> 
          <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
      </View>
      : 
      <View style={{backgroundColor:'#ffffff'}}>
            <ScrollView ref={(scroller) => {this.scroller = scroller}}>
                <View>
                <View style={{flexDirection: 'column'}}>
                  <Text style={Constant.styles.blackboldlefttext}>{Constant.StringText.MYBAG}</Text>
                  <View style={Constant.styles.viewStyle}></View>
                  <Text style={Constant.styles.listSmallText}>{this.state.items_count} {Constant.StringText.ITEMS}</Text>
                  <View style={Constant.styles.viewStyle}></View>
                </View>
                <View style={{flex:1}}>
                  <FlatList
                    keyExtractor = {( item, index ) => index }
                    data={this.state.dataSource}
                    renderItem={this.renderItem.bind(this)}
                    ItemSeparatorComponent={this.renderSeparator}
                  />
                </View>
                <View style={{flex:1}}>
                    <View style={{flexDirection: 'column'}}>
                       <View style={Constant.styles.viewStyle}></View>
                      <CouponPanel title={Constant.StringText.DISCOUNTCODES}>
                      <View style={Constant.styles.panelViewColumn}>  
                        <View style={Constant.styles.m10Top}/>
                        {this.state.coupon_code==""||this.state.coupon_code==null?
                        <TextInput onChangeText={coupon_code => this.setState({coupon_code})} placeholder={Constant.StringText.ENTERCOUPONCODE} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                        :
                        <TextInput onChangeText={coupon_code => this.setState({coupon_code})} value={this.state.coupon_code} placeholder={Constant.StringText.ENTERCOUPONCODE} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>}
                        <View style={Constant.styles.m10Top}/>
                        <View style={{flexDirection:'row'}}>
                          <TouchableHighlight style={Constant.styles.applycode} onPress={()=>this.getCouponCode()}>
                            <Text style={Constant.styles.whitebuttontext}>{Constant.StringText.APPLY}</Text>
                          </TouchableHighlight>
                          {this.state.status==1||!this.state.coupon_code==""?
                           <TouchableHighlight style={Constant.styles.removecode} onPress={()=>this.removeCouponCode()}>
                           <Text style={Constant.styles.whitebuttontext}>{Constant.StringText.REMOVE}</Text>
                           </TouchableHighlight>
                          :null}
                        </View>
                      </View>
                      </CouponPanel>
                       <View style={Constant.styles.viewStyle}></View>
                    </View>
                    <View style={Constant.styles.ratecardView}>
                        <Text style={Constant.styles.blacknormalleftText}>{Constant.StringText.YOUHAVE} {this.state.items_count} {Constant.StringText.ITMEINYOURBAG}</Text>
                        <View style={{flexDirection: 'row',marginTop:Constant.styles.m10Top}}>
                        <Text style={Constant.styles.blacknormalleftText}>{Constant.StringText.SUBTOTAL}</Text><Text style={Constant.styles.blacknormalrightText}>{Constant.Currency} {parseFloat(this.state.subtotal).toFixed(2)}</Text>
                        </View>
                        <View style={Constant.styles.viewStyle}></View>
                        <View style={{flexDirection: 'row'}}>
                        <Text style={Constant.styles.blacknormalleftText}>{Constant.StringText.TOTAL}</Text><Text style={Constant.styles.blacknormalrightText}>{Constant.Currency} {parseFloat(this.state.subtotal).toFixed(2)}</Text>
                        </View>  
                        <View style={Constant.styles.viewStyle}></View>
                        <TouchableHighlight style={Constant.styles.orangebutton} onPress={()=>this.nextScreen()} >
                          <Text style={Constant.styles.whitebuttontext}>{Constant.StringText.PROCEDTOCHECKOUT}</Text>
                        </TouchableHighlight>
                    </View>
                </View>
                <View style={Constant.styles.checkout1LogoView}>
                    <Image source={Constant.checkout1Logo} style={Constant.styles.checkout1logoImage}/>
                </View>
                </View>
            </ScrollView>
            <TouchableOpacity style={Constant.styles.stickybutton} onPress={this.scrollToTop}>
              <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
            </TouchableOpacity>
          </View>
      );
  }
 //top of screen
 scrollToTop = () => {
  this.scroller.scrollTo({x: 0, y: 0});
  };
  //get cart listing
  getCartListing=()=>{
    //webservice to get item list
    Service.getCartListing(Constant.getCartDetail,this.state.card_id,this.state.customer_id).
    then((responseJson)=>{
      this.setState({isLoading:false});
      if(responseJson.result.status==1)
      {
        if(responseJson.result.data.length!=0)
        {
          this.setState({dataSource:responseJson.result.data,
              items_count: responseJson.result.items_count,
              items_qty:responseJson.result.items_qty,
              subtotal:responseJson.result.subtotal,
              grandtotal:responseJson.result.grandtotal,
              refreshing:false,
            })
        }
      }
      else{
        this.setState({dataSource:"",
          items_count: "",
          items_qty:"",
          subtotal:"",
          grandtotal:"",
          refreshing:false
        });
      }
    })
  }
  
  //navigate another page with check customer_id 
  nextScreen = () => {
    if(this.state.customer_id!=null||this.state.customer_id!=""){
      this.props.navigation.navigate('CheckOut2');
    }else{
      this.props.navigation.navigate('Account');
    }
  } 

  //apply coupon code
  getCouponCode=()=>{
  if(this.state.coupon_code=="")
  {
    Toast.show(Constant.StringText.ENTERCOUPONCODE,Toast.SHORT);
  }else{
  //webservice to get item list
  this.setState({ isLoading: true });
  Service.applyCouponCode(Constant.couponApply,this.state.coupon_code).then(async(responseJson)=>{
    Toast.show(responseJson.result.coupon_message,Toast.SHORT);
    this.setState({isLoading:false});
        if(responseJson.result.status==1)
        {
          if(responseJson.result.data.length!=0)
          {
            await AsyncStorage.setItem("coupon_code",responseJson.result.coupon_code);
            await AsyncStorage.setItem("card_id",responseJson.result.card_id);
            this.setState({
                status:responseJson.result.status,
                dataSource:responseJson.result.data,
                items_count: responseJson.result.items_count,
                items_qty:responseJson.result.items_qty,
                subtotal:responseJson.result.subtotal,
                grandtotal:responseJson.result.grandtotal,
                coupon_code:responseJson.result.coupon_code
              })
              try {
                await AsyncStorage.setItem('items_count', this.state.items_count+"");
              } catch (error) {
                // Error saving data
                Toast.show(error,Toast.SHORT);
              }
              //call getlisting to update data
              this.getCartListing();
          }
        }
      })
    }
  }

  //remove coupon code
  removeCouponCode=()=>{
    if(this.state.coupon_code=="")
    {
      Toast.show(Constant.StringText.ENTERCOUPONCODE,Toast.SHORT);
    }else{
    //webservice to get item list
    this.setState({ isLoading: true });
    Service.removeCouponCode(Constant.removecouponcode,this.state.coupon_code,this.state.customer_id
  ,this.state.card_id).then(async(responseJson)=>{
    alert(JSON.stringify(responseJson))
    this.setState({isLoading:false});
     /*  Toast.show(responseJson.message,Toast.SHORT);
      this.setState({isLoading:false});
          if(responseJson.status==1)
          {
            if(responseJson.data.length!=0)
            {
              this.setState({
                  status:responseJson.result.status,
                  dataSource:responseJson.result.data,
                  items_count: responseJson.result.items_count,
                  items_qty:responseJson.result.items_qty,
                  subtotal:responseJson.result.subtotal,
                  grandtotal:responseJson.result.grandtotal,
                  coupon_code:responseJson.result.coupon_code
                })
                try {
                  await AsyncStorage.setItem('items_count', this.state.items_count+"");
                } catch (error) {
                  // Error saving data
                  Toast.show(error,Toast.SHORT);
                }
                //call getlisting to update data
                this.getCartListing();
            }
          } */
        })
      }
    }

  //delete product from list
  _delete=(product_id)=> {
    Alert.alert(
      '',
      'Are you sure want delete this item. ',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'OK', onPress: () => 
        {
          this.setState({isLoading:true});
          //delete product from cart
          Service.deletCardProduct(Constant.deleteItemsincart,this.state.card_id,product_id).then((responseJson)=>{
              this.setState({isLoading:false });
              Toast.show(responseJson.message,Toast.SHORT);
              if(responseJson.status==1)
              {
                var dataSource = [...this.state.dataSource]
                let index = dataSource.indexOf(product_id);
                dataSource.splice(index, 1);
                this.setState({dataSource});
                this.getCartListing();
              }
          })
        }
        },
      ],
      { cancelable: false }
    ) 
  }
}