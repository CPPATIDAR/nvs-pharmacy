import React,{Component} from 'react'
import {Text,TouchableOpacity,View,Image,ScrollView,TextInput,Picker,ActivityIndicator,Platform,Modal,
  KeyboardAvoidingView,AsyncStorage,BackHandler,Alert} from 'react-native'
import Swiper from 'react-native-swiper'
import Panel from '../CommonPanel/Panel'
import Stars from 'react-native-stars';
import Constant from '../../../Constant/Constant'
import Toast from 'react-native-simple-toast';
import Service from '../../config/Service';
import ImageViewer from 'react-native-image-zoom-viewer';
import ImageView from 'react-native-image-progress';
import TopArrow from 'react-native-vector-icons/FontAwesome'
import ArrowIcon from 'react-native-vector-icons/Ionicons';
import RNPickerSelect from 'react-native-picker-select'; 
import CrossIcon from 'react-native-vector-icons/Entypo';
const nonFavImage = require('../../../resource/img/heart.png');
const favImage = require('../../../resource/img/fill_heart.png');
import HTMLTEXT from 'react-native-html-to-text'

export default class  productdetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
        params:{product_id:this.props.navigation.state.params.product_id,quantity:this.props.navigation.state.params.quantity},
        galleryImage: [],
        isLoading: false,
        error: null,
        name: "",
        brand:"",
        short_description:"",
        description:"",
        price:"",
        size:"",
        availability:"",
        rating:"",
        special_price:"",
        how_to_use:"",
        ingredients:"",
        extra_info:"",
        review_summary:[],
        configurable_optionsArray:[],
        configurable_options_Id:"",
        configurable_options:"",
        pickerValue: "",
        nickname:"",
        summaryreview:"",
        review:"",
        pricestarts:"",
        valuestars:"",
        qualitystars:"",
        quantity:"1",
        showNonFavImage:true,
        customer_id:"",
        device_id:"",
        product_type:"",
        imageIndex: 0,
        isImageViewVisible: false,
        product_image:"",
        pickerLabel:"",
        card_id:"",
        qtyarray:[],
    };
  }
  //handle click event
  handleKey = async(e)=> {
    if(e.state.routeName=="ProductDetail")
      {
      var customer_id = await AsyncStorage.getItem("customer_id");
      var card_id = await AsyncStorage.getItem("card_id");
      if(this.state.params.quantity==""||this.state.params.quantity==null)
      {
        this.setState({quantity:1});
      }else{
        this.setState({quantity:this.state.params.quantity});
      }
      this.setState({customer_id:customer_id,card_id:card_id});
      this.getProductdetail();
      }
  }
 
  //start activty from 
  componentWillMount(){
    this.props.navigation.addListener('willFocus',this.handleKey);
    var array=[];
    for (var i = 1; i <= 50; i++) {
      array.push(i+"");
    }
    var newItems = array.map(item => ({
      label: item+"",
      value: item+"",
    }));
    this.setState({qtyarray:newItems,isLoading: true });
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  //hardware back button
  componentWillUnmount() {
  //handle tab click listener  remove
   BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  //harware mobile back button
  handleBackButton = () => {
    this.props.navigation.goBack(null);
    return true;
  }  

  //image footer
  renderFooter=()=>{
    return ( 
    <TouchableOpacity style={{alignSelf:'center',justifyContent:'center',paddingBottom:60}}>
     <Text style={Constant.styles.whitebuttontext}>{this.state.name}</Text>
    </TouchableOpacity>
    );
  }

  //image header
  renderHeader=()=>{
    return ( 
      <TouchableOpacity style={{alignSelf:'flex-end',padding:10}} onPress={()=>{this.setModalVisible(!this.state.isImageViewVisible)}}>
       <CrossIcon name={"circle-with-cross"} size={25} color={'#ffffff'}/>
      </TouchableOpacity>
      );
  }

  setModalVisible(visible) {
    this.setState({isImageViewVisible: visible});
  }

  render(){
    return( 
      this.state.isLoading?
      <View style={Constant.styles.indicatoreViewsTyle}> 
          <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
      </View>
      :
      <KeyboardAvoidingView style={{width:'100%',height:'100%'}} keyboardVerticalOffset={Constant.offset} contentContainerStyle={{height:'100%',width:'100%'}} behavior={'padding'} enabled>
           
    <View style={Constant.styles.container}>
      <ScrollView style={Constant.styles.container} ref={(scroller) => {this.scroller = scroller}}>
      <View  style={Constant.styles.containerwithp10}>
        <Text style={Constant.styles.blackboltext}>{this.state.name}</Text>
        <View style={Constant.styles.productImageSlider}>
          {this.state.galleryImage?
            <Swiper style={{backgroundColor:Constant.color.white}}  
             showsButtons={true} activeDotColor={'transparent'} dotColor={'transparent'} 
             prevButton={<ArrowIcon name="ios-arrow-back"size={28} color={Constant.color.black}/>}
             nextButton={<ArrowIcon name="ios-arrow-forward"size={28} color={Constant.color.black}/>}>
             {this.state.galleryImage.map((image, i) => (
                  <TouchableOpacity
                  key={i}
                  onPress={() => {
                    this.setModalVisible(!this.state.isImageViewVisible)
                    this.setState({imageIndex:i})}} >
                     <ImageView
                      style={Constant.styles.productImageStyle}
                      indicator={ImageView.Pie}
                      indicatorProps={{size: 'small',color: '#006d50'}}
                      source={{uri:image.url}}
                      resizeMode="contain"/>
                     
            </TouchableOpacity>
            ))}
           </Swiper> :null}
        </View>
           <Modal visible={this.state.isImageViewVisible} transparent={true} onRequestClose={()=>this.setModalVisible(!this.state.isImageViewVisible)}>
              <ImageViewer
                imageUrls={this.state.galleryImage}
                renderHeader={this.renderHeader}
                 renderFooter={this.renderFooter}
                 footerContainerStyle={{alignSelf:'center',justifyContent:'center',paddingBottom:Platform.OS=='android'?60:40}}/>
            </Modal>
            {this.state.rating==""?
              <View style={Constant.styles.RatingStarViewStyle}>
                <Stars
                      half={true}
                      value={this.state.rating/20}
                      spacing={4}
                      starSize={15}
                      count={5}
                      fullStar={Constant.RatedStar}
                      emptyStar={Constant.InRatedStar}
                      halfStar={Constant.RatedStar}/>
              </View>
            :null}
        <View style={{flexDirection:'row'}}>
            <View style={{flex:1,flexDirection:'column',alignItems:'flex-start'}}>
            <View style={{flexDirection:'row'}}><Text style={Constant.styles.listSmallText}>{Constant.StringText.BRAND}</Text><Text style={Constant.styles.greenboldrightText}>{this.state.brand}</Text></View>
            {this.state.size!=null?
              <Text style={Constant.styles.listSmallText}>{Constant.StringText.SIZE}{this.state.size}</Text>:null}
            </View>
            <View style={{flex:1,flexDirection:'column',alignItems:'flex-end'}}>
              {this.state.special_price!=null
                ?
                <View style={Constant.styles.detailpagePriceStyle}>
                  <Text style={Constant.styles.listSmallRedText}>{Constant.StringText.NOW}{parseFloat(this.state.special_price).toFixed(2)}</Text>
                  <Text style={Constant.styles.crossText}>{Constant.StringText.WAS}{parseFloat(this.state.price).toFixed(2)}</Text>
                  <Text style={Constant.styles.tosmallText}>(You save {Constant.Currency} 
                  {parseFloat(parseFloat(this.state.price).toFixed(2)-parseFloat(this.state.special_price).toFixed(2)).toFixed(2)} on this)</Text>
                </View>  
                :
                <View style={Constant.styles.detailpagePriceStyle}>
                  <Text style={Constant.styles.listSmallText}>{Constant.StringText.PRICE}{":"} {Constant.Currency}{parseFloat(this.state.price).toFixed(3)}</Text>
                </View>
              }
            </View>
        </View>
        <View style={{flexDirection:'row',marginTop:5}}>
        {(this.state.configurable_options!=""||this.state.product_type=="configurable")?
          <View style={{flexDirection:'row',flex:1,marginRight:10}}>
            <View style={{flexDirection:'column',alignItems:'flex-start'}}>
                <View style={{flexDirection:'row'}}><Text style={Constant.styles.redregulartext}>{Constant.StringText.ASTERISK}</Text><Text style={Constant.styles.pickerheadertext}>{this.state.configurable_options.label} </Text></View>
                { Platform.OS=='ios'?
                   <View style={Constant.styles.detailpickerstyle}> 
                    <RNPickerSelect placeholder={{label: 'Select options',value: "0",}}  
                      hideIcon={true}
                      items={this.state.configurable_optionsArray}                                
                      onValueChange={(value) => this.onPickerValueChange(value)}                                
                      style={Constant.pickerSelectStyles}                                 
                      value={this.state.pickerValue}   
                      />
                      <Image source={Constant.downpickericon}/>
                  </View> 
                         :
                  <View style={Constant.styles.boxQuanity}>
                    <Picker
                      ref="picker"
                      selectedValue={this.state.pickerValue}
                      style={Constant.styles.colorPickerStyle}
                      mode='dropdown'
                      onValueChange={(itemValue, itemIndex) =>this.onPickerValueChange(itemValue)}>
                      <Picker.Item label="Select options" value="0" />
                      {this.state.configurable_optionsArray.map(function(item){
                        return(
                            <Picker.Item label={item.label} key={item.value} value={item.value}/>
                          );
                        })
                      }
                    </Picker>
                </View>
                }
          </View>
          </View>
          :null}
          <View style={{flexDirection:'row',flex:1}}>
           <View style={{flexDirection:'column',alignItems:'flex-start'}}>
                <View style={{flexDirection:'row'}}><Text style={Constant.styles.redregulartext}>{Constant.StringText.ASTERISK}</Text><Text style={Constant.styles.pickerheadertext}>Quantity</Text></View>
                { Platform.OS=='ios'?
                   <View style={Constant.styles.detailpickerstyle}> 
                    <RNPickerSelect placeholder={{label: 'Select Qty',value: "0",}}  
                      hideIcon={false}
                      items={this.state.qtyarray}   
                      hideIcon={true}                
                      style={Constant.pickerSelectStyles}             
                      onValueChange={(value) =>this.setState({quantity:value})}                                              
                      value={this.state.quantity}   
                      />
                      <Image source={Constant.downpickericon}/>
                  </View> 
                  :
                  <View style={Constant.styles.boxQuanity}>
                    <Picker
                      ref="picker"
                      selectedValue={this.state.quantity}
                      style={Constant.styles.colorPickerStyle}
                      mode='dropdown'
                      onValueChange={(itemValue) =>this.setState({quantity:itemValue})}>
                      {this.state.qtyarray.map( (x,i) => { 
                        return(<Picker.Item label={x.label+""} key={i} value={x.value+""}/>)} )
                      }
                    </Picker>
                </View>
                }
            </View>
          </View>
        </View>
        <Image style={{width:'100%',marginTop:10}}source={Constant.horizontaldotImgae}/>
        <View style={Constant.styles.p5CenterRow}>
        <TouchableOpacity style={Constant.styles.addbagbutton} onPress={this.addToCart.bind(this)}>
          <Text style={Constant.styles.whitebuttontext}>{Constant.StringText.ADDTOBAG}</Text>
        </TouchableOpacity>
        <TouchableOpacity  onPress={() =>this.hearclick(this.state.params.product_id)}>
            {this.renderImage()}
        </TouchableOpacity>
        </View>
          <View style={{flex:1,backgroundColor:Constant.color.white}}>
            <View style={{backgroundColor : Constant.color.white}}>
            {this.state.description!=null&&this.state.description!=""?
              <Panel title={Constant.StringText.DESCRIPTION}>
              <HTMLTEXT style={Constant.styles.listSmallText} html={this.state.description}/>
              </Panel>
            :null}
            {this.state.ingredients!=null&&this.state.ingredients!=""?
              <Panel title={Constant.StringText.INGREDIENTS}>
                <HTMLTEXT style={Constant.styles.listSmallText} html={this.state.ingredients}/>
              </Panel>
            :null}
            {this.state.how_to_use!=null&&this.state.how_to_use!=""?
              <Panel title={Constant.StringText.HOWTOUSE}>
                <HTMLTEXT style={Constant.styles.listSmallText} html={this.state.how_to_use}/>
              </Panel>
            :null}
            {this.state.extra_info!=null&&this.state.extra_info!=""?
              <Panel title={Constant.StringText.EXTRAINFO}>
                <HTMLTEXT style={Constant.styles.listSmallText} html={this.state.extra_info}/>
              </Panel>
            :null}
            <Panel title={Constant.StringText.PRODUCTREVIEWS}>
              {this.state.review_summary.length!=0?
                  this.state.review_summary.map(function(item,index){
                      return(
                        <View key={index} style={{flexDirection:'column'}}>
                          <View style={{flexDirection:'column'}}> 
                            <Text style={Constant.styles.greenboldrightText}>{item.title}</Text>
                            <Text style={Constant.styles.listSmallText}> {Constant.StringText.REVIEWBY} {item.nickname}</Text> 
                          </View> 
                            {item.ratings.map(function(data,index){
                              return(
                                <View key={index} style={{flexDirection:'row'}}>
                                <View style={{flex:.2,flexDirection:'column'}}>
                                  <Text style={Constant.styles.greenboldrightText}>{data.rating_code}</Text>
                                </View>
                                <View style={{flex:.8,flexDirection:'column',alignItems:'flex-start'}}>
                                <Stars
                                    half={true}
                                    value={data.rating_percent/20}
                                    spacing={4}
                                    starSize={15}
                                    count={5}
                                    fullStar={Constant.RatedStar}
                                    emptyStar={Constant.InRatedStar}
                                    halfStar={Constant.RatedStar}/>
                                </View>
                                </View>
                              );
                              })
                            }
                          <Text style={Constant.styles.listSmallText}>{item.details}</Text> 
                          </View>
                      );
                    })
                  :null}
                  {/* review form */}
                  <View style={{flexDirection:'column'}}>
                    <Text style={Constant.styles.blackboltext}>{Constant.StringText.WRITEYOUROWNREVIEW}</Text> 
                    <View style={{flexDirection:'column'}}><Text style={Constant.styles.listSmallText}>{Constant.StringText.YOUAREREVIEWING}</Text>
                    <Text style={Constant.styles.greenTextWithlineLeft}  lineBreakMode={true} >{this.state.name}</Text></View> 
                    <View style={{flexDirection:'row'}}><Text style={Constant.styles.listSmallText}>{Constant.StringText.HOWDOYOURATE}</Text><Text style={Constant.styles.redregulartext}>{Constant.StringText.ASTERISK}</Text></View>
                    <View style={{flexDirection:'row'}}>
                      <View style={{flexDirection:"column",flex:.2}}>
                       <Text style={Constant.styles.listSmallText}>{Constant.StringText.PRICE}</Text> 
                       <Text style={Constant.styles.listSmallText}>{Constant.StringText.VALUE}</Text>
                       <Text style={Constant.styles.listSmallText}>{Constant.StringText.QUALITY}</Text>
                      </View>
                      <View style={{flexDirection:"column",flex:.8,alignItems:'flex-start'}}>
                      <Stars
                                    update={this.priceRatingChanged}
                                    spacing={4}
                                    style={{flex:.8}}
                                    starSize={15}
                                    count={5}
                                    fullStar={Constant.RatedStar}
                                    emptyStar={Constant.InRatedStar}
                                    halfStar={Constant.RatedStar}/>
                    <Stars
                                    spacing={4}
                                    update={this.valueRatingChange}
                                    starSize={15}
                                    count={5}
                                    fullStar={Constant.RatedStar}
                                    emptyStar={Constant.InRatedStar}
                                    halfStar={Constant.RatedStar}/>
                    <Stars
                                    
                                    spacing={4}
                                    update={(val)=>{this.setState({qualitystars: val})}}
                                    starSize={15}
                                    count={5}
                                    fullStar={Constant.RatedStar}
                                    emptyStar={Constant.InRatedStar}
                                    halfStar={Constant.RatedStar}/>
                      </View>
                    
                   </View> 
                    <View style={{flexDirection:'row'}}><Text style={Constant.styles.listSmallText}>{Constant.StringText.NICKNAME}</Text><Text style={Constant.styles.redregulartext}>{Constant.StringText.ASTERISK}</Text></View> 
                    <TextInput onChangeText={nickname => this.setState({nickname})} placeholder="" underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>                  
                    <View style={{flexDirection:'row'}}><Text style={Constant.styles.listSmallText}>{Constant.StringText.SUMMARYYOURREVIEW}</Text><Text style={Constant.styles.redregulartext}>{Constant.StringText.ASTERISK}</Text></View> 
                    <TextInput onChangeText={summaryreview => this.setState({summaryreview})} placeholder="" underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                    <View style={{flexDirection:'row'}}><Text style={Constant.styles.listSmallText}>{Constant.StringText.REVIEW}</Text><Text style={Constant.styles.redregulartext}>{Constant.StringText.ASTERISK}</Text></View> 
                    <TextInput onChangeText={review => this.setState({review})} placeholder="" underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                    <TouchableOpacity style={Constant.styles.darkGreenbutton} onPress={()=>this.submitReview()}>
                      <Text style={Constant.styles.whitebuttontext}>{Constant.StringText.SUBMITREVIEW}</Text>
                    </TouchableOpacity>
                  </View>
              </Panel>
            </View> 
          </View>
        </View>
        </ScrollView>
        <TouchableOpacity style={Constant.styles.stickybuttonstart} onPress={this.scrollToTop}>
              <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
        </TouchableOpacity>
      </View>
      </KeyboardAvoidingView>
      )
    }

     //top of screen
    scrollToTop = () => {
      this.scroller.scrollTo({x: 0, y: 0});
    };
    //render fav heart image
    renderImage = () => {
      var imgSource = this.state.showNonFavImage ? nonFavImage : favImage;
      return (<Image style={Constant.styles.wishlistimageSize} source={imgSource}/>);
    }
    //get product detail
    getProductdetail()
    { 
      Service.getProductDetail(Constant.product_detail,this.state.params.product_id,this.state.customer_id).then((responseJson)=>{
        this.setState({ 
          name: responseJson.name ,
          product_type: responseJson.product_type,
          price: responseJson.price,
          brand: responseJson.brand,
          size: responseJson.size,
          short_description: responseJson.short_description,
          availability : responseJson.availability,
          description: responseJson.description,
          rating: responseJson.rating,
          special_price:responseJson.special_price,
          how_to_use:responseJson.how_to_use,
          ingredients:responseJson.ingredients,
          extra_info:responseJson.extra_info,
          review_summary:responseJson.review_summary,
          configurable_options_Id:responseJson.configurable_options.id,
          configurable_options:responseJson.configurable_options,
          product_image:responseJson.image,
          isLoading:false,
        });
        if(this.state.customer_id!=""&&this.state.customer_id!=null)
        {
          if(responseJson.wishlist_item==1)
          {
            this.setState({showNonFavImage:false});
          }else{
            this.setState({showNonFavImage:true});
          }
        }
      var imageOptions = [];
        Object.keys(responseJson.gallery_image).forEach(function(key) {
          imageOptions.push({"url":responseJson.gallery_image[key]}); 
        });
        this.setState({galleryImage:imageOptions});

       if(responseJson.configurable_options!=""||responseJson.product_type=="configurable")
       {
        var newItems = responseJson.configurable_options.data.map(item => ({
          label: item.product_label,
          value: item.id
        }));
        this.setState({configurable_optionsArray:newItems});
       }

      }) 
    }
    //submit review with validation 
    submitReview = () =>
    {
      if(this.state.nickname=="")
      {
        Toast.show(Constant.StringText.ENTERNICKNAME,Toast.SHORT);
      }else if(this.state.summaryreview=="")
      {
        Toast.show(Constant.StringText.ENTERSUMMARYREVIEW,Toast.SHORT);
      }else if(this.state.review=="")
      {
        Toast.show(Constant.StringText.ENTERREVIEW,Toast.SHORT);
      }else if(this.state.pricestarts=="")
      {
        Toast.show(Constant.StringText.RATINGFORPRICE,Toast.SHORT);
      }else if(this.state.valuestars=="")
      {
        Toast.show(Constant.StringText.RATINGFORVALUE,Toast.SHORT);
      }else if(this.state.qualitystars=="")
      {
        Toast.show(Constant.StringText.RATINGFORQUATITY,Toast.SHORT);
      }else{
        if(this.state.customer_id==null||this.state.customer_id=="")
        {
          Toast.show(Constant.StringText.NEEDLOGIN,Toast.SHORT);
        }else{
          Service.submitReview(Constant.submit_review,this.state.params.product_id,this.state.customer_id,this.state.nickname
          ,this.state.summaryreview,this.state.review,this.state.pricestarts.toString(),this.state.valuestars.toString(),this.state.qualitystars.toString()).then((responseJson)=>{
            Toast.show(responseJson.message,Toast.SHORT);
          })
        }
      }
    }

  //calline service addToCart
  addToCart = () =>
  {
    if (this.state.quantity==""||this.state.quantity==0) 
    {
      Toast.show(Constant.StringText.SELECTQUANTITY,Toast.SHORT);
    }
    else if(this.state.quantity>50)
    {
      Toast.show(Constant.StringText.MAXIMAMQUANTITY,Toast.SHORT);
    }
    else if(this.state.product_type=="configurable")
    {
      if(this.state.pickerValue==""){
        Toast.show("Please select option",Toast.SHORT);
      }  
      else{
       this.addToCartService();
      }
    }
    else {
       this.addToCartService();
    }
  }

  onPickerValueChange=(itemValue)=>{
    if(itemValue!=0)
    {
      let labelName = this.state.configurable_optionsArray.find(label=>label.value === itemValue);
      this.setState({pickerValue: itemValue,pickerLabel:labelName.label});
    }
  }

  onpickerchangeforqty=(itemValue)=>{
    if(itemValue!=0)
    {
      this.setState({quantity: itemValue});
    }
  }

  //calling add to card service
  async addToCartService()
  { 
     if(this.state.customer_id==""||this.state.customer_id==null)
    {
    var existingobj = await AsyncStorage.getItem("array");
    var existingobjparse=JSON.parse(existingobj);
    var obj={};
    var rowtotal = "";
    if(this.state.special_price==null||this.state.special_price==0)
    {
      rowtotal = parseFloat(this.state.price*this.state.quantity).toFixed(2);
    }else{
      rowtotal = parseFloat(this.state.special_price*this.state.quantity).toFixed(2);
    }
    if(this.state.configurable_options_Id==""||this.state.configurable_options_Id==undefined)
    {
      obj =  {
        "product_id": this.state.params.product_id.toString(),
        "quantity": this.state.quantity.toString(),
        "attribute_id": "",
        "attribute_value":"",
        "type":"",
        "name": this.state.name,
        "price": this.state.price,
        "special_price": this.state.special_price,
        "available": this.state.availability,
        "size": this.state.size,
        "brand": this.state.brand,
        "configurable_options": [],
        "image": this.state.product_image,
        "rowtotal":rowtotal
        };
    }
    else{
       var configurableObject={
      "attribute_id":this.state.configurable_options_Id,
      "attribute_value": this.state.pickerValue,
      "label": this.state.configurable_options.label,
      "value": this.state.pickerLabel
      };
      var configurableArray =[configurableObject];
      obj =  {
        "product_id": this.state.params.product_id.toString(),
        "quantity": this.state.quantity.toString(),
        "attribute_id": this.state.configurable_options_Id.toString(),
        "attribute_value": this.state.pickerValue.toString(),
        "type":"configurable",
        "name": this.state.name,
        "price": this.state.price,
        "special_price": this.state.special_price,
        "available": this.state.availability,
        "size": this.state.size,
        "brand": this.state.brand,
        "configurable_options": configurableArray,
        "image": this.state.product_image,
        "rowtotal":rowtotal
        };
    }
    var buyItems=[]; 
    if(existingobjparse==""|| existingobjparse ==null)
    {
      existingobjparse=[];
    }
    var isPresent = existingobjparse.some((el)=>{ return el.product_id === this.state.params.product_id});
   if(isPresent==true)
    {
      var updateData=[];
      existingobjparse.map(data=>{
        if(data.product_id===this.state.params.product_id){
          data.quantity = parseInt(data.quantity)+parseInt(this.state.quantity);
          if(data.special_price==0||data.special_price==null)
          {
            data.rowtotal=data.price*(parseInt(data.quantity));
          }else{
            data.rowtotal=data.special_price*(parseInt(data.quantity));
          }
        }
        updateData=[...updateData,data];
      });
      buyItems=[...updateData]
    }
    else{
      buyItems=[...existingobjparse,obj];
    }
   
    await AsyncStorage.setItem("array",JSON.stringify(buyItems));
    Toast.show("Product added in cart",Toast.SHORT);
    this.props.navigation.navigate('CartListing');
    }
    else{
      this.setState({ isLoading:true});
      Service.addTocart(Constant.addToCart,this.state.card_id,this.state.params.product_id,this.state.quantity,this.state.customer_id
        ,this.state.configurable_options_Id,this.state.pickerValue).then(async(responseJson)=>
        {
          this.setState({ isLoading:false});
          Toast.show(responseJson.message,Toast.SHORT);
          this.props.navigation.navigate('CartListing');
        });
    }
  }

  //price rating star
  priceRatingChanged = (newRating) => {
    if(newRating==1)
    {
      this.setState({pricestarts:11});
    }else if(newRating==2)
    {
      this.setState({pricestarts:12});
    }else if(newRating==3)
    {
      this.setState({pricestarts:13});
    }else if(newRating==4)
    {
      this.setState({pricestarts:14});
    }else if(newRating==5)
    {
      this.setState({pricestarts:15});
    }
  }

  //value rating star
  valueRatingChange = (newRating) => {
    if(newRating==1)
    {
      this.setState({valuestars:6});
    }else if(newRating==2)
    {
      this.setState({valuestars:7});
    }else if(newRating==3)
    {
      this.setState({valuestars:8});
    }else if(newRating==4)
    {
      this.setState({valuestars:9});
    }else if(newRating==5)
    {
      this.setState({valuestars:10});
    }
  }

  //click heart image
  hearclick(product_id){
    if(this.state.customer_id==""||this.state.customer_id==null)
    {
        Toast.show(Constant.StringText.NEEDLOGIN,Toast.SHORT);
    }else{
      if(this.state.showNonFavImage==true)
      {
        //add product in wishlist
        this.setState({ isLoading: true });
        Service.deletAddProduct(Constant.addwishListitem,this.state.customer_id,product_id).then((responseJson)=>{
          this.setState({isLoading:false,showNonFavImage:false});
          Toast.show(responseJson.message,Toast.SHORT);
        })
      }else{
        Alert.alert( '','Are you sure want this item remove wishlist. ',
          [
            {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
            {text: 'OK', onPress: () => 
            {
              //delete product from wishlist
              this.setState({ isLoading: true });
              Service.deletAddProduct(Constant.deletewishlistitem,this.state.customer_id,product_id).then((responseJson)=>{
                if(responseJson.status==1)
                {
                  this.setState({isLoading:false,showNonFavImage:true});
                  Toast.show(responseJson.message,Toast.SHORT);
                }else{
                  Toast.show(responseJson.message,Toast.SHORT);
                }
              });
            }
          },
          ],
          { cancelable: false }
        ) 
      }
    }
  }
}