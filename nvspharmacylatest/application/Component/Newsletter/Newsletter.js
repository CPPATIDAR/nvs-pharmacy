import React, { Component } from 'react';
import {View,ScrollView,Text,TextInput,TouchableHighlight,BackHandler,NetInfo,Alert,ActivityIndicator} from 'react-native';
import Constant from '../../../Constant/Constant';
import Icon from 'react-native-vector-icons/Ionicons'
import Toast from 'react-native-simple-toast';
import Service from '../../config/Service';

export class Newsletter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      error: null,
      emailError:"",
      email:"",
    };
  }

  componentWillMount() {     
     BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  //hardware back button
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  //harware mobile back button
  handleBackButton = () => {
  this.props.navigation.navigate('Home');
    return true;
  } 
   
  render() {
          return (
            this.state.isLoading?
            <View style={Constant.styles.indicatoreViewsTyle}> 
                <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
            </View>
            :
              <ScrollView style={Constant.styles.container}>
                <View style={[Constant.styles.containerwithp10]}>
                  <TextInput onChangeText={email => this.setState({email})} placeholder="Sign up for our Newsletter" underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                  {!!this.state.emailError && (<Text style={Constant.styles.redregulartext}>{this.state.emailError}</Text>)}
                  <TouchableHighlight style={Constant.styles.darkGreenbutton} onPress={()=>this.submitReview()}>
                    <Text style={Constant.styles.whitebuttontext}>{Constant.StringText.NEWSLETTERSUBSCRIPTION}</Text>
                  </TouchableHighlight> 
                </View>
              </ScrollView>
          );
    }

     //submit review with validation 
     submitReview = () =>
     {
      if (this.state.email=="") 
      {
        this.setState(() => ({ emailError: Constant.StringText.REQUIREFILED}));
      }  
      else {
        //check Internet connectivity 
        NetInfo.getConnectionInfo().then((conectionInfo)=>
        {
        if(conectionInfo.type!="none")
        {
            //calling webservice submit feedback data  
            this.setState({ isLoading: true });
            Service.newslettersubscription(Constant.emailsubscribe,this.state.email).then((responseJson)=>{
                Toast.show(responseJson.message,Toast.SHORT);
                this.setState({isLoading: false});
                if(responseJson.status==1)
                {
                    this.props.navigation.navigate("Home");
                }
            })
        }
        else{
            Toast.show(Constant.StringText.CHECKINTERNETCONNECTIVITY,Toast.SHORT);  
        }
        });
      }
     }
}