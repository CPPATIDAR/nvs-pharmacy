import React, { Component } from 'react';
import {RefreshControl,Text,View,BackHandler,AsyncStorage,FlatList,Alert,ActivityIndicator,
  TouchableOpacity,Image,TouchableHighlight} from 'react-native';
import Constant from '../../../Constant/Constant';
import CrossIcon from 'react-native-vector-icons/Entypo';
import Toast from 'react-native-simple-toast';
import Service from '../../config/Service';
import ImageView from 'react-native-image-progress';
import TopArrow from 'react-native-vector-icons/FontAwesome'


export default class WishList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      customer_id:"",
      dataSource:[],
      isLoading: false,
      error: null,
      refreshing: false,
    };
  }
  //handle tab event
  handleKey = async(e) => {
    if(e.state.routeName=="WishList")
    {
      var customer_id = await AsyncStorage.getItem("customer_id");
      this.setState({customer_id:customer_id});
      if(customer_id!=null&&customer_id!="")
      {
        this.getWishListing();
      }else{
        this.props.navigation.navigate('Account');
      }
    }
  } 
  /**
     * Call _fetchData after component has been mounted
  */
  async componentWillMount() {
    this.props.navigation.addListener('willFocus',this.handleKey); 
    var customer_id = await AsyncStorage.getItem("customer_id");
    this.setState({customer_id:customer_id});
    var cartcout = await AsyncStorage.getItem("cartcout"); 
    this.props.navigation.setParams({cartcout:cartcout});
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  //get all wishlist items
  getWishListing(){
    //webservice to get item list
    this.setState({ isLoading: true });
    Service.getUserDetail(Constant.getwishlist,this.state.customer_id).then((responseJson)=>{
        this.setState({isLoading:false,refreshing:false});
          if(responseJson.status==1)
          {
              if(responseJson.items.length!=0)
              {
                this.setState({dataSource:responseJson.items,isLoading:false,refreshing:false});
                this.props.navigation.setParams({wishlistcount:responseJson.items.length+""});
              }else{
                Toast.show(Constant.StringText.DATANOTAVAILABLE,Toast.SHORT);
              }
          }
          else{
            Toast.show(Constant.StringText.DATANOTAVAILABLE,Toast.SHORT);
          }
    });
  }
  //handle back hardware button
  handleBackButton = () => {
    this.props.navigation.navigate('Home');
    return true;
  } 
  //render item for listview
  renderItem(data) {
  let { item, index } = data;  
  return (
    <View style={Constant.styles.listcontainer}>
        <TouchableOpacity activeOpacity={0.7} style={Constant.styles.listImageView} onPress={() => {this.navigateProductDetail('ProductDetail',{product_id:item.product_id,quantity:item.quantity})}}>
          <ImageView 
              source={{uri:item.image}}
              style={Constant.styles.listImage} 
              indicator={ImageView.Pie}
              indicatorProps={{size: 40,color: '#006d50'}}/>
        </TouchableOpacity>
      <View style={Constant.styles.m2Left}> 
          <Text style={Constant.styles.listSmallText}>{item.name.toUpperCase()}</Text>
          <View style={Constant.styles.listRow}> 
            {item.size!=null?
              <Text style={Constant.styles.listSmallText}>{Constant.StringText.SIZE}{item.size}</Text>
            :null}
          </View>
          
          {item.special_price==null?
          <View style={Constant.styles.listRow}> 
            <Text style={Constant.styles.listSmallText}>{Constant.StringText.NOW}{parseFloat(item.price).toFixed(2)}</Text>
          </View>
          :
          <View style={Constant.styles.listRow}> 
            <Text style={Constant.styles.listSmallRedText}>{Constant.StringText.NOW}{parseFloat(item.special_price).toFixed(2)}</Text>
            <Text style={Constant.styles.crossText}>{Constant.StringText.WAS}{parseFloat(item.price).toFixed(2)}</Text>
          </View>
          }
      </View> 
        <TouchableHighlight style={{alignSelf:'flex-start'}} onPress={()=>{this._delete(item.product_id)}}>
          <CrossIcon name={"cross"} size={25} color={'#000000'}/>
        </TouchableHighlight>
    </View>
    ) 
  }

  //refersh list view
  _onRefresh = () => {
  this.setState({refreshing: true});
  this.getWishListing();
  }

  //list seperator
  renderSeparator = () => {
    return (<View style={Constant.styles.separator}/>);
  };
  /**
   * Renders the list
   */
  render(){
    return( 
      this.state.isLoading?
      <View style={Constant.styles.indicatoreViewsTyle}> 
          <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
      </View>
      :
    <View style={Constant.styles.cartView}>
     {this.state.dataSource.length!=0?
        <View style={Constant.styles.p5CenterRow}>
            <Text style={Constant.styles.blacknormalleftText}>{this.state.dataSource.length} {Constant.StringText.ITEMS}</Text>
            <Text style={Constant.styles.greenboldrightText} onPress={()=>this._clear()}>{Constant.StringText.CLEARALL}</Text>
        </View>:null}
        {this.state.dataSource.length!=0?
          <View style={{flex:1}}>
          <FlatList
            keyExtractor = {( item, index ) => index }
            data={this.state.dataSource}
            renderItem={this.renderItem.bind(this)}
            ItemSeparatorComponent={this.renderSeparator}
            ref='ListView_Reference'
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
          />
           <TouchableOpacity style={Constant.styles.stickybutton} onPress={this.scrollToTop}>
                <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
            </TouchableOpacity>
        </View>
        :null}
    </View>
    );
  }
  //top of listview
  scrollToTop =()=>{
    this.refs.ListView_Reference.scrollToOffset({ offset: 0, animated: true });
  }
  //clear all product from list
  _clear(){
    Alert.alert(
      '',
      'Are you sure want clear  all item. ',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'OK', onPress: () => 
        {
          //For clear all product from list
          this.setState({isLoading:true});
          Service.getUserDetail(Constant.clearallwishlist,this.state.customer_id).then((responseJson)=>{
            Toast.show(responseJson.message,Toast.SHORT);
            this.setState({isLoading:false});
            this.props.navigation.setParams({wishlistcount:responseJson.items.length+""});
            if(responseJson.status==1)
            {
             this.setState({ dataSource:[]});
              this.getWishListing();
            }
          })
        }},
      ],
      { cancelable: false }
    ) 
  }

  //delete item from list
  _delete=(product_id)=> {
    Alert.alert(
      '',
      'Are you sure want delete this item. ',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'OK', onPress: () => {
          this.setState({isLoading:true});
          Service.deletAddProduct(Constant.deletewishlistitem,this.state.customer_id,product_id).then((responseJson)=>{
            Toast.show(responseJson.message,Toast.SHORT);
            this.setState({isLoading:false});
            if(responseJson.status==1)
            {
              var dataSource = [...this.state.dataSource]
              let index = dataSource.indexOf(product_id);
              dataSource.splice(index, 1);
              this.setState({dataSource});
              this.getWishListing();
            }
          })
        } },
      ],
      { cancelable: false }
    ) 
  }
  //navigate another page with param
  navigateProductDetail = (SCREEN,data) => {
    if(data.product_id!==undefined && typeof data.product_id!="undefined"){
      this.props.navigation.navigate(SCREEN,data);
    }
  } 
}