import React, { Component } from 'react';
import {View,ScrollView,Text,TouchableOpacity,BackHandler} from 'react-native';
import Constant from '../../../Constant/Constant';
import Icon from 'react-native-vector-icons/Ionicons'
import TopArrow from 'react-native-vector-icons/FontAwesome'

export class CustomerService extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      error: null,
    };
  }

  componentWillMount() {     
     BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  //hardware back button
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  //harware mobile back button
  handleBackButton = () => {
  this.props.navigation.navigate('Home');
    return true;
  } 

  //navigateToanother
  navigateToanother(SCREEN){
    this.props.navigation.navigate(SCREEN);
  }
  render() {
        return (
              <ScrollView style={Constant.styles.container}>
                <View style={Constant.styles.containerwithp10}>
                        <TouchableOpacity style={Constant.styles.listitemBlock} onPress={()=>this.navigateToanother('FAQS')}>
                            <Text style={Constant.styles.listitemNameParent}>{Constant.StringText.FAQS}</Text>
                            <Icon name="ios-arrow-forward" size={28} color="#000000"/>
                        </TouchableOpacity>
                        <TouchableOpacity style={Constant.styles.listitemBlock} onPress={()=>this.navigateToanother('TrackingOrder')}>
                            <Text style={Constant.styles.listitemNameParent}>{Constant.StringText.TRACKINGORDER}</Text>
                            <Icon name="ios-arrow-forward" size={28} color="#000000"/>
                        </TouchableOpacity>
                        <TouchableOpacity style={Constant.styles.listitemBlock} onPress={()=>this.navigateToanother('TermNCondition')}>
                            <Text style={Constant.styles.listitemNameParent}>{Constant.StringText.TERMANDCONDITION}</Text>
                            <Icon name="ios-arrow-forward" size={28} color="#000000"/>
                        </TouchableOpacity>
                        <TouchableOpacity style={Constant.styles.listitemBlock} onPress={()=>this.navigateToanother('PrivacyPolicy')}>
                            <Text style={Constant.styles.listitemNameParent}>{Constant.StringText.PRIVACYPOLICY}</Text>
                            <Icon name="ios-arrow-forward" size={28} color="#000000"/>
                        </TouchableOpacity>
                        <TouchableOpacity style={Constant.styles.listitemBlock} onPress={()=>this.navigateToanother('ReturnPolicy')}>
                            <Text style={Constant.styles.listitemNameParent}>{Constant.StringText.RETURNPOLICY}</Text>
                            <Icon name="ios-arrow-forward" size={28} color="#000000"/>
                        </TouchableOpacity>
                        <TouchableOpacity style={Constant.styles.listitemBlock} onPress={()=>this.navigateToanother('DataProtectionPolicy')}>
                            <Text style={Constant.styles.listitemNameParent}>{Constant.StringText.DATAPROTECTIONPOLICY}</Text>
                            <Icon name="ios-arrow-forward" size={28} color="#000000"/>
                        </TouchableOpacity>
                        <TouchableOpacity style={Constant.styles.listitemBlock} onPress={()=>this.navigateToanother('ProblemOrComplain')}>
                            <Text style={Constant.styles.listitemNameParent}>{Constant.StringText.PROBLEMSORCOMPLAINS}</Text>
                            <Icon name="ios-arrow-forward" size={28} color="#000000"/>
                        </TouchableOpacity>
                    </View>
              </ScrollView>
          );
    }
}