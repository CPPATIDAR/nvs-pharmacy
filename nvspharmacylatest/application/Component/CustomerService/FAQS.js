import React, { Component } from 'react';
import {View,Text,ScrollView,BackHandler,ActivityIndicator,StyleSheet,NetInfo,TouchableOpacity} from 'react-native';
import Constant from '../../../Constant/Constant';
import CouponPanel from '../CommonPanel/CouponPanel';
import Icon from 'react-native-vector-icons/Ionicons';
import Panel from '../CommonPanel/Panel';
import HTMLView from 'react-native-htmlview';
import Toast from 'react-native-simple-toast';
import Service from '../../config/Service';
import TopArrow from 'react-native-vector-icons/FontAwesome'

export class FAQS extends Component {
  constructor(props) {
    super(props);
    this.state = {
        title1:"",
        title1_tab1:"",
        title1_tab1_content:"",
        title2:"",
        title2_tab1:"",
        title2_tab1_content:"",
        title2_tab2:"",
        title2_tab2_content:"",
        title3:"",
        title3_tab1:"",
        title3_tab1_content:"",
        title3_tab2:"",
        title3_tab2_content:"",
        title3_tab3:"",
        title3_tab3_content:"",
        title3_tab4:"",
        title3_tab4_content:"",
        title3_tab5:"",
        title3_tab5_content:"",
        title3_tab6:"",
        title3_tab6_content:"",
        title4:"",
        title4_tab1:"",
        title4_tab1_content:"",
        title4_tab2:"",
        title4_tab2_content:"",
        title5:"",
        title5_tab1:"",
        title5_tab1_content:"",
        title5_tab2:"",
        title5_tab2_content:"",
        title5_tab3:"",
        title5_tab3_content:"", 
        title5_tab4:"",
        title5_tab4_content:"",
        title6:"",
        title6_tab1:"",
        title6_tab1_content:"",
        title6_tab2:"",
        title6_tab2_content:"",
        title6_tab3:"",
        title6_tab3_content:"",
        title7:"",
        title7_tab1:"",
        title7_tab1_content:"",
        title7_tab2:"",
        title7_tab2_content:"",
        title7_tab3:"",
        title7_tab3_content:"",
        title8:"",
        title8_tab1:"",
        title8_tab1_content:"",
        title8_tab2:"",
        title8_tab2_content:"",
        title8_tab3:"",
        title8_tab3_content:"",
        title8_tab4:"",
        title8_tab4_content:"",
        title8_tab5:"",
        title8_tab5_content:"",
        title8_tab6:"",
        title8_tab6_content:"",
        title8_tab7:"",
        title8_tab7_content:"",
        isLoading: false,
        error: null
    };
  }
  
  componentWillMount() {   
    //calling webservice fetch data  
   this.setState({ isLoading: true });
   //check Internet connectivity 
   NetInfo.getConnectionInfo().then((conectionInfo)=>
   {
     if(conectionInfo.type!="none")
     {
       //webservice calling
       Service.getdataFromService(Constant.getFaqscontent).then((responseJson)=>
       {
        this.setState({
            title1:responseJson.title1,
            title1_tab1:responseJson.title1_tab1,
            title1_tab1_content:responseJson.title1_tab1_content,
            title2:responseJson.title2,
            title2_tab1:responseJson.title2_tab1,
            title2_tab1_content:responseJson.title2_tab1_content,
            title2_tab2:responseJson.title2_tab2,
            title2_tab2_content:responseJson.title2_tab2_content,
            title3:responseJson.title3,
            title3_tab1:responseJson.title3_tab1,
            title3_tab1_content:responseJson.title3_tab1_content,
            title3_tab2:responseJson.title3_tab2,
            title3_tab2_content:responseJson.title3_tab2_content,
            title3_tab3:responseJson.title3_tab3,
            title3_tab3_content:responseJson.title3_tab3_content,
            title3_tab4:responseJson.title3_tab4,
            title3_tab4_content:responseJson.title3_tab4_content,
            title3_tab5:responseJson.title3_tab5,
            title3_tab5_content:responseJson.title3_tab5_content,
            title3_tab6:responseJson.title3_tab6,
            title3_tab6_content:responseJson.title3_tab6_content,
            title4:responseJson.title4,
            title4_tab1:responseJson.title4_tab1,
            title4_tab1_content:responseJson.title4_tab1_content,
            title4_tab2:responseJson.title4_tab2,
            title4_tab2_content:responseJson.title4_tab2_content,
            title5:responseJson.title5,
            title5_tab1:responseJson.title5_tab1,
            title5_tab1_content:responseJson.title5_tab1_content,
            title5_tab2:responseJson.title5_tab2,
            title5_tab2_content:responseJson.title5_tab2_content,
            title5_tab3:responseJson.title5_tab3,
            title5_tab3_content:responseJson.title5_tab3_content, 
            title5_tab4:responseJson.title5_tab4,
            title5_tab4_content:responseJson.title5_tab4_content,
            title6:responseJson.title6,
            title6_tab1:responseJson.title6_tab1,
            title6_tab1_content:responseJson.title6_tab1_content,
            title6_tab2:responseJson.title6_tab2,
            title6_tab2_content:responseJson.title6_tab2_content,
            title6_tab3:responseJson.title6_tab3,
            title6_tab3_content:responseJson.title6_tab3_content,
            title7:responseJson.title7,
            title7_tab1:responseJson.title7_tab1,
            title7_tab1_content:responseJson.title7_tab1_content,
            title7_tab2:responseJson.title7_tab2,
            title7_tab2_content:responseJson.title7_tab2_content,
            title7_tab3:responseJson.title7_tab3,
            title7_tab3_content:responseJson.title7_tab3_content,
            title8:responseJson.title8,
            title8_tab1:responseJson.title8_tab1,
            title8_tab1_content:responseJson.title8_tab1_content,
            title8_tab2:responseJson.title8_tab2,
            title8_tab2_content:responseJson.title8_tab2_content,
            title8_tab3:responseJson.title8_tab3,
            title8_tab3_content:responseJson.title8_tab3_content,
            title8_tab4:responseJson.title8_tab4,
            title8_tab4_content:responseJson.title8_tab4_content,
            title8_tab5:responseJson.title8_tab5,
            title8_tab5_content:responseJson.title8_tab5_content,
            title8_tab6:responseJson.title8_tab6,
            title8_tab6_content:responseJson.title8_tab6_content,
            title8_tab7:responseJson.title8_tab7,
            title8_tab7_content:responseJson.title8_tab7_content,
         isLoading: false
        });
       });
     }
     else{
       Toast.show(Constant.StringText.CHECKINTERNETCONNECTIVITY,Toast.SHORT);  
     }
   })
  }

  render() {
    return( 
        this.state.isLoading?
        <View style={Constant.styles.indicatoreViewsTyle}> 
            <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
        </View>
        :
        <View style={Constant.styles.container}>
        <ScrollView style={Constant.styles.container} ref={(scroller) => {this.scroller = scroller}}>
                    <View style={Constant.styles.containerwithp10}>
                    {/* first tab */}
                     <Text style={Constant.styles.greenSmallText}>{this.state.title1}</Text>
                     <Panel title={this.state.title1_tab1}>
                         <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title1_tab1_content}/>
                     </Panel>
                      {/* second tab */}
                     <Text style={Constant.styles.greenSmallText}>{this.state.title2}</Text>
                     <Panel title={this.state.title2_tab1}>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title2_tab1_content}/>
                     </Panel>
                     <Panel title={this.state.title2_tab2}>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title2_tab2_content}/>
                     </Panel>
                      {/* third tab */}
                     <Text style={Constant.styles.greenSmallText}>{this.state.title3}</Text>
                     <Panel title={this.state.title3_tab1}>
                         <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title3_tab1_content}/>
                     </Panel>
                     <Panel title={this.state.title3_tab2}>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title3_tab2_content}/>
                     </Panel>
                     <Panel title={this.state.title3_tab3}>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title3_tab3_content}/>
                     </Panel>
                     <Panel title={this.state.title3_tab4}>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title3_tab4_content}/>
                     </Panel>
                     <Panel title={this.state.title3_tab5}>
                         <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title3_tab5_content}/>
                     </Panel>
                     <Panel title={this.state.title3_tab6}>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title3_tab6_content}/>
                     </Panel>
                      {/* fourth tab */}
                     <Text style={Constant.styles.greenSmallText}>{this.state.title4}</Text>
                     <Panel title={this.state.title4_tab1}>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title4_tab1_content}/>
                     </Panel>
                     <Panel title={this.state.title4_tab2}>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title4_tab2_content}/>
                     </Panel>
                      {/* five tab */}
                     <Text style={Constant.styles.greenSmallText}>{this.state.title5}</Text>
                     <Panel title={this.state.title5_tab1}>
                         <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title5_tab1_content}/>
                     </Panel>
                     <Panel title={this.state.title5_tab2}>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title5_tab2_content}/>
                     </Panel>
                     <Panel title={this.state.title5_tab3}>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title5_tab3_content}/>
                     </Panel>
                     <Panel title={this.state.title5_tab4}>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title5_tab4_content}/>
                     </Panel>
                      {/* six tab */}
                     <Text style={Constant.styles.greenSmallText}>{this.state.title6}</Text>
                     <Panel title={this.state.title6_tab1}>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title6_tab1_content}/>
                     </Panel>
                     <Panel title={this.state.title6_tab2}>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title6_tab2_content}/>
                     </Panel>
                     <Panel title={this.state.title6_tab3}>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title6_tab3_content}/>
                     </Panel>
                      {/* seven tab */}
                     <Text style={Constant.styles.greenSmallText}>{this.state.title7}</Text>
                     <Panel title={this.state.title7_tab1}>
                         <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title7_tab1_content}/>
                     </Panel>
                     <Panel title={this.state.title7_tab2}>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title7_tab2_content}/>
                     </Panel>
                     <Panel title={this.state.title7_tab3}>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title7_tab3_content}/>
                     </Panel>
                      {/* eight tab */}
                     <Text style={Constant.styles.greenSmallText}>{this.state.title8}</Text>
                     <Panel title={this.state.title8_tab1}>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title8_tab1_content}/>
                     </Panel>
                     <Panel title={this.state.title8_tab2}>
                         <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title8_tab2_content}/>
                     </Panel>
                     <Panel title={this.state.title8_tab3}>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title8_tab3_content}/>
                     </Panel>
                     <Panel title={this.state.title8_tab4}>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title8_tab4_content}/>
                     </Panel>
                     <Panel title={this.state.title8_tab5}>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title8_tab5_content}/>
                     </Panel>
                     <Panel title={this.state.title8_tab6}>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title8_tab6_content}/>
                     </Panel>
                     <Panel title={this.state.title8_tab7}>
                        <HTMLView stylesheet={this.styles} textComponentProps={{ style:{color:'#000000'}}} value={this.state.title8_tab7_content}/>
                     </Panel>
                    </View>
            </ScrollView>
            <TouchableOpacity style={Constant.styles.stickybutton} onPress={this.scrollToTop}>
              <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
        </TouchableOpacity>
            </View>
        );
    }

    //top of screen
    scrollToTop = () => {
      this.scroller.scrollTo({x: 0, y: 0});
    };

    //for html text because this is not take common style
    styles = StyleSheet.create({
        a: {
          fontWeight:'300',
          color: '#006d50', // pink links
        },
    })
}