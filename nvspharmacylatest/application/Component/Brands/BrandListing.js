import React, { Component } from 'react';
import {View,Text,TextInput,TouchableOpacity,BackHandler,NetInfo,FlatList,ActivityIndicator,AsyncStorage} from 'react-native';
import Constant from '../../../Constant/Constant';
import IconSearch from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/Ionicons'
import Toast from 'react-native-simple-toast';
import Service from '../../config/Service';
import TopArrow from 'react-native-vector-icons/FontAwesome'
 
//class to implement functionality for brand listing
export class BrandListing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource:[],
      isLoading: false,
      error: null,
      text:""
    };
  }

 //handle tab event
 handleKey = e => {
  if(e.state.routeName=="BrandListing")
  {
     //check Internet connectivity 
     NetInfo.getConnectionInfo().then((conectionInfo)=>
    {
      if(conectionInfo.type!="none")
      {
        //webservice calling
        Service.getdataFromService(Constant.getBrandCategories).then((responseJson)=>
        {
          this.setState({dataSource: responseJson,isLoading: false});
        });
      }
      else{
        Toast.show(Constant.StringText.CHECKINTERNETCONNECTIVITY,Toast.SHORT);  
      }
    })
  }
  } 

  async componentWillMount() {   
    this.props.navigation.addListener('willFocus',this.handleKey);  
    this.setState({ isLoading: true });
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount(newProps) {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  //handle hardware bck button
  handleBackButton = () => {
  this.props.navigation.navigate('Home');
    return true;
  } 
  
  //submit search text
  submitTextInput=async()=>{
    await AsyncStorage.setItem('input_text',this.state.text);
    if(this.state.text=="")
    {
     Toast.show(Constant.StringText.WhatAreLookingFor,Toast.SHORT);
    }else{
     this.props.navigation.navigate('Featured',{input_text:this.state.text}); 
    }
   }
  
  //navigate to another with param
  async NavigateAnother (SCREEN,data) {
    if(data.category_id!==undefined && typeof data.category_id!="undefined"){
      try {
        await AsyncStorage.setItem('category_id', data.category_id+"");
        await AsyncStorage.setItem('is_brand', data.brand+"");
        await AsyncStorage.setItem("input_text","");
      } catch (error) {
        // Error saving data
        console.log(error)
      }
      this.props.navigation.navigate(SCREEN,data);
    }
  } 

  //render listitem
  renderItem(data) {
    let { item, index } = data;    
      return (
        <TouchableOpacity key={index} onPress={() => {this.NavigateAnother('Featured',{category_id:item.catergory_id,brand:1,input_text:""})}}>
          <View style={Constant.styles.listitemBlock}>
            <Text style={Constant.styles.listitemNameParent}>{item.name}</Text>
        </View>
      </TouchableOpacity>
    ); 
  }
 
  render() {
    return( 
      this.state.isLoading?
      <View style={Constant.styles.indicatoreViewsTyle}> 
          <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
      </View>
      :
       <View style={Constant.styles.container}>
                <View style={Constant.styles.searchViewGreenBack}>
                  <View style={Constant.styles.searchSection}>
                    <IconSearch style={Constant.styles.p5} name="search" size={20} color="#000"/>
                    <TextInput
                        style={Constant.styles.input}
                        autoFocus={true} 
                        setFocus={true}
                        autoCorrect={true} 
                        autoCapitalize="none" 
                        placeholder={Constant.StringText.WhatAreLookingFor}
                        underlineColorAndroid='#fff'
                        returnKeyType={"search"} // adding returnKeyType will do the work
                        onChangeText={(text) =>  this.setState({text : text})}
                        onSubmitEditing={() => this.submitTextInput()}
                    />
                  </View>
                </View>
                <Text style={Constant.styles.listitemBlock}>{Constant.StringText.BRANDATOZ}</Text>
                <FlatList 
                    keyExtractor = {( item, index ) => index }
                    data={this.state.dataSource}
                    renderItem={this.renderItem.bind(this)}
                    ref='ListView_Reference'
                />
              <TouchableOpacity style={Constant.styles.stickybutton} onPress={this.scrollToTop}>
                <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
              </TouchableOpacity>
          </View>
          );
    }

    //top of listview
    scrollToTop =()=>{
      this.refs.ListView_Reference.scrollToOffset({ offset: 0, animated: true });
   }
}