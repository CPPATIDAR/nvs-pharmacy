import React, { Component } from 'react';
import {View,ScrollView,Text,TextInput,TouchableOpacity,BackHandler,AsyncStorage} from 'react-native';
import Constant from '../../../Constant/Constant';
import Icon from 'react-native-vector-icons/Ionicons'
import IconSearch from 'react-native-vector-icons/FontAwesome';
import Toast from 'react-native-simple-toast';

export class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      error: null,
      text:""
    };
  }

  async componentWillMount() {     
     BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
     var cartcout = await AsyncStorage.getItem("cartcout"); 
     this.props.navigation.setParams({cartcout:cartcout});
  }
  //hardware back button
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  //harware mobile back button
  handleBackButton = () => {
  this.props.navigation.navigate('Home');
    return true;
  } 

  //navigateToanother
  navigateToanother(SCREEN){
    this.props.navigation.navigate(SCREEN);
  }
  
 //submit search text
 submitTextInput=async()=>{
  await AsyncStorage.setItem('input_text',this.state.text);
  if(this.state.text=="")
  {
   Toast.show(Constant.StringText.WhatAreLookingFor,Toast.SHORT);
  }else{
   this.props.navigation.navigate('Featured',{input_text:this.state.text}); 
  }
 }

  render() {
          return (
            <View style={Constant.styles.container}>
            <View style={Constant.styles.searchViewGreenBack}>
            <View style={Constant.styles.searchSection}>
              <IconSearch style={Constant.styles.p5} name="search" size={20} color="#000"/>
              <TextInput
                        style={Constant.styles.input}
                        autoCorrect={true} 
                        autoCapitalize="none" 
                        placeholder={Constant.StringText.WhatAreLookingFor}
                        underlineColorAndroid='#fff'
                        returnKeyType={"search"} // adding returnKeyType will do the work
                        onChangeText={(text) =>  this.setState({text : text})}
                        onSubmitEditing={() => this.submitTextInput()}
                    />
            </View>
            </View>
              <ScrollView style={Constant.styles.container}>
                <View style={[Constant.styles.containerwithp10]}>
                        <TouchableOpacity style={Constant.styles.listitemBlock} onPress={()=>this.navigateToanother('BrandListing')}>
                            <Text style={Constant.styles.listitemNameParent}>{Constant.StringText.BRANDATOZ}</Text>
                            <Icon name="ios-arrow-forward" size={28} color="#000000"/>
                        </TouchableOpacity>
                        <TouchableOpacity style={Constant.styles.listitemBlock} onPress={()=>this.navigateToanother('CategoryList')}>
                            <Text style={Constant.styles.listitemNameParent}>{Constant.StringText.CATEGORYATOZ}</Text>
                            <Icon name="ios-arrow-forward" size={28} color="#000000"/>
                        </TouchableOpacity>
                </View>
              </ScrollView>
            </View> 
          );
    }
}