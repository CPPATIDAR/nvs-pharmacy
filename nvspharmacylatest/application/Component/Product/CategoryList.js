import React, { Component } from 'react';
import {View,Text,TextInput,TouchableOpacity,BackHandler,ActivityIndicator,AsyncStorage,NetInfo} from 'react-native';
import ExpandableList from 'react-native-expandable-section-flatlist';
import IconSearch from 'react-native-vector-icons/FontAwesome';
import Constant from '../../../Constant/Constant';
import Icon from 'react-native-vector-icons/Ionicons'
import Toast from 'react-native-simple-toast';
import Service from '../../config/Service';
import TopArrow from 'react-native-vector-icons/FontAwesome'

//class to implement functionality for category listing
export class CategoryList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource:[],
      isLoading: false,
      text:"",
    };
  }

  componentWillMount() {   
    //calling webservice fetch data  
   this.setState({ isLoading: true });
   //check Internet connectivity 
   NetInfo.getConnectionInfo().then((conectionInfo)=>
    {
     if(conectionInfo.type!="none")
     {
       //getcategory listing service calling
       Service.getdataFromService(Constant.getProductCategories).then((responseJson)=>
       {
        this.setState({dataSource:responseJson,isLoading: false});
       });
     }
     else{
       Toast.show(Constant.StringText.CHECKINTERNETCONNECTIVITY,Toast.SHORT);  
     }
    })
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  } 

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  //handle back button
  handleBackButton = () => {
    this.props.navigation.navigate('Home');
    return true;
  } 
    
  //submit search text
  submitTextInput=async()=>{
    await AsyncStorage.setItem('input_text',this.state.text);
    if(this.state.text=="")
    {
     Toast.show(Constant.StringText.WhatAreLookingFor,Toast.SHORT);
    }else{
     this.props.navigation.navigate('Featured',{input_text:this.state.text}); 
    }
  }

  //navigate to another screen
  async navigateAnother(SCREEN,data) {
  if(data.category_id!==undefined && typeof data.category_id!="undefined"){
    try {
      await AsyncStorage.setItem('category_id', data.category_id);
      await AsyncStorage.setItem('is_brand',"");
      await AsyncStorage.setItem("input_text","");
    } catch (error) {
      // Error saving data
      Toast.show(error,Toast.SHORT);
    }
    this.props.navigation.navigate(SCREEN,data);
    }
  } 

  //render child list row
  _renderRow = (rowItem, rowId, sectionId) => (
    <TouchableOpacity key={rowId} onPress={() => {this.navigateAnother('Featured',{category_id:rowItem.catergory_id,input_text:""})}}>
            <View style={[Constant.styles.listitemBlock]}>
              <Text style={Constant.styles.listitemNameChild} >{rowItem.name}</Text>
            </View>
    </TouchableOpacity>
  );
    
  //render header list
  _renderSection = (section, sectionId)  => {
    return (
      <View style={Constant.styles.listitemBlock}>
          <Text style={Constant.styles.listitemNameParent}> {section}</Text>
          <Icon name="ios-arrow-forward" size={28} color="#000000"/>
      </View>
    );
  };
    
  render() {
    return( 
      this.state.isLoading?
      <View style={Constant.styles.indicatoreViewsTyle}> 
          <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
      </View>
      :
      <View style={[Constant.styles.container]}>
         <View style={Constant.styles.searchViewGreenBack}>
            <View style={[Constant.styles.searchSection]}>
             <IconSearch style={[Constant.styles.p5]} name="search" size={20} color="#000"/>
             <TextInput
                    style={[Constant.styles.input]}
                    autoFocus={true} 
                    setFocus={true}
                    autoCorrect={true} 
                    autoCapitalize="none" 
                    placeholder={Constant.StringText.WhatAreLookingFor}
                    underlineColorAndroid='#fff'
                    returnKeyType={"search"} // adding returnKeyType will do the work
                    onChangeText={(text) =>  this.setState({text : text})}
                    onSubmitEditing={() => this.submitTextInput()}
              />
            </View>
            </View>
            <Text style={Constant.styles.listitemBlock}>{Constant.StringText.CATEGORYATOZ}</Text>
             <ExpandableList
                  dataSource={this.state.dataSource}
                  headerKey="name"
                  memberKey="children"
                  renderRow={this._renderRow}
                  renderSectionHeaderX={this._renderSection}
                  ref='ListView_Reference'
              />
               <TouchableOpacity style={Constant.styles.stickybutton} onPress={this.scrollToTop}>
                <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
              </TouchableOpacity>
         </View>
      );
  }
  //top of listview
  scrollToTop =()=>{
    this.refs.ListView_Reference.scrollToOffset({ offset: 0, animated: true });
 }
}