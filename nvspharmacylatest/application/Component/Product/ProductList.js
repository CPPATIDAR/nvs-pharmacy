import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Image,TouchableOpacity,Text,View,FlatList,ActivityIndicator,Alert,BackHandler,AsyncStorage,ScrollView} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Constant from '../../../Constant/Constant';
import Toast from 'react-native-simple-toast';
import CrossIcon from 'react-native-vector-icons/Entypo';
import Service from '../../config/Service';
import TopArrow from 'react-native-vector-icons/FontAwesome'
const nonFavImage = require('../../../resource/img/heart.png');
const favImage = require('../../../resource/img/fill_heart.png');
var SampleArray = [] ;
var tabs="" 

export class ProductList extends Component {
    constructor(props) {
      super(props);
      this.onEndReachedCalledDuringMomentum = true;
      this.state = {
        //from another page
        input_text:"",
        category_id:"",
        brand:"0",
        data: [],
        customer_id:"",
        initialExe:true,
        page:1,
        jsondata:{},
        isLoading: false,
        fetching_Status:false,
        error: null,
        refreshing: false,
        sortingvalue:"",
        showSortingImage:true,
        brand_id:"",
      };
    }

    //add text to array
    addItemsToArray=()=>{
      //Adding Items To Array.
      if(this.state.input_text!=""&&this.state.input_text!=null)
      {
        SampleArray.push(this.state.input_text.toString());
        SampleArray = SampleArray.filter( function( item, index, inputArray ) {
          return inputArray.indexOf(item) == index;
        });
      }
    }

    //handle tab event
    handleKey = e => {
      if(e.state.key=="Featured")
      {
        tabs="featured";
      }else if(e.state.key=="Newest"){
        tabs="newest";
      }else if(e.state.key=="Rating"){
        tabs="rating";
      }else if(e.state.key=="Price"){
        tabs="price";
      }
    }
 
    async componentWillMount() { 
      //handle tab click listener  
      this.props.navigation.addListener('willFocus',this.handleKey);
      //handle session data  
      var customer_id = await AsyncStorage.getItem("customer_id");
      var category_id = await AsyncStorage.getItem('category_id')
      var is_brand= await AsyncStorage.getItem('is_brand');
      var jsondata = await AsyncStorage.getItem('data'); 
      var input_text = await AsyncStorage.getItem('input_text');
      var brand_id = await AsyncStorage.getItem('brand_id');
      this.setState({category_id:category_id,customer_id:customer_id,input_text:input_text,
        isLoading:true,jsondata:JSON.parse(jsondata)})
        if(brand_id!='undefined')
        {
          this.setState({brand_id:brand_id.split(",")});
        }else{
          this.setState({brand_id:""});
        }
      if(is_brand!=null && is_brand!=''){
        this.setState({brand:1});
      }
      this.addItemsToArray();
      this.getProduct();      
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    //get product from server
    async getProduct(){
      const { page } = this.state;
      var jsonData2 = {"cat_id":this.state.category_id,"brands":this.state.brand,"tabs":tabs,
      "page":page,"search":this.state.input_text,"orderBy":this.state.sortingvalue,"brand_id":this.state.brand_id};
      if(!this.state.jsondata==null||!this.state.jsondata=="")
      {
        var obj = Object.assign(this.state.jsondata,jsonData2);
        Service.getFilterProductList(Constant.get_categories_products,obj,true).then((responseJson)=>{
        if(responseJson.length==0)
        {
          Toast.show(Constant.StringText.DATANOTAVAILABLE,Toast.SHORT)
          this.setState({isLoading:false,fetching_Status:false});
        }
        else if(responseJson.length>0){
          this.setState({ data: page === 1?responseJson : [...this.state.data, ...responseJson]
          ,isLoading:false,fetching_Status:false});
        } 
        })
        await AsyncStorage.setItem('data',"");
      }
      else
      {
        Service.getFilterProductList(Constant.get_categories_products,jsonData2,false).then((responseJson)=>{
        if(responseJson.length==0)
        {
          Toast.show(Constant.StringText.DATANOTAVAILABLE,Toast.SHORT)
          this.setState({isLoading:false,fetching_Status:false});
        }
        else if(responseJson.length>0){
          this.setState({ data: page === 1?responseJson : [...this.state.data, ...responseJson]
          ,isLoading:false,fetching_Status:false});
        } 
        })
      }
    }
    
    //hardware back button
    componentWillUnmount() {
       //handle tab click listener  remove
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
  
    //harware mobile back button
    handleBackButton = () => {
    this.props.navigation.goBack(null);
      return true;
    }
    
    //navigate another page with param
    navigateAnother = (SCREEN,data) => {
      if(data.product_id!==undefined && typeof data.product_id!="undefined"){
        this.props.navigation.navigate(SCREEN,data);
      }
    } 

     //render fav heart image
    renderImage = (product_id) => {
      var imgSource = this.state["showNonFavImage"+product_id] ? favImage : nonFavImage;
      return (<Image style={Constant.styles.wishlistimageSize} source={ imgSource }/>);
    }
    //click heart image
    hearclick(product_id){
      if(this.state.customer_id==null || this.state.customer_id=="")
      {
        Toast.show(Constant.StringText.NEEDLOGIN,Toast.SHORT); 
      }else{
        if(!this.state["showNonFavImage"+product_id]==true)
        {
          //add to wishlist product
          this.setState({isLoading:true});
          Service.deletAddProduct(Constant.addwishListitem,this.state.customer_id,product_id).then((responseJson)=>{
            this.setState({isLoading:false});
            Toast.show(responseJson.message,Toast.SHORT);
            if(responseJson.status==1)
            {
              this.setState({["showNonFavImage"+product_id]:true});
            }
          })
        }else{
          Alert.alert( '','Are you sure want this item remove from wishlist. ',
          [
            {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
            {text: 'OK', onPress: () =>
            {
              //delete product from wishlist
              this.setState({isLoading:true});
              Service.deletAddProduct(Constant.deletewishlistitem,this.state.customer_id,product_id).then((responseJson)=>{
                this.setState({isLoading:false});
                Toast.show(responseJson.message,Toast.SHORT);
                if(responseJson.status==1)
                {
                  this.setState({["showNonFavImage"+product_id]:false});
                }
              })
            }},
          ],
          { cancelable: false }
          ) 
        }
      }
    }

    //render item for listview
    renderItem(data) {
      let { item, index } = data;    
      return (
        <View style={Constant.styles.productItemBlock}>
            <TouchableOpacity  onPress={() =>this.hearclick(item.product_id)} style={Constant.styles.heartImageView}>
            {this.renderImage(item.product_id)}
            </TouchableOpacity>
            <View style={{marginTop:2}}/>
            <TouchableOpacity  onPress={() => {this.navigateAnother('ProductDetail',{product_id:item.product_id,quantity:""})}}>
            <View style={Constant.styles.productImageView}>
              <Image source={{uri:item.image}} style={Constant.styles.productImageStyle}/>
            </View>
            <View style={Constant.styles.productItemStyle}> 
                  <Text numberOfLines={2} style={Constant.styles.listSmallText}>{item.name.toUpperCase()}</Text>
                  <Text style={Constant.styles.listSmallText}>{Constant.StringText.SIZE}{item.size}</Text>
                  
                  {item.special_price==null?
                  <View style={Constant.styles.listRow}> 
                    <Text style={Constant.styles.listSmallText}>{Constant.StringText.PRICE} {Constant.Currency}{parseFloat(item.price).toFixed(2)}</Text>
                  </View>
                  :
                  <View style={Constant.styles.listRow}> 
                    <Text style={Constant.styles.listSmallRedText}>{Constant.StringText.NOW}{parseFloat(item.special_price).toFixed(2)}</Text>
                    <Text style={Constant.styles.crossText}>{Constant.StringText.WAS}{parseFloat(item.price).toFixed(2)}</Text>
                  </View>
                }
            </View>
          </TouchableOpacity >
        </View>
      ) 
    }

    renderSortingImage=()=>{
      var imgSource = this.state.showSortingImage? Constant.acending : Constant.decending;
      return (
        <View style={{flexDirection:'row'}}>
          <Image style={{alignSelf:"center",height:20,width:20}} resizeMode={'contain'} source={ imgSource } />
          <Text style={Constant.styles.blacknormalcenterText}>{Constant.StringText.SORTING}</Text>
        </View>
      )
    }

    sortingClick=()=>{
      if(this.state.showSortingImage==true)
      {
        this.setState({showSortingImage:!this.state.showSortingImage,sortingvalue:"LowToHigh"});
      }else{
        this.setState({showSortingImage:!this.state.showSortingImage,sortingvalue:"HighToLow"});
      }
      this.getProduct();
    }

    removeSearch=(item)=>
    {
      Alert.alert( '','Are you sure want this item remove. ',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'OK', onPress: async() =>
        {

         // var index = SampleArray.indexOf(item);
          var todos = [...SampleArray]
          SampleArray= todos.splice(item, 1);
          if(SampleArray.length==0)
          {
            SampleArray=[];
            await AsyncStorage.setItem('input_text',"");
          }
        }},
      ],
      { cancelable: false }
      ) 
    }

    render() {
      return( 
        this.state.isLoading?
        <View style={Constant.styles.indicatoreViewsTyle}> 
            <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
        </View>
        :
          <View style={{flex:1,backgroundColor:'#ffffff'}}>
          {this.state.input_text!=""&&this.state.input_text!=null?
          <View>
          <ScrollView style={{margin:5}} horizontal={true}>
          {SampleArray.map((item,key)=>  
                <View key={key} style={Constant.styles.tagTextView}>
                          <Text key={key}>{item}</Text>
                          {/* <CrossIcon name={"circle-with-cross"} size={18} padding={10} color={'#000000'} onPress={()=>this.removeSearch(SampleArray,item)}/> */}
                </View>)}
          </ScrollView>
          </View>
          :null} 
            <View style={{flex:1}}>
            {this.state.data.length!=0?
               <View style={{flexDirection:'row'}}>
               <TouchableOpacity style={Constant.styles.boxbordershorting} onPress={()=>this.sortingClick() } >
                 {this.renderSortingImage()}
               </TouchableOpacity>
             </View> :null}
              <FlatList 
                    keyExtractor = {( item, index ) => index }
                    data={this.state.data}
                    ListFooterComponent = { this.renderFooter }
                    renderItem={this.renderItem.bind(this)}
                    numColumns={2}
                    onRefresh={this.handleRefresh}
                    refreshing={this.state.refreshing}
                    onEndReachedThreshold={0.5}
                    ref='ListView_Reference'
                    onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                    onEndReached={async(distanceFromEnd)=> {
                   if(!this.onEndReachedCalledDuringMomentum){
                     this.setState({fetching_Status:true});
                     var jsonData2 = {"cat_id":this.state.category_id,"brands":this.state.brand,"tabs":tabs,
                     "page":this.state.page+1,"search":this.state.input_text,"orderBy":this.state.sortingvalue};
                     if(!this.state.jsondata==null||!this.state.jsondata=="")
                     {
                       var obj = Object.assign(this.state.jsondata,jsonData2);
                       Service.getFilterProductList(Constant.get_categories_products,obj,false).then((responseJson)=>{
                       if(responseJson.length==0)
                       {
                         Toast.show(Constant.StringText.DATANOTAVAILABLE,Toast.SHORT)
                         this.setState({isLoading:false,fetching_Status:false});
                       }
                       else if(responseJson.length>0){
                         this.setState({ data: page === 1?responseJson : [...this.state.data, ...responseJson]
                         ,isLoading:false,fetching_Status:false});
                       } 
                       })
                       await AsyncStorage.setItem('data',"");
                     }
                     else
                     {
                       Service.getFilterProductList(Constant.get_categories_products,jsonData2,false).then((responseJson)=>{
                       if(responseJson.length==0)
                       {
                         Toast.show(Constant.StringText.DATANOTAVAILABLE,Toast.SHORT)
                         this.setState({isLoading:false,fetching_Status:false});
                       }
                       else if(responseJson.length>0){
                         this.setState({ data: page === 1?responseJson : [...this.state.data, ...responseJson]
                         ,isLoading:false,fetching_Status:false});
                       } 
                       })
                     }
                    this.onEndReachedCalledDuringMomentum = true;
                    }
                  }
                  }   
              /> 
               <TouchableOpacity style={Constant.styles.stickybutton} onPress={this.scrollToTop}>
                <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
              </TouchableOpacity>
            </View>
          </View>
       );
    }

    //top of listview
    scrollToTop =()=>{
      this.refs.ListView_Reference.scrollToOffset({ offset: 0, animated: true });
    }
    
    //listivew footer
    renderFooter = () => {
      return (
      <View>
        {this.state.fetching_Status?<ActivityIndicator color = "#006d50" size='large'/>:null}
      </View>)
    };
}