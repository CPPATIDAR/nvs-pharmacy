import React, { Component } from 'react';
import { View,Text,Dimensions,TouchableOpacity,Image,AsyncStorage,ActivityIndicator,BackHandler,
  ScrollView} from 'react-native';
import ExpandableList from 'react-native-expandable-section-flatlist';
import Icon from 'react-native-vector-icons/Ionicons';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import Constant from '../../../Constant/Constant'
import Service from '../../config/Service';
import Toast from 'react-native-simple-toast';
import CheckBox from 'react-native-checkbox';
import Panel from '../CommonPanel/Panel'
import TopArrow from 'react-native-vector-icons/FontAwesome'
var arrayOptions = {};

export class FilterScreen extends Component {
    constructor()
    {
      super();
      this.state ={
        dataSource:[],
        values: [],
        category_id:"",
        isLoading: false,
        priceCode:"",
        clear:{},
        attribute_codeArray:{},
      }
    }

    async componentWillMount() { 
      //handle session data  
      var category_id = await AsyncStorage.getItem('category_id');
      this.setState({category_id:category_id});
      this.getFilterProduct(category_id);      
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    //harware mobile back button
    handleBackButton = () => {
      this.props.navigation.goBack(null);
      return true;
    } 
    //calling service for filter category
    getFilterProduct(category_id){
      this.setState({isLoading:true});
      Service.getfilter_category(Constant.layered_navigation1,category_id).then((responseJson)=>{
        this.setState({isLoading:false});
        if(responseJson.status==1)
        {
          this.setState({
            dataSource:responseJson.results.attribute_data,
            priceCode:responseJson.results.price_data.code,
            });
            [...this.state.values.splice(0, 2,responseJson.results.price_data.min_price, responseJson.results.price_data.max_price)];
           this.setState({
             values:[this.state.values[0],this.state.values[1]]
           });
          arrayOptions = {};
           this.state.dataSource.forEach(function(key) {
              arrayOptions[key.attribute_code]=[]; 
            });
            this.setState({attribute_codeArray:arrayOptions});
          }
        else
        {
          Toast.show(responseJson.message,Toast.SHORT);
        }
      })
    }

      //range slider value
      multiSliderValuesChange = (values) => {
        this.setState({values:values});
      }
 
      Capitalize(str){
        return str.charAt(0).toUpperCase() + str.slice(1);
      }

      render() {
        return( 
          this.state.isLoading?
          <View style={Constant.styles.indicatoreViewsTyle}> 
              <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
          </View>
          :
            <View style={Constant.styles.container}>
              <View style={{flexDirection:'column'}}>
                <View style={{flexDirection:'row'}}>
                <View style={Constant.styles.buttonFilter}>
                <Text style={Constant.styles.blacknormalcenterText} onPress={()=>this.navigateAnother('Featured',
              JSON.stringify(this.state.clear))}>{Constant.StringText.CLEARALL}</Text>
                </View>
                <View style={Constant.styles.viewinfilter}></View>
                <View style={Constant.styles.buttonFilter}>
                <Text style={Constant.styles.blacknormalcenterText} onPress={()=>this.navigateAnother('Featured',
                JSON.stringify(this.state.attribute_codeArray))}>{Constant.StringText.DONE}</Text>
                </View>
                </View>
                <View style={Constant.styles.headerfilterText}>
                  <Text style={Constant.styles.listitemNameParent}>{this.Capitalize(this.state.priceCode)}</Text>
                </View>
                <View style={Constant.styles.pricefilterTextview}>
                  <Text style={Constant.styles.sliderfilterLeftText}>{Constant.Currency}{this.state.values[0]}</Text>
                  <Text style={Constant.styles.sliderfilterRightText}>{Constant.Currency}{this.state.values[1]}</Text>
                </View>
                <View style={Constant.styles.sliderstylefilter}>
                <MultiSlider style={{paddingTop:10,alignSelf:'center'}}
                        trackStyle={{height:2}}
                        selectedStyle={{backgroundColor: '#8cc63e'}}
                        unselectedStyle={{backgroundColor: '#000000'}}
                        values={[this.state.values[0], this.state.values[1]]}
                        sliderLength={Dimensions.get('window').width-50}
                        onValuesChange={this.multiSliderValuesChange}
                        min={this.state.values[0]}
                        max={this.state.values[1]}
                        step={10}
                        isMarkersSeparated={true}
                        customMarkerLeft={(e) => {
                          return (<Image source={Constant.slideImagefilter}
                          values={this.state.values[0]}/>)}}
                          
                        customMarkerRight={(e)=> {
                            return (<Image source={Constant.slideImagefilter}
                            values={this.state.values[1]}/>) }}
                    />
                </View>
              </View> 
              <View style={Constant.styles.viewStyle}/>
                <ExpandableList
                    dataSource={this.state.dataSource}
                    headerKey="attribute_code"
                    memberKey="attribute_values"
                    renderRow={this._renderRow}
                    ref='ListView_Reference'
                    renderSectionHeaderX={this._renderSection}
                />
                 <TouchableOpacity style={Constant.styles.stickybutton} onPress={this.scrollToTop}>
                <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
              </TouchableOpacity>
         </View>
        );
      }

      //top of listview
    scrollToTop =()=>{
      this.refs.ListView_Reference.scrollToOffset({ offset: 0, animated: true });
    }
      valueChkbox(e,code,value)
      {
        var filterArray=this.state.attribute_codeArray;
        if(e==true)
        {                      
          filterArray[code].push(value); 
        }else{
          filterArray[code].pop(value);
        }
        this.setState({attribute_codeArray:filterArray});        
      }

      //render child list row
      _renderRow = (rowItem, rowId, sectionId) => (
        <TouchableOpacity key={rowId}>
                <View style={Constant.styles.listitemBlock}>
                  <CheckBox
                    styles={Constant.styles.listitemNameChild}
                    label={[rowItem.label+"("+rowItem.count+")"]}
                    onChange={(e) => this.valueChkbox(e,rowItem.code,rowItem.value)}
                  />
               </View>
        </TouchableOpacity>
      );
    
       //render header list
      _renderSection = (section, sectionId)  => {
        return (
          <View style={Constant.styles.listitemBlock}>
              <Text style={Constant.styles.listitemNameParent}> {this.Capitalize(section)}</Text>
              <Icon name="ios-arrow-forward" size={28} color="#000000"/>
          </View>
        );
      };

       //navigate to another screen
      async navigateAnother(SCREEN,data) {
          var price = {"price":this.state.values};
          var data = JSON.parse(data);
          var obj = Object.assign(price,data);
          if(obj!==undefined && typeof obj!="undefined")
          {
            try {
              await AsyncStorage.setItem('data',JSON.stringify(obj));
            } catch (error) {
              // Error saving data
              Toast.show(error,Toast.SHORT);
            }
            this.props.navigation.navigate(SCREEN);
          }
         
      } 
    
  }