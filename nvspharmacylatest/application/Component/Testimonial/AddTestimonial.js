import React, { Component } from 'react';
import {View,ScrollView,Text,TextInput,TouchableOpacity,ActivityIndicator,AsyncStorage,
BackHandler,NetInfo} from 'react-native';
import Constant from '../../../Constant/Constant';
import Toast from 'react-native-simple-toast';
import Service from '../../config/Service';
import TopArrow from 'react-native-vector-icons/FontAwesome'
import Stars from 'react-native-stars';

export class AddTestimonial extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      error: null,
      name:"",
      email:"",
      rating:"",
      comment:"",
      customer_id:"",
    };
  }
  

 async componentWillMount() {   
   //Session value
    var customer_id = await AsyncStorage.getItem("customer_id");
    var customer_firstname = await AsyncStorage.getItem("customer_firstname");
    var customer_lastname = await AsyncStorage.getItem("customer_lastname");
    var customer_email = await AsyncStorage.getItem("customer_email");
    this.setState({customer_id:customer_id});
    this.setState({name:(customer_firstname+" "+customer_lastname)});
    this.setState({email:customer_email});
    if(!customer_id==""||!customer_id==null)
    {
        this.setState({name:(customer_firstname+" "+customer_lastname),email:customer_email});
    }else{
      this.setState({name:" ",email:""});
    }
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton.bind(this));
    }
    
    //hardware back button
    componentWillUnmount() {
    //handle tab click listener  remove
     BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton.bind(this));
    }

     //harware mobile back button
    handleBackButton(){
      if (this.props.navigation.state.routeName == 'AddTestimonial') {
        this.props.navigation.navigate('Testimonial');
        return true;
      } 
    }

  //navigate to another with validation
  NavigateAnother = (SCREEN) =>{
      if(this.state.name=="")
      {
          Toast.show(Constant.StringText.ENTERNAME,Toast.SHORT);
      }else if(this.state.email=="")
      {
        Toast.show(Constant.StringText.ENTEREMAIL,Toast.SHORT);
      }else if(this.state.comment=="")
      {
        Toast.show(Constant.StringText.ENTERCOMMENT,Toast.SHORT);
      }else{
           //check Internet connectivity 
            NetInfo.getConnectionInfo().then((conectionInfo)=>
            {
            if(conectionInfo.type!="none")
            {
                //calling webservice submit feedback data  
                this.setState({ isLoading: true });
                Service.addtestimonial(Constant.add_testimonial,this.state.name,this.state.email,this.state.comment,this.state.rating).then((responseJson)=>{
                    Toast.show(responseJson.message,Toast.SHORT);
                    this.setState({isLoading: false});
                    if(responseJson.status==1)
                    {
                        this.props.navigation.navigate("Testimonial");
                    }
                })
            }
            else{
                Toast.show(Constant.StringText.CHECKINTERNETCONNECTIVITY,Toast.SHORT);  
            }
            });
      }
  } 

  render() {
    return( 
      this.state.isLoading?
      <View style={Constant.styles.indicatoreViewsTyle}> 
          <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
      </View>
      :
      <View style={Constant.styles.containerwithp10}>
            <ScrollView ref={(scroller) => {this.scroller = scroller}}>
              <View style={Constant.styles.containerwithp10}>
                        <View style={Constant.styles.lightGreenPanelAcc}> 
                        <Text style={Constant.styles.whitenormalcentertext}>{Constant.StringText.TESTIMONIAL}</Text>
                        </View>
                        <View flexDirection='row'><Text style={Constant.styles.listSmallText}>{Constant.StringText.NAME}</Text>
                           <Text style={Constant.styles.listSmallRedText}>{Constant.StringText.ASTERISK}</Text></View>
                        <TextInput onChangeText={name => this.setState({name})} value={this.state.name} placeholder={Constant.StringText.NAME} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                        <View flexDirection='row'><Text style={Constant.styles.listSmallText}>{Constant.StringText.EMAILADDRESS}</Text>
                           <Text style={Constant.styles.listSmallRedText}>{Constant.StringText.ASTERISK}</Text></View>
                        <TextInput onChangeText={email => this.setState({email})} value={this.state.email} placeholder={Constant.StringText.EMAIL} underlineColorAndroid='transparent'style={Constant.styles.boxwithborder}/>
                        <View flexDirection='row'><Text style={Constant.styles.listSmallText}>{Constant.StringText.COMMENT}</Text>
                           <Text style={Constant.styles.listSmallRedText}>{Constant.StringText.ASTERISK}</Text></View>
                        <TextInput onChangeText={comment => this.setState({comment})} placeholder={Constant.StringText.COMMENT} multiline={true} underlineColorAndroid='transparent'style={Constant.styles.messageboxwithborder}/>
                        <Text style={Constant.styles.listSmallText}>{Constant.StringText.RATING}</Text>
                        <View style={{alignItems:'flex-start'}}>
                        <Stars
                                    half={true}
                                    update={(val)=>{this.setState({rating: val})}}
                                    spacing={4}
                                    starSize={15}
                                    count={5}
                                    fullStar={Constant.RatedStar}
                                    emptyStar={Constant.InRatedStar}
                                    halfStar={Constant.RatedStar}/>
                        </View>
                        <View style={Constant.styles.m10Top}></View>
                        <TouchableOpacity style={Constant.styles.orangebutton} onPress={()=>this.NavigateAnother('Home')}>
                            <Text style={Constant.styles.whitebuttontext}>{Constant.StringText.SUBMIT}</Text>
                        </TouchableOpacity>
              </View>
            </ScrollView>
          <TouchableOpacity style={Constant.styles.stickybutton} onPress={this.scrollToTop}>
            <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
          </TouchableOpacity>
        </View>
      );
    }
    //top of screen
    scrollToTop = () => {
      this.scroller.scrollTo({x: 0, y: 0});
    };
 
   
}