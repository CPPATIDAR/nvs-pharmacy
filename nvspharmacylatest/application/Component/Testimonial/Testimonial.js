import React, { Component } from 'react';
import {View,ScrollView,Text,TextInput,TouchableOpacity,BackHandler,NetInfo,ActivityIndicator,
    FlatList,Image,AsyncStorage} from 'react-native';
import Constant from '../../../Constant/Constant';
import Icon from 'react-native-vector-icons/Ionicons'
import Service from '../../config/Service';
import TopArrow from 'react-native-vector-icons/FontAwesome'
import ImageView from 'react-native-image-progress';
import Stars from 'react-native-stars';

export class Testimonial extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource:[],
      isLoading: false,
      error: null,
    };
  }

  componentWillMount() {     
    this.props.navigation.addListener('willFocus',this.handleKey);  
    this.setState({ isLoading: true });
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  //hardware back button
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

    //handle tab event
    handleKey = e => {
    if(e.state.routeName=="Testimonial")
    {
       //check Internet connectivity 
       NetInfo.getConnectionInfo().then((conectionInfo)=>
      {
        if(conectionInfo.type!="none")
        {
          //webservice calling
          Service.getdataFromService(Constant.testimonial_list).
          then((responseJson)=>
          {
            this.setState({dataSource: responseJson.data,isLoading: false});
          });
        }
        else{
          Toast.show(Constant.StringText.CHECKINTERNETCONNECTIVITY,Toast.SHORT);  
        }
      })
    }
    } 

  //harware mobile back button
  handleBackButton = () => {
  this.props.navigation.navigate('Home');
    return true;
  } 

  //navigateToanother
  navigateToanother(SCREEN){
    this.props.navigation.navigate(SCREEN);
  }
  render() {
    return( 
        this.state.isLoading?
        <View style={Constant.styles.indicatoreViewsTyle}> 
            <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
        </View>
        :
        <View style={Constant.styles.container}>
          <View style={Constant.styles.testimonialbuttonView}>
            <TouchableOpacity style={Constant.styles.darkGreenbutton} onPress={()=>this.props.navigation.navigate('AddTestimonial')}>
                <Text style={Constant.styles.whitebuttontext}>{Constant.StringText.SUBMITYOURTESTIMONIAL}</Text>
            </TouchableOpacity>
          </View>
            <View>
             <FlatList
                 keyExtractor = {( item, index ) => index }
                 data={this.state.dataSource}
                 renderItem={this.renderItem.bind(this)}
                 ref='ListView_Reference'
               />   
               </View>
          <TouchableOpacity style={Constant.styles.stickybutton} onPress={this.scrollToTop}>
                  <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
              </TouchableOpacity>
         </View>
        );
    }
  
    //render listitem
    renderItem(data) {
        let { item, index } = data;    
        return (
        <View>
            <View style={Constant.styles.simpleBox}>
                <Text style={Constant.styles.italicblacktext}>{item.message}</Text> 
                <Text style={Constant.styles.greenboldleftText}>{item.name}</Text> 
            </View>
           <View style={{flexDirection:'row',paddingLeft:10,paddingRight:10}}>
             <View style={{flexDirection:'row',flex:1,justifyContent:'flex-start'}}>
              <Text style={Constant.styles.listSmallText}>{Constant.StringText.RATING}</Text> 
              <Stars
                  half={true}
                  display={item.rating}
                  spacing={4}
                  count={5}
                  starSize={15}
                  fullStar={Constant.RatedStar}
                  emptyStar={Constant.InRatedStar}
                  halfStar={Constant.RatedStar}/>
             </View>
             <View style={{flexDirection:'row',flex:1,justifyContent:'flex-end'}}>
                <Text style={Constant.styles.listSmallText}>Place on {item.date}</Text> 
              </View>
           </View>
          <View style={Constant.styles.viewStyle}></View>
        </View>
        ); 
    }

     //top of listview
     scrollToTop =()=>{
        this.refs.ListView_Reference.scrollToOffset({ offset: 0, animated: true });
     }
}