import React, { Component } from 'react';
import {Text,Image,ScrollView,View,TextInput,Dimensions,BackHandler,Alert,TouchableOpacity,ActivityIndicator,Linking,AsyncStorage,NetInfo} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import Icon from 'react-native-vector-icons/FontAwesome';
import ArrowIcon from 'react-native-vector-icons/Ionicons';
import Grid from 'react-native-grid-component';
import Constant from '../../../Constant/Constant'
import Toast from 'react-native-simple-toast';
import Service  from '../../config/Service';
import TopArrow from 'react-native-vector-icons/FontAwesome'
import Swiper from 'react-native-swiper'
import Ionicons from 'react-native-vector-icons/Ionicons';
  
export class Home extends Component{
  constructor(props) {
      super(props);
      this.state = {
          text :"",
          carouselElements:[],
          isLoading: false,
          error: null,
          brands:[],
          discovermore:[],
          giftitem:[],
      };
  }

  //handle tab event
  handleKey = async(e) => {
   if(e.state.routeName=="Home")
   {
      var cartcout = await AsyncStorage.getItem("cartcout"); 
      this.props.navigation.setParams({cartcout:cartcout});
     //check Internet connectivity 
     NetInfo.getConnectionInfo().then((conectionInfo)=>
    {
      if(conectionInfo.type!="none")
      {
          this.getBannerImage();
          this.getBrandsImage();
          this.getGiftData();
          this.getDiscoverImage();
      }
      else{
        Toast.show(Constant.StringText.CHECKINTERNETCONNECTIVITY,Toast.SHORT);  
      }
    })
   }
  } 
  async componentWillMount() {
    SplashScreen.hide();
    this.props.navigation.addListener('willFocus',this.handleKey);
   
    this.setState({ isLoading:true});
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress.bind(this));
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress.bind(this));
  }

  //render view
  render() { 
    return (
      this.state.isLoading?
        <View style={Constant.styles.indicatoreViewsTyle}>
              <ActivityIndicator
                size="large" color="#006d50"
                animating={this.state.isLoading} />
        </View>
      :
      <View>
      <ScrollView  ref={(scroller) => {this.scroller = scroller}}>
        <View  style={Constant.styles.maincontainer} >
          <View style={Constant.styles.searchhomeSection}>
          <TouchableOpacity style={Constant.styles.searchpadding}>
            <Icon style={Constant.styles.p5} name="search" size={20} color="#000"/>
            <TextInput
                        style={Constant.styles.input}
                        autoFocus={false} 
                        setFocus={false}
                        autoCorrect={true} 
                        autoCapitalize="none" 
                        placeholder={Constant.StringText.WhatAreLookingFor}
                        underlineColorAndroid='#fff'
                        returnKeyType={"search"} // adding returnKeyType will do the work
                        onChangeText={(text) =>  this.setState({text : text})}
                        onSubmitEditing={() => this.submitTextInput()}
                        value={this.state.text}/>
          </TouchableOpacity>
          </View>
          <View style={Constant.styles.slideImageView}>
              {this.state.carouselElements?
                <Swiper
                  style={Constant.styles.slideImage}
                  onAnimateNextPage={(p) => console.log(p)}
                  showsButtons={true} activeDotColor={'transparent'} dotColor={'transparent'} 
                  prevButton={<ArrowIcon name="ios-arrow-back"size={28} color={Constant.color.black}/>}
                  nextButton={<ArrowIcon name="ios-arrow-forward"size={28} color={Constant.color.black}/>}
                  >
                {
                   this.state.carouselElements.map((item,i)=>{
                    return(
                    <TouchableOpacity style={Constant.styles.slideImage} key={i} onPress={()=>this.navigateAnother('Featured',
                    {category_id:item.category_id+"",brand:item.brand,input_text:""})}>
                        <Image style={Constant.styles.slideImage} resizeMode={'stretch'} source={{uri:item.image}}/>
                    </TouchableOpacity>
                    );
                  })
                }
              </Swiper>:null}
          </View>
          <View style={Constant.styles.gridView}>
                <Grid
                  style={Constant.styles.gridViewStyle}
                  data={this.state.giftitem}
                  renderItem={this._renderGiftitem}
                  itemsPerRow={3}
                />
          </View>
          <TouchableOpacity style={Constant.styles.lightGreenPanel} onPress={()=> this.navigateAnother('Featured',{category_id:"4451"+"",brand:"0",input_text:""})}>
              <Text style={Constant.styles.pannelText}>{Constant.StringText.GIFTANDNVS}</Text>
          </TouchableOpacity>
           <Text style={Constant.styles.blacknormalcenterText}>{Constant.StringText.FEATUREBRADNS}</Text> 
          <View style={Constant.styles.gridView}>
                <Grid
                  style={Constant.styles.gridViewStyle}
                  data={this.state.brands}
                  renderItem={this._renderItem}
                  itemsPerRow={3}
                />
          </View>
          <Text style={Constant.styles.blacknormalcenterText}>{Constant.StringText.FEATUREPRODUCT}</Text>
          <View  style={Constant.styles.gridView}>
              <Grid
                style={Constant.styles.gridViewStyle}
                data={this.state.discovermore}
                renderItem={this._renderDiscoverItem}
                itemsPerRow={2}
              />
          </View>
          <View style={Constant.styles.lightGreenPanel20}>
              <Text style={Constant.styles.whitebuttontext}>{Constant.StringText.FREESAMPLEWITHORDER}</Text>
          </View>
         <View style={Constant.styles.homeBottomView}>
            <TouchableOpacity style={Constant.styles.socialView} onPress={()=>{Constant.openUrl(Constant.StringText.FACEBOOKLINK)}}>
              <Image style={Constant.styles.socialImage} source={Constant.facebookIcon}/>
              <Text style={Constant.styles.listSmallText}>{Constant.StringText.FACEBOOK}</Text>
            </TouchableOpacity>
            <Image style={Constant.styles.h74} source={Constant.verticaldotImage}/>
            <TouchableOpacity style={Constant.styles.socialView} onPress={()=>{Constant.openUrl(Constant.StringText.TWITTERLINK)}}>
              <Image style={Constant.styles.socialImage} source={Constant.twitterIcon} />
              <Text style={Constant.styles.listSmallText}>{Constant.StringText.TWITTER}</Text>
            </TouchableOpacity>
            <Image style={Constant.styles.h74} source={Constant.verticaldotImage}/>
            <TouchableOpacity style={Constant.styles.socialView} onPress={()=>{Constant.openUrl(Constant.StringText.GOOGLELINK)}}>
              <Image style={Constant.styles.socialImage} source={Constant.google_plus} />
              <Text style={Constant.styles.listSmallText}>{Constant.StringText.GOOGEL}</Text>
            </TouchableOpacity>
         </View>
        </View>
      </ScrollView>
        <TouchableOpacity style={Constant.styles.stickybutton} onPress={this.scrollToTop}>
              <TopArrow name="chevron-circle-up" size={40} color={Constant.color.darkGreen}/>
        </TouchableOpacity>
      </View>
    );
  }
  //top of screen
  scrollToTop = () => {
    this.scroller.scrollTo({x: 0, y: 0});
  };

   //open next screen
   FunctionToOpenSecondActivity = (SCREEN) =>
   {
     this.props.navigation.navigate(SCREEN);
   }
   //hardware back button to exit application
   onBackPress=()=> {
     if (this.props.navigation.state.routeName == 'Home') {
       Alert.alert(
         'Exit App',
         'Exiting the application?', [{
             text: 'Cancel',
             onPress: () => console.log('Cancel Pressed'),
             style: 'cancel'
         }, {
             text: 'OK',
             onPress: () => BackHandler.exitApp()
         }, ], {
             cancelable: false
         }
      )
      return true;
     }
   } 
 
   //get banner slide banner image
  getBannerImage() {
    Service.getdataFromService(Constant.home_banner_image).then((responseJson)=>
    {
      this.setState({carouselElements:responseJson,isLoading: false});
    })
  }
   //get gift & slider  image
  getGiftData() {
    Service.getdataFromService(Constant.home_blocks_gift).then((responseJson)=>
    {
      this.setState({giftitem:responseJson});
    })
  }
    //get popular item image
  getBrandsImage() {
    Service.getdataFromService(Constant.popular_item).then((responseJson)=>
    {
      this.setState({brands:responseJson});
    })
  }
    //get popular item image
  getDiscoverImage() {
    Service.getdataFromService(Constant.feachured_product)
    .then((responseJson)=>{ 
      this.setState({discovermore:responseJson});
     })
  } 
  //render popular item
  _renderItem= (data, i)=>
  (
    <TouchableOpacity style={Constant.styles.popularitem} key={i} onPress={()=>this.navigateAnother('Featured',
    {category_id:data.category_id,brand:"1",input_text:""})}>
    <Image style={Constant.styles.popularitemImage} resizeMode={'contain'}  source={{uri:data.Brand_logo}} />
    </TouchableOpacity>
  );
 
  //render gift item
  _renderGiftitem=(data,i)=>
  (
    <TouchableOpacity style={Constant.styles.giftItem} key={i} onPress={()=>this.navigateAnother('Featured',
    {category_id:3619,brand:"1",input_text:"",brand_id:data.category_id,isNaN:data.category_id})}>
    <Image style={Constant.styles.giftitemImage} source={{uri:data.image}} />
    </TouchableOpacity>
  );
   
  //render discover item
  _renderDiscoverItem = (data, i) => (
    <TouchableOpacity  style={Constant.styles.discoveritem}  key={i} onPress={()=>{this.props.navigation.navigate('ProductDetail',{product_id:data.product_id,quantity:""})}}>
          <View style={Constant.styles.productImageView}> 
            <Image style={Constant.styles.productImageStyle} resizeMode={'contain'} source={{uri:data.product_image}} />
          </View> 
            <View style={Constant.styles.productItemStyle}> 
                  <Text numberOfLines={2} style={Constant.styles.listSmallText}>{data.name.toUpperCase()}</Text>
                  {data.special_price==null?
                  <View style={Constant.styles.listRow}> 
                    <Text style={Constant.styles.listSmallText}>{Constant.StringText.PRICE} {Constant.Currency}{parseFloat(data.price).toFixed(2)}</Text>
                  </View>
                  :
                  <View style={Constant.styles.listRow}> 
                    <Text style={Constant.styles.listSmallRedText}>{Constant.StringText.NOW}{parseFloat(data.special_price).toFixed(2)}</Text>
                    <Text style={Constant.styles.crossText}>{Constant.StringText.WAS}{parseFloat(data.price).toFixed(2)}</Text>
                  </View>
                }
            </View>
    </TouchableOpacity>
  );

    //navigate another page with param
    navigateAnother = async(SCREEN,data) => {
      if(data.isNaN=="mbt"||data.isNaN=='undefined')
      {
        this.props.navigation.navigate('MBT');
      }else{
        if(data!==undefined && typeof data!="undefined"){
          await AsyncStorage.setItem('category_id', data.category_id+"");
          await AsyncStorage.setItem('is_brand',data.brand+"");
          await AsyncStorage.setItem("brand_id",data.brand_id+"")
          await AsyncStorage.setItem('input_text',"");
          this.props.navigation.navigate(SCREEN,data);
        }
      }
    } 
  
   //submit search text
   submitTextInput=async()=>{
    await AsyncStorage.setItem('input_text',this.state.text);
    if(this.state.text=="")
    {
     Toast.show(Constant.StringText.WhatAreLookingFor,Toast.SHORT);
    }else{
     this.props.navigation.navigate('Featured',{input_text:this.state.text}); 
    }
  } 

}
