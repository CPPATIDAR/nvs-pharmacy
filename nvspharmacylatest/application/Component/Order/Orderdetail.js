import React, { Component } from 'react';
import {ScrollView,RefreshControl,Text,View,BackHandler,AsyncStorage,FlatList,
  ActivityIndicator,TouchableOpacity,Image,TouchableHighlight} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Constant from '../../../Constant/Constant';
import CrossIcon from 'react-native-vector-icons/Entypo';
import Toast from 'react-native-simple-toast';
import Service from '../../config/Service';

export default class Orderdetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      order_id:"",
      customer_id:"",
      order_date:"",
      order_total:"",
      status:"",
      full_name:"",
      email:"",
      product_list:[],
      shipping_address:"",
      billing_address:"",
      shipping_description:"",
      payment_method:"",
      isLoading: false,
      error: null,
      refreshing: false,
      s_firstname:"",
      s_lastname:"",
      s_street:"",
      s_city:"",
      s_region:"",
      s_postcode:"",
      s_country:"",
      s_telephone:"",
      b_firstname:"",
      b_lastname:"",
      b_street:"",
      b_city:"",
      b_region:"",
      b_postcode:"",
      b_country:"",
      b_telephone:"",
      base_total:"",
      shipping_amount:""
    };
  }

   //handle click event
 handleKey = async(e)=> {
  if(e.state.routeName=="Orderdetail")
  {
    var customer_id = await AsyncStorage.getItem("customer_id");
    var order_id = await AsyncStorage.getItem("order_id");
    this.setState({customer_id:customer_id,order_id:order_id});
    //get order detail
   this.getOrderdetail(order_id);
  }
 }
 
  /**
     * Call _fetchData after component has been mounted
  */
  async componentWillMount() {
    this.props.navigation.addListener('willFocus',this.handleKey);
    this.setState({ isLoading: true });
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  //get order detail items
  getOrderdetail(order_id){
    //webservice to get item list
    Service.getOrderdetail(Constant.getorderdetail,order_id).then((responseJson)=>{
      if(responseJson.status==1)
      {
        this.setState({
          order_id:responseJson.data.order_information.order_id,
          order_date:responseJson.data.order_information.order_date,
          order_total:responseJson.data.order_information.order_total,
          status:responseJson.data.order_information.status,
          full_name:responseJson.data.order_information.full_name,
          email:responseJson.data.order_information.email,
          product_list:responseJson.data.items_ordered,
          shipping_description:responseJson.data.order_information.shipping_description,
          payment_method:responseJson.data.order_information.payment_method,
          s_firstname:responseJson.data.shipping_address.firstname,
          s_lastname:responseJson.data.shipping_address.lastname,
          s_street:responseJson.data.shipping_address.street,
          s_city:responseJson.data.shipping_address.city,
          s_region:responseJson.data.shipping_address.region,
          s_postcode:responseJson.data.shipping_address.postcode,
          s_country:responseJson.data.shipping_address.country,
          s_telephone:responseJson.data.shipping_address.telephone,
          b_firstname:responseJson.data.billing_address.firstname,
          b_lastname:responseJson.data.billing_address.lastname,
          b_street:responseJson.data.billing_address.street,
          b_city:responseJson.data.billing_address.city,
          b_region:responseJson.data.billing_address.region,
          b_postcode:responseJson.data.billing_address.postcode,
          b_country:responseJson.data.billing_address.country,
          b_telephone:responseJson.data.billing_address.telephone,
          shipping_amount:responseJson.data.shipping_amount,
          base_total:responseJson.data.base_total,
          isLoading:false});
      }
      else{
        this.setState({isLoading:false});
        Toast.show(Constant.StringText.DATANOTAVAILABLE,Toast.SHORT);
      }
    });
  }
  //handle back hardware button
  handleBackButton = () => {
    this.props.navigation.navigate('Account');
    return true;
  } 
  render(){
    return( 
      this.state.isLoading?
      <View style={Constant.styles.indicatoreViewsTyle}> 
          <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
      </View>
      :
      <View style={Constant.styles.container}>
        <ScrollView>
           <View style={Constant.styles.containerwithp10}>
              <View style= {{flex:1, flexDirection:'column'}}>
                  <Text style={Constant.styles.blackboltext}>{Constant.StringText.ORDERDETAIL}</Text>
                  <View style={Constant.styles.container}>
                      <View style={{flexDirection:'row'}}>
                        <View style={{flexDirection:'row',flex:1,alignItems:'flex-start'}}><Text style={Constant.styles.blackboldlefttext}>{Constant.StringText.ORDERID} {this.state.order_id}</Text></View>
                        <View style={{flexDirection:'row',flex:1}}><Text style={Constant.styles.greennormelrightText}>{this.state.status}</Text></View>
                      </View>
                      <Text style={Constant.styles.listSmallText}>{Constant.StringText.DATE} {this.state.order_date}</Text>
                      <Text style={Constant.styles.listSmallText}>{Constant.StringText.SHIPTO} {this.state.shipping_address}</Text>
                      <Text style={Constant.styles.listSmallText}>{Constant.StringText.ORDERTOTAL} {this.state.order_total}</Text>
                  </View>
                  <View style={Constant.styles.m10Top}/>
                  <View style={{flexDirection:'row'}}>
                        <View style={{flexDirection:'column',flex:1,alignItems:'flex-start'}}>
                        <Text style={Constant.styles.blackboldlefttext}>{Constant.StringText.DEFAULTSHIPPINGADD}</Text>
                        <Text style={Constant.styles.listSmallText}>{this.state.s_street}</Text>
                        <Text style={Constant.styles.listSmallText}>{this.state.s_city},{this.state.s_region},{this.state.s_postcode}</Text>
                        <Text style={Constant.styles.listSmallText}>{this.state.s_country}</Text>
                        <Text style={Constant.styles.listSmallText}>T: {this.state.s_telephone}</Text>
                        </View>
                        <View style={{flexDirection:'column',flex:1,marginLeft:5}}>
                        <Text style={Constant.styles.blackboldlefttext}>{Constant.StringText.DEFAULTBIILINGADD}</Text>
                        <Text style={Constant.styles.listSmallText}>{this.state.b_street}</Text>
                        <Text style={Constant.styles.listSmallText}>{this.state.b_city},{this.state.b_region},{this.state.b_postcode}</Text>
                        <Text style={Constant.styles.listSmallText}>{this.state.b_country}</Text>
                        <Text style={Constant.styles.listSmallText}>T: {this.state.b_telephone}</Text>
                        </View>
                  </View>
                  <View style={Constant.styles.m10Top}/>
                  <View style={Constant.styles.revieworderView}>
                       <Text style={Constant.styles.blackboltext}>{Constant.StringText.YOURORDER}</Text>
                      <View style= {{flexDirection: 'row',padding: 5,backgroundColor: Constant.color.lightGreen}}>
                      <Text style={Constant.styles.whitenormalleftext}>{Constant.StringText.PRODUCTNAME}</Text>
                      <Text style={Constant.styles.whitenormalcentertext}>{Constant.StringText.QTY}</Text>
                      <Text style={Constant.styles.whitnormalrighttext}>{Constant.StringText.SUBTOTAL}</Text>
                      </View>
                      {/* product listing */}
                      {this.state.product_list.map((item,key)=>{
                          return(
                            <View key={key}>
                            <View  style={{flexDirection: 'row',padding: 5}}>
                              <Text style={Constant.styles.blacknormalleftText}>{item.product_name}</Text>
                              <Text style={Constant.styles.blacknormalcenterText}>{item.qty}</Text>
                              {item.special_price==""||item.special_price==null
                              ?
                              <Text style={Constant.styles.blacknormalrightText}>{Constant.Currency}{parseFloat(item.price).toFixed(2)}</Text>
                              :
                              <Text style={Constant.styles.blacknormalrightText}>{Constant.Currency}{parseFloat(item.special_price).toFixed(2)}</Text>}
                            </View>
                            <View style={Constant.styles.viewStyle}></View>
                            </View>
                          );
                        })
                      }
                      <View style= {{flexDirection: 'row',padding: 5}}>
                      <Text style={Constant.styles.blacknormalleftText}>{Constant.StringText.SUBTOTAL}</Text>
                      <Text style={Constant.styles.blacknormalrightText}>{Constant.Currency}{parseFloat(this.state.base_total).toFixed(2)}</Text>
                      </View>
                      <View style={Constant.styles.viewStyle}></View>
                      <View style= {{flexDirection: 'row',padding: 5}}>
                      <Text style={Constant.styles.blacknormalleftText}>{this.state.shipping_description}</Text>
                      <Text style={Constant.styles.blacknormalrightText}>{Constant.Currency}{parseFloat(this.state.shipping_amount).toFixed(2)}</Text>
                      </View>
                      <View style={Constant.styles.viewStyle}></View>
                      <View style= {{flexDirection: 'row',padding: 5}}>
                      <Text style={Constant.styles.blackboldlefttext}>{Constant.StringText.GRANDTOTAL}</Text>
                      <Text style={Constant.styles.blackboldrighttext}>{Constant.Currency}{parseFloat(this.state.order_total).toFixed(2)}</Text>
                      </View>  
                  </View>
                </View>
              </View>
          </ScrollView>
        </View>
    );
  }
}