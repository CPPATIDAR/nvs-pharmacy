import React, { Component } from 'react';
import {RefreshControl,Text,View,BackHandler,AsyncStorage,FlatList,ActivityIndicator,
TouchableOpacity,Image} from 'react-native';
import { StackNavigator} from 'react-navigation'
import Constant from '../../../Constant/Constant';
import CrossIcon from 'react-native-vector-icons/Entypo';
import Toast from 'react-native-simple-toast';
import Service from '../../config/Service';

export default class OrderListing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      customer_id:"",
      dataSource:[],
      isLoading: false,
      error: null,
      refreshing: false,
      responseJson:"",
    };
  }

 //handle click event
 handleKey = async(e) => {
  if(e.state.routeName=="OrderListing")
  {
    var customer_id = await AsyncStorage.getItem("customer_id");
    this.setState({customer_id:customer_id});
    //webservice calling
    this.getOrderListing();
  }
 }
  /**
     * Call _fetchData after component has been mounted
  */
  async componentWillMount() {
    this.props.navigation.addListener('willFocus',this.handleKey);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
//handle back hardware button
handleBackButton = () => {
  this.props.navigation.navigate('Home');
  return true;
} 
  //get all orderlisting items
  getOrderListing(){ 
    this.setState({isLoading:true});
    //get order listing from customer_id
    Service.getOrderListFromCustomerID(Constant.getOrderListing,this.state.customer_id).then((responseJson)=>
    {
      if(responseJson.status==1)
          {
            if(responseJson.data.order_information.length!=0)
            {
              this.setState({dataSource:responseJson.data.order_information,isLoading:false,refreshing:false});
            }
            else{
              this.setState({isLoading:false});
              Toast.show(Constant.StringText.DATANOTAVAILABLE,Toast.SHORT);
            }
          }
          else{
            this.setState({isLoading:false});
            Toast.show(Constant.StringText.DATANOTAVAILABLE,Toast.SHORT);
          }
      });
  }

  

  //navigate another page with param
  navigateAnother = async(SCREEN,data) => {
      if(data!==undefined && typeof data!="undefined"){
        await AsyncStorage.setItem('order_id', data+"");
        this.props.navigation.navigate(SCREEN,data);
    }
  } 
  //render item for listview
  renderItem(data) {
  let { item, index } = data;  
  return (
    <TouchableOpacity onPress={()=>this.navigateAnother('Orderdetail',{data:item.order_id})}>
    <View style={Constant.styles.container}>
      
        <View style={{flexDirection:'row'}}>
          <View style={{flexDirection:'row',flex:1,alignItems:'flex-start'}}><Text style={Constant.styles.blackboldlefttext}>{Constant.StringText.ORDERID} {item.order_id}</Text></View>
          <View style={{flexDirection:'row',flex:1}}><Text style={Constant.styles.greennormelrightText}>{item.status}</Text></View>
        </View>
        <Text style={Constant.styles.listSmallText}>{Constant.StringText.DATE} {item.date}</Text>
        <Text style={Constant.styles.listSmallText}>{Constant.StringText.SHIPTO} {item.ship_to}</Text>
        <Text style={Constant.styles.listSmallText}>{Constant.StringText.ORDERTOTAL} {item.order_total}</Text>
       
    </View>
    </TouchableOpacity>
    ) 
  }

  //refershing flatlistview
  _onRefresh = () => {
    this.setState({refreshing: true});
    this.getOrderListing();
  }

  //list seperator
  renderSeparator = () => {
    return (<View style={[Constant.styles.separator]}/>);
  };
  /**
   * Renders the list
   */
  render(){
    return( 
      this.state.isLoading?
      <View style={Constant.styles.indicatoreViewsTyle}> 
          <ActivityIndicator size="large" color="#006d50"></ActivityIndicator>
      </View>
      :
      <View style={Constant.styles.cartView}>
          {this.state.dataSource.length!=0?
            <View style={{flex:1}}>
            <FlatList
              keyExtractor = {( item, index ) => index }
              data={this.state.dataSource}
              renderItem={this.renderItem.bind(this)}
              ItemSeparatorComponent={this.renderSeparator}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }
            />
          </View>
          :null}
      </View>
    );
  }
}