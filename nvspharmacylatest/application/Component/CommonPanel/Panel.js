import React,{Component} from 'react'
import {Text,View,Image,TouchableHighlight,Animated} from 'react-native';
import Constant from '../../../Constant/Constant'

class Panel extends Component{
    constructor(props){
        super(props);

        this.icons = {
            'up'    : require('../../../resource/img/down-arrow.png'),
            'down'  : require('../../../resource/img/next.png')
        };

        this.state = {
            title       : props.title,
            expanded    : false,
            animation   : new Animated.Value()
        };
    }

    toggle(){
        let initialValue    = this.state.expanded? this.state.maxHeight + this.state.minHeight : this.state.minHeight,
            finalValue      = this.state.expanded? this.state.minHeight : this.state.maxHeight + this.state.minHeight;

        this.setState({
            expanded : !this.state.expanded
        });

        this.state.animation.setValue(initialValue);
        Animated.spring(
            this.state.animation,
            {
                toValue: finalValue
            }
        ).start();
    }

    _setMaxHeight(event) {
       if (!this.state.maxHeight) {
         this.setState({
           maxHeight: event.nativeEvent.layout.height,
         });
       }
     }

     _setMinHeight(event) {
       if (!this.state.minHeight) {
         this.setState({
           minHeight: event.nativeEvent.layout.height,
           animation: new Animated.Value(event.nativeEvent.layout.height),
         });
       }
     }

    render(){
        let icon = this.icons['down'];

        if(this.state.expanded){
            icon = this.icons['up'];
        }

        return (
            <Animated.View
                style={[Constant.styles.pannelcontainer,{height: this.state.animation}]}>
                <View style={Constant.styles.panelView} onLayout={this._setMinHeight.bind(this)}>
                    <Text style={Constant.styles.listitemNameParent}>{this.state.title}</Text>
                    <TouchableHighlight onPress={this.toggle.bind(this)} underlayColor="#f1f1f1" style={{alignItems:'center'}}>
                        <Image style={{margin:10}}  source={icon}></Image>
                    </TouchableHighlight>
                </View>
                <View style={{paddingBottom:10,paddingLeft:10,paddingRight:10}} onLayout={this._setMaxHeight.bind(this)}>
                    {this.props.children}
                </View>

            </Animated.View>
        );
    }
}
export default Panel;
