import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {NavigationActions,DrawerActions} from 'react-navigation';
import {ScrollView, Text, View,TouchableOpacity,Image,TextInput,AsyncStorage} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'
import IconSearch from 'react-native-vector-icons/FontAwesome';
import Constant from '../../Constant/Constant';
import Toast from 'react-native-simple-toast';

class SideMenu extends Component {
  constructor(props){
    super(props);
    this.state = {
      customer_id:"",
      text:""
    }
  }

  //handle tab event
  handleKey = async(e) => {
    if(e.state.routeName=="DrawerClose")
    {
      var customer_id = await AsyncStorage.getItem("customer_id");
      this.setState({customer_id:customer_id});
    }
  } 
  async componentWillMount()
  {
    this.props.navigation.addListener('willFocus',this.handleKey);
  }

  //navigate to another screen
  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }
//clear session onLogout
  navigateSessionClear = (route) => async() => {
    this.setState({customer_id:""});
    await AsyncStorage.setItem("customer_id","");
    await AsyncStorage.setItem("customer_firstname","");
    await AsyncStorage.setItem("customer_lastname","");
    await AsyncStorage.setItem("customer_email","");
    await AsyncStorage.setItem("card_id","");
    await AsyncStorage.setItem("array","");
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }

   //submit search text
   submitTextInput=async()=>{
    await AsyncStorage.setItem('input_text',this.state.text);
     if(this.state.text=="")
     {
      Toast.show(Constant.StringText.WhatAreLookingFor,Toast.SHORT);
     }else{
      this.props.navigation.navigate('Featured',{input_text:this.state.text}); 
     }
    }

  render() {
    return (
      <View style={Constant.styles.p10Top}>
      <View style={Constant.styles.slideMenuView}>
        <TouchableOpacity  onPress={this.navigateToScreen('DrawerClose')} style={backgroundColor=Constant.color.white}>
            <Icon  name="ios-arrow-back" size={28} color="#006d50"/>
        </TouchableOpacity>
          <View style={Constant.styles.logoViewCenter}>
            <Image resizeMode={'contain'} style={Constant.styles.logoImage} source={Constant.logoImage}/>
          </View>
      </View>
         {/*  <View style={Constant.styles.searchViewGreenBack}>
            <View style={Constant.styles.searchSection}>
              <IconSearch style={Constant.styles.p5} name="search" size={20} color="#000"/>
              <TextInput
                        style={Constant.styles.input}
                        autoCorrect={true} 
                        autoCapitalize="none" 
                        placeholder={Constant.StringText.WhatAreLookingFor}
                        underlineColorAndroid='#fff'
                        returnKeyType={"search"} // adding returnKeyType will do the work
                        onChangeText={(text) =>  this.setState({text : text})}
                        onSubmitEditing={() => this.submitTextInput()}
                    />
            </View>
          </View> */}
          <View style={Constant.styles.viewStyle}></View>
        <ScrollView>
          <View style={{backgroundColor:'#ffffff'}}>
            <TouchableOpacity  onPress={this.navigateToScreen('Home')} style={Constant.styles.slideMenuItemView}>
               <Text style={Constant.styles.drawertext}>{Constant.StringText.Home}</Text>
            </TouchableOpacity >
            <TouchableOpacity  onPress={this.navigateToScreen('BrandListing')} style={Constant.styles.slideMenuItemView}>
               <Text style={Constant.styles.drawertext}>{Constant.StringText.SHOPBYBRAND}</Text><Icon name="ios-arrow-forward" size={28} color="#000000"/>
            </TouchableOpacity >
            <TouchableOpacity onPress={this.navigateToScreen('CategoryList')} style={Constant.styles.slideMenuItemView}>
               <Text style={Constant.styles.drawertext}>{Constant.StringText.SHOPBYPEODUCTS}</Text><Icon name="ios-arrow-forward" size={28} color="#000000"/>
            </TouchableOpacity >
            <TouchableOpacity  onPress={this.navigateToScreen('SpecialOfferListing')} style={Constant.styles.slideMenuItemView}>
               <Text style={Constant.styles.drawertext}>{Constant.StringText.SPECIALOFFER}</Text>
            </TouchableOpacity >
            <View style={Constant.styles.viewStyle}></View>
            <TouchableOpacity  onPress={this.navigateToScreen('Information')} style={Constant.styles.slideMenuItemView}>
               <Text style={Constant.styles.drawertext}>{Constant.StringText.INFORMATION}</Text>
            </TouchableOpacity>
            <TouchableOpacity  onPress={this.navigateToScreen('CustomerService')} style={Constant.styles.slideMenuItemView}>
               <Text style={Constant.styles.drawertext}>{Constant.StringText.CUSTOMERSERVICE}</Text>
            </TouchableOpacity >
            <TouchableOpacity  onPress={this.navigateToScreen('Newsletter')} style={Constant.styles.slideMenuItemView}>
               <Text style={Constant.styles.drawertext}>{Constant.StringText.NEWSLETTER}</Text>
            </TouchableOpacity >
            <TouchableOpacity  onPress={this.navigateToScreen('Blog')} style={Constant.styles.slideMenuItemView}>
               <Text style={Constant.styles.drawertext}>{Constant.StringText.BLOG}</Text>
            </TouchableOpacity >
            <TouchableOpacity  onPress={this.navigateToScreen('MBT')} style={Constant.styles.slideMenuItemView}>
               <Text style={Constant.styles.drawertext}>{Constant.StringText.MBT}</Text>
            </TouchableOpacity >
            <View style={Constant.styles.viewStyle}></View>
            <TouchableOpacity  onPress={this.navigateToScreen('Account')} style={Constant.styles.slideMenuItemView}>
               <Text style={Constant.styles.drawertext}>{Constant.StringText.ACCOUNT}</Text>
            </TouchableOpacity >
            {/* <TouchableOpacity  onPress={this.navigateToScreen('Setting')} style={Constant.styles.slideMenuItemView}>
               <Text style={Constant.styles.drawertext}>{Constant.StringText.SETTING}</Text>
            </TouchableOpacity>  */}
            <TouchableOpacity  onPress={this.navigateToScreen('Feedback')} style={Constant.styles.slideMenuItemView}>
               <Text style={Constant.styles.drawertext}>{Constant.StringText.FEEDBACK}</Text>
            </TouchableOpacity>           
     
            <View style={Constant.styles.viewStyle}></View>
            {this.state.customer_id!=""&&this.state.customer_id!=null ?
             <TouchableOpacity  onPress={this.navigateSessionClear('Home')} style={Constant.styles.slideMenuItemView}>
                <Text style={Constant.styles.drawertext}>{Constant.StringText.LOGOUT}</Text>
             </TouchableOpacity >
            :
              null
            }
           
          </View>
        </ScrollView>
        
      </View>
    );
  }
}
export default SideMenu;