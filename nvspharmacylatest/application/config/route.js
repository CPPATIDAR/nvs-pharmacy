import React from 'react'
import {StackNavigator,TabNavigator} from 'react-navigation'
import {Dimensions,Text,TouchableOpacity,AsyncStorage} from 'react-native'
import {BrandListing} from '../Component/Brands/BrandListing'
import {CategoryList} from '../Component/Product/CategoryList'
import SlideMenu from './SideMenu'
import {Setting} from '../Component/Setting/Setting'
import Constant from '../../Constant/Constant'
import { Newsletter } from '../Component/Newsletter/Newsletter';
import {MBT} from '../Component/MBT/MBT';
import { Feedback } from '../Component/Feedback/Feedback';
import { Home } from '../Component/Home/Home';
import { ProductList } from '../Component/Product/ProductList';
import productdetail from '../Component/CommonScreen/Productdetail';
import CheckOut1 from '../Component/CommonScreen/CheckOut1';
import CheckOut2 from '../Component/CommonScreen/CheckOut2';
import { Search } from '../Component/Search/Search';
import CartListing from '../Component/Cart/CartListing';
import WishList from '../Component/WishList/WishList';
import Account from '../Component/Account/Account';
import TabIcon from 'react-native-vector-icons/FontAwesome'
import IconBadge from 'react-native-icon-badge';
import Orderdetail from '../Component/Order/Orderdetail';
import AddAddress from '../Component/Account/AddAddress';
import ForgotPassword from '../Component/Account/ForgotPassword';
import EditAccountInform from '../Component/Account/EditAccountInform';
import { Blog } from '../Component/Blog/Blog';
import { BlogDetail } from '../Component/Blog/BlogDetail';
import { CustomerService } from '../Component/CustomerService/CustomerService';
import { FAQS } from '../Component/CustomerService/FAQS';
import { TrackingOrder } from '../Component/CustomerService/TrackingOrder';
import { TermNCondition } from '../Component/CustomerService/TermNCondition';
import { PrivacyPolicy } from '../Component/CustomerService/PrivacyPolicy';
import { ReturnPolicy } from '../Component/CustomerService/ReturnPolicy';
import { DataProtectionPolicy } from '../Component/CustomerService/DataProtectionPolicy';
import { ProblemOrComplain } from '../Component/CustomerService/ProblemOrComplain';
import { Information } from '../Component/Information/Information';
import { AboutUs } from '../Component/Information/AboutUs';
import { StoreLocation } from '../Component/Information/StoreLocation';
import { GPhCMHRA } from '../Component/Information/GPhCMHRA';
import { Howtoorder } from '../Component/Information/Howtoorder';
import { Shippingdelivery } from '../Component/Information/Shippingdelivery';
import { Testimonial } from '../Component/Testimonial/Testimonial';
import { AddTestimonial } from '../Component/Testimonial/AddTestimonial';
import { ContactUs } from '../Component/Information/ContactUs';
import OrderListing from '../Component/Order/OrderListing';
import { FilterScreen } from '../Component/Product/FilterScreen';
import { SpecialOfferListing } from '../Component/SpecialOffer/SpecialOfferListing';

//drawer navigation
export const Drawer = StackNavigator({
    MainHome: {
        screen: TabNavigator({
            Home:{
                screen: Home,
                navigationOptions: ({ navigation }) => ({
                    tabBarLabel:Constant.StringText.Home,
                    tabBarIcon: ({ tintColor  }) => <TabIcon name={"home"} size={28} color={tintColor}/>,
                    headerLeft:null,
                    headerRight : Constant.DrawerIcon(navigation),
                    headerTitle: Constant.HeaderLogo(false)
                }),
            },
            Search: {
                screen: Search,
                navigationOptions: ({ navigation }) => ({
                    tabBarLabel:Constant.StringText.Search,
                    tabBarIcon: ({ tintColor }) => <TabIcon name={"search"} size={26} color={tintColor}/>,
                    headerLeft : Constant.BackIcon(navigation),
                    headerRight : Constant.DrawerIcon(navigation),
                    headerTitle: Constant.centerLogo()
                }),
            },  
            CartListing: {
                screen: CartListing,
                navigationOptions:({ navigation }) =>({
                    headerLeft : Constant.BackIcon(navigation),
                    headerRight : Constant.DrawerIcon(navigation),
                    headerTitle: Constant.centerLogo(),
                    tabBarLabel:Constant.StringText.Cart,
                    tabBarIcon: ({ tintColor }) =>
                    <IconBadge
                    MainElement={
                        <TabIcon name={"shopping-bag"} size={24} color={tintColor}/>
                    }
                    BadgeElement={
                      <Text style={Constant.styles.darkgreenText}>{navigation.getParam('cartcout')}</Text>
                    }
                    IconBadgeStyle={Constant.styles.IconBadgeStyle}
                    Hidden={navigation.getParam('cartcout')==0||navigation.getParam('cartcout')==undefined||navigation.getParam('cartcout')==null}
                    />,
                }),
            },  
            WishList: {
                screen:WishList,
                navigationOptions: ({ navigation }) => ({
                    headerLeft : Constant.BackIcon(navigation),
                    headerRight : Constant.DrawerIcon(navigation),
                    headerTitle: Constant.centerLogo(),
                    tabBarLabel:Constant.StringText.Wishlist,
                    tabBarIcon: ({ tintColor }) => <IconBadge
                    MainElement={
                        <TabIcon name={"heart"} size={24} color={tintColor}/>
                    }
                    BadgeElement={
                      <Text style={Constant.styles.darkgreenText}>{navigation.getParam('wishlistcount')}</Text>
                    }
                    IconBadgeStyle={Constant.styles.IconBadgeStyle}
                    Hidden={navigation.getParam('wishlistcount')==0||navigation.getParam('wishlistcount')==undefined||navigation.getParam('wishlistcount')==null}
                    />,
                }),
            },
            Account: {
                screen: Account,
                navigationOptions: ({ navigation }) => ({
                    tabBarLabel:Constant.StringText.Account,
                    tabBarIcon: ({ tintColor }) => <TabIcon name={"user"} size={25} color={tintColor}/> , 
                    headerLeft : Constant.BackIcon(navigation),
                    headerRight : Constant.DrawerIcon(navigation),
                    headerTitle: Constant.centerLogo()       
                }),
            }   
        },{
            swipeEnabled: false,   
            tabBarOptions: {
                upperCaseLabel: false,
                indicatorStyle:[Constant.styles.indicatorStyle],
                style:[Constant.styles.tabBarstyle],
                labelStyle:[Constant.styles.labelStyle],
                activeTintColor: "#006d50",
                showIcon: true ,
                activeBackgroundColor:[Constant.color.white],
                inactiveBackgroundColor:[Constant.color.white],
                inactiveTintColor:"#000000"
            }, 
            initialRouteName:"Home",
            tabBarPosition:'bottom',
        }),
    },
    BrandListing: {
        screen: BrandListing,
        navigationOptions: ({ navigation }) => ({
            headerLeft : Constant.BackIcon(navigation),
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle: Constant.centerLogo()
        }),
    },
    EditAccountInform:{
        screen: EditAccountInform,
        navigationOptions: ({ navigation }) => ({
            drawerLockMode: 'locked-closed',
            headerLeft:null,
            headerTitle:Constant.SecureCheckoutHeader(navigation,"Edit Account"),
         }),
    },
    Orderdetail:{
        screen:Orderdetail,
        navigationOptions: ({ navigation }) => ({
            drawerLockMode: 'locked-closed',
            headerTitle:Constant.HeaderLogo(true),
            headerTintColor:'#006d50'
        }),
    },
    AddAddress:{
        screen:AddAddress,
        navigationOptions: ({ navigation }) => ({
            drawerLockMode: 'locked-closed',
            headerLeft:null,
            headerTitle:Constant.SecureCheckoutHeader(navigation,"Add Address"),
         }),
    },
    ForgotPassword:{
        screen: ForgotPassword,
        navigationOptions: ({ navigation }) => ({
           drawerLockMode: 'locked-closed',
           headerLeft:null,
           headerTitle:Constant.SecureCheckoutHeader(navigation,"Forgot Password"),
        }),
    },
    ProductDetail:{
        screen: productdetail,
        navigationOptions: ({ navigation }) => ({
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle:Constant.centerLogo(),
            headerTintColor:'#006d50',
            tabBarVisible:true
        }),
    },
    Checkout1:{
        screen: CheckOut1,
        navigationOptions: ({ navigation }) => ({
          drawerLockMode: 'locked-closed',
          headerLeft:null,
          headerTitle:Constant.SecureCheckoutHeader(navigation,Constant.StringText.SECURECHECKOUT),
        }),
    },
     CheckOut2:{
        screen: CheckOut2,
        navigationOptions: ({ navigation }) => ({
           drawerLockMode: 'locked-closed',
           headerLeft:null,
           headerTitle:Constant.SecureCheckoutHeader(navigation,Constant.StringText.SECURECHECKOUT),
        }),
    },
    CategoryList: {
        screen: CategoryList,
        navigationOptions: ({ navigation }) => ({
            headerLeft :Constant.BackIcon(navigation),
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle: Constant.centerLogo()
        }),
    }, Feedback: {
        screen: Feedback,
        navigationOptions: ({ navigation }) => ({
            headerLeft :Constant.BackIcon(navigation),
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle: Constant.centerLogo()
        }),
    }, MBT: {
        screen: MBT,
        navigationOptions: ({ navigation }) => ({
            headerLeft :Constant.BackIcon(navigation),
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle: Constant.centerLogo()
        }),
    }, Newsletter: {
        screen: Newsletter,
        navigationOptions: ({ navigation }) => ({
            headerLeft :Constant.BackIcon(navigation),
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle: Constant.centerLogo()
        }),
    },  Setting: {
        screen: Setting,
        navigationOptions: ({ navigation }) => ({
            headerLeft :Constant.BackIcon(navigation),
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle: Constant.centerLogo()
        }),
    }, Blog: {
        screen: Blog,
        navigationOptions: ({ navigation }) => ({
            headerLeft :Constant.BackIcon(navigation),
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle: Constant.centerLogo()
        }),
    },
    BlogDetail: {
        screen: BlogDetail,
        navigationOptions: ({ navigation }) => ({
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle:Constant.centerLogo(),
            headerTintColor:'#006d50'
        }),
    },
    CustomerService: {
        screen: CustomerService,
        navigationOptions: ({ navigation }) => ({
            headerLeft :Constant.BackIcon(navigation),
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle: Constant.centerLogo()
        }),
    },
    FAQS:{
        screen: FAQS,
        navigationOptions: ({ navigation }) => ({
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle:Constant.centerLogo(),
            headerTintColor:'#006d50'
        }),
    },
    TrackingOrder:{
        screen: TrackingOrder,
        navigationOptions: ({ navigation }) => ({
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle:Constant.centerLogo(),
            headerTintColor:'#006d50'
        }),
    },
    TermNCondition:{
        screen: TermNCondition,
        navigationOptions: ({ navigation }) => ({
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle:Constant.centerLogo(),
            headerTintColor:'#006d50'
        }),
    },
    PrivacyPolicy:{
        screen: PrivacyPolicy,
        navigationOptions: ({ navigation }) => ({
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle:Constant.centerLogo(),
            headerTintColor:'#006d50'
        }),
    },
    ReturnPolicy:{
        screen: ReturnPolicy,
        navigationOptions: ({ navigation }) => ({
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle:Constant.centerLogo(),
            headerTintColor:'#006d50'
        }),
    },
    DataProtectionPolicy:{
        screen: DataProtectionPolicy,
        navigationOptions: ({ navigation }) => ({
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle:Constant.centerLogo(),
            headerTintColor:'#006d50'
        }),
    },
    ProblemOrComplain:{
        screen: ProblemOrComplain,
        navigationOptions: ({ navigation }) => ({
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle:Constant.centerLogo(),
            headerTintColor:'#006d50'
        }),
    },Information: {
        screen: Information,
        navigationOptions: ({ navigation }) => ({
            headerLeft :Constant.BackIconDrawer(navigation),
            headerRight :Constant.DrawerIcon(navigation),
            headerTitle:Constant.centerLogo(),
        }),
    }, 
    AboutUs: {
        screen: AboutUs,
        navigationOptions: ({ navigation }) => ({
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle:Constant.centerLogo(),
            headerTintColor:'#006d50'
        }),
    },
    StoreLocation: {
        screen: StoreLocation,
        navigationOptions: ({ navigation }) => ({
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle:Constant.centerLogo(),
            headerTintColor:'#006d50'
        }),
    },
    GPhCMHRA: {
        screen: GPhCMHRA,
        navigationOptions: ({ navigation }) => ({
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle:Constant.centerLogo(),
            headerTintColor:'#006d50'
        }),
    },
    Howtoorder: {
        screen: Howtoorder,
        navigationOptions: ({ navigation }) => ({
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle:Constant.centerLogo(),
            headerTintColor:'#006d50'
        }),
    },
    Shippingdelivery: {
        screen: Shippingdelivery,
        navigationOptions: ({ navigation }) => ({
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle:Constant.centerLogo(),
            headerTintColor:'#006d50'
        }),
    },
    Testimonial: {
        screen: Testimonial,
        navigationOptions: ({ navigation }) => ({
            headerLeft :Constant.BackIconDrawer(navigation),
            headerRight :Constant.DrawerIcon(navigation),
            headerTitle:Constant.centerLogo(),
        }),
    },
    AddTestimonial:{
        screen: AddTestimonial,
        navigationOptions: ({ navigation }) => ({
            headerRight :Constant.DrawerIcon(navigation),
            headerTitle:Constant.centerLogo(),
            headerTintColor:'#006d50'
        }),
    },
    ContactUs:{
        screen:ContactUs,
        navigationOptions: ({ navigation }) => ({
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle:Constant.centerLogo(),
            headerTintColor:'#006d50'
        }),
    }, OrderListing: {
        screen: OrderListing,
        navigationOptions: ({ navigation }) => ({
            headerLeft :Constant.BackIconDrawer(navigation),
            headerRight :Constant.DrawerIcon(navigation),
            headerTitle:Constant.centerLogo(),
        }),
    },
    Orderdetail:{
        screen:Orderdetail,
        navigationOptions: ({ navigation }) => ({
            drawerLockMode: 'locked-closed',
            headerTitle:Constant.HeaderLogo(true),
            headerTintColor:'#006d50'
        }),
    },   
     ProductList:{
        screen: TabNavigator({
            Featured: {
              screen: ProductList,
              navigationOptions: ({ navigation }) => ({
                tabBarLabel: Constant.StringText.FEATURED,
              }),
            },
            Newest: { 
              screen: ProductList,
              navigationOptions: ({ navigation }) => ({
                tabBarLabel: Constant.StringText.NEWEST,
              }),
            },Rating: {
                screen: ProductList,
                navigationOptions: ({ navigation }) => ({
                    tabBarLabel: Constant.StringText.RATING,
                }),
              },Price: {
                screen: ProductList,
                navigationOptions: ({ navigation }) => ({
                    tabBarLabel: Constant.StringText.PRICE,
                }),
              },
          },{
            lazy:true,
            swipeEnabled: false,
            tabBarOptions: {
                upperCaseLabel: false,
                indicatorStyle:[Constant.styles.indicatorStyle],
                style:[Constant.styles.tabBarstyleWithoutIcon],
                labelStyle:[Constant.styles.labelStyleWithoutIcon],
                activeTintColor: '#98c954',
                activeBackgroundColor:[Constant.color.white],
                inactiveBackgroundColor:[Constant.color.white],
                inactiveTintColor:'#000000'
            },
            tabBarPosition: 'top',
        }), 
            navigationOptions: ({ navigation }) => ({
                tabBarVisible: true,
                drawerLockMode: 'locked-closed',
                headerLeft :Constant.BackIconDrawer(navigation),
                headerRight :  <TouchableOpacity style={{flexDirection:'row',marginRight:15}} onPress={() => navigation.navigate('Filter')}>
                <Text style={Constant.styles.greennormelrightText}>{Constant.StringText.FILTERS}</Text>
                </TouchableOpacity>
              /* <IconBadge
                    MainElement={
                        <TouchableOpacity style={{flexDirection:'row',marginRight:15}} onPress={() => navigation.navigate('Filter')}>
                        <Text style={Constant.styles.greennormelrightText}>{Constant.StringText.FILTERS}</Text>
                        </TouchableOpacity>
                    }
                     BadgeElement={
                    <Text style={Constant.styles.whitebuttontext}></Text>
                    }
                    IconBadgeStyle={Constant.styles.RedIconBadgeStyle}
                    Hidden={} */
                    ,
                headerTitle: Constant.centerLogo(),
        }),
    }, Filter:{
        screen: FilterScreen,
        navigationOptions: ({ navigation }) => ({
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle:Constant.centerLogo(),
            headerTintColor:'#006d50'
        }),
    }, 
     SpecialOfferListing: {
        screen: SpecialOfferListing,
        navigationOptions: ({ navigation }) => ({
            headerLeft :Constant.BackIcon(navigation),
            headerRight : Constant.DrawerIcon(navigation),
            headerTitle: Constant.centerLogo()
        }),
    },
},)
