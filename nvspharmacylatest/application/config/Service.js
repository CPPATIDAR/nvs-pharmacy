import React from 'react'
import Constant  from '../../Constant/Constant'

const defaultOptions = {
    url: null,
    store: 'default',
    userAgent: 'Interactiavated Magento Library',
    authentication: {
      login: {
        type: 'admin',
        username: undefined,
        password: undefined
      },
      integration: {
        consumer_key: undefined,
        consumer_secret: undefined,
        access_token: undefined,
        access_token_secret: undefined
      }
    }
  };


class Service{
  setOptions(options) {
		this.configuration = { ...defaultOptions, ...options };
		this.base_url = this.configuration.url;
		this.root_path = `rest/${this.configuration.store}`;
	}

  init() {
    return new Promise((resolve, reject) => {
      if (this.configuration.authentication.integration.access_token) {
        this.access_token = this.configuration.authentication.integration.access_token;
        resolve(this);
      } else if (this.configuration.authentication.login) {
        const { username, password, type } = this.configuration.authentication.login;
        if (username) {
          let path;
          if (type === 'admin') {
            path = 'index.php/nvs_admin/oauth_authorize';
          } else {
            path = '/V1/integration/customer/token';
          }

          this.post(path, { username, password })
            .then(token => {
              // debugger;
              console.log('token');
              this.access_token = token;
              resolve(this);
            })
            .catch(e => {
              console.log(e);
              reject(e);
            });
        }
      }
    });
  }
    //Post method to post data
    post(path, params) {
        return this.send(path, 'POST', null, params);
    }
    
    //Get method to get data
    get(path, params, data) {
        return this.send(path, 'GET', params, data);
    }

    //Send method to take data from GET and POST , sent to server
    send(url, method, params, data) {
        //header
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        };

      return new Promise((resolve, reject) => {
        console.log({ url, method, headers, data, ...params });
          fetch(url, { method, headers, body: JSON.stringify(data) })
            .then(response => {
                console.log(response);
                return response.json();
              })
            .then(responseData => {
                // TODO: check response code
                 // debugger;
                console.log("responseData===="+JSON.stringify(responseData));
                resolve(responseData);
              })
            .catch(error => {
              console.log(error);
              reject(error);
            });
      });
    }
 
    //get data without any params
    getdataFromService(URL) {
        return new Promise((resolve, reject) => {
          this.get(URL)
            .then(data => {
              resolve(data);
            })
            .catch(e => {
              console.log(e);
              reject(e);
            });
        });
    }

    //get data with page name
    getPagesdata(URL,pagename) {
      return new Promise((resolve, reject) => {
        this.post(URL,{pagename:pagename})
          .then(data => {
            resolve(data);
          })
          .catch(e => {
            console.log(e);
            reject(e);
          });
      });
  }

   //get data with page name
   getBlogdetail(URL,id) {
    return new Promise((resolve, reject) => {
      this.post(URL,{id:id})
        .then(data => {
          resolve(data);
        })
        .catch(e => {
          console.log(e);
          reject(e);
        });
    });
}

  //get data with id
  getBlogDetaildata(URL,id) {
    return new Promise((resolve, reject) => {
      this.post(URL,{id:id})
        .then(data => {
          resolve(data);
        })
        .catch(e => {
          console.log(e);
          reject(e);
        });
    });
}

    //getdetail , delet account , get order list from customer_id
    getUserDetail(URL,customer_id)
    { 
      return new Promise((resolve, reject) => 
      {
      this.post(URL,{customer_id:customer_id})
      .then(data => {
        resolve(data);
      })
      .catch(e => {
        console.log(e);
        reject(e);
      });
      });
    }

    //dlete address from customer_id and address_id
    deletAddress(URL,customer_id,address_id)
    { 
      return new Promise((resolve, reject) => 
      {
      this.post(URL,{customer_id:customer_id,address_id:address_id})
      .then(data => {
        resolve(data);
      })
      .catch(e => {
        console.log(e);
        reject(e);
      });
      });
    }

    //delete product from prduct_id and cutomer_id and also add in wishlist
    deletAddProduct(URL,customer_id,product_id)
    { 
      return new Promise((resolve, reject) => 
      {
      this.post(URL,{customer_id:customer_id,product_id:product_id})
      .then(data => {
        resolve(data);
      })
      .catch(e => {
        console.log(e);
        reject(e);
      });
      });
    }

    deletCardProduct(URL,card_id,product_id)
    {
      return new Promise((resolve, reject) => 
      {
      this.post(URL,{card_id:card_id,product_id:product_id})
      .then(data => {
        resolve(data);
      })
      .catch(e => {
        console.log(e);
        reject(e);
      });
      });
    }
  
    //edit profile from customer_id,first_name,last_name,email,oldpassword,password,checkpassword
    editProfile(URL,customer_id,first_name,last_name,email,oldpassword,password,checkpassword)
    {
      return new Promise((resolve, reject) => 
      {
      this.post(URL,{customer_id:customer_id,first_name:first_name,last_name:last_name,email:email,
        oldpassword:oldpassword,password:password,checkpassword:checkpassword})
      .then(data => {
        resolve(data);
      })
      .catch(e => {
        console.log(e);
        reject(e);
      });
      });
    }

    //login from email , password
    loginUser(URL,email,password,cartdata)
    {
     return new Promise((resolve, reject) => 
      {
      this.post(URL,{email:email,password:password,cartdata:cartdata})
      .then(data => {
        resolve(data);
      })
      .catch(e => {
        console.log(e);
        reject(e);
      });
    });

    }

    //forget password from email
    forgotPassword(URL,email)
    { 
      return new Promise((resolve, reject) => 
      {
      this.post(URL,{email:email})
      .then(data => {
        resolve(data);
      })
      .catch(e => {
        console.log(e);
        reject(e);
      });
    });
    }
    
    //signup from first_name,telephone,last_name,email,address,city,postalcode,country,password
    signupUser(URL,first_name,telephone,last_name,email,address,city,postalcode,country,password,state,cartdata)
    { 
      return new Promise((resolve, reject) => 
      {
      this.post(URL,{first_name:first_name,telephone:telephone,last_name:last_name,email:email,
        address:address,city:city,postalcode:postalcode,country:country,password:password,state:state,cartdata:cartdata
      })
      .then(data => {
        resolve(data);
      })
      .catch(e => {
        console.log(e);
        reject(e);
      });
    });
    }

    //get product detail from product_id
    getProductDetail(URL,product_id,cutomer_id)
    {
      return new Promise((resolve, reject) => 
      {
      this.post(URL,{product_id:product_id,cutomer_id:cutomer_id})
      .then(data => {
        resolve(data);
      })
      .catch(e => {
        console.log(e);
        reject(e);
      });
     });    
    }
  
    //add to cart from product_id,quantity,customer_id,attribute_id,attribute_value
    addTocart(URL,card_id,product_id,quantity,customer_id,attribute_id,attribute_value)
    {
      return new Promise((resolve, reject) => 
      {
      this.post(URL,{card_id:card_id,product_id:product_id,quantity:quantity,customer_id:customer_id,attribute_id:attribute_id,
        attribute_value:attribute_value})
      .then(data => {
        resolve(data);
      })
      .catch(e => {
        console.log(e);
        reject(e);
      });
    });
    }

   //submit review
    submitReview(URL,product_id,customer_id,nick_name,review_summary,review,rating_price,rating_value,rating_quality)
    {
      return new Promise((resolve, reject) => 
      {
      this.post(URL,{product_id:product_id,customer_id:customer_id,nick_name:nick_name,review_summary:review_summary
      ,review:review,rating_price:rating_price,rating_value:rating_value,rating_quality:rating_quality})
      .then(data => {
        resolve(data);
      })
      .catch(e => {
        console.log(e);
        reject(e);
      });
    });
    }
   
    //get cart listing from
    getCartListing(URL,card_id,customer_id)
    {
      return new Promise((resolve, reject) => 
      {
      this.post(URL,{card_id:card_id,customer_id:customer_id})
      .then(data => {
        resolve(data);
      })
      .catch(e => {
        console.log(e);
        reject(e);
      });
    });
    }

    //apply coupon code
    applyCouponCode(URL,couponcode)
    {
      return new Promise((resolve, reject) => 
      {
      this.post(URL,{couponcode:couponcode})
      .then(data => {
        resolve(data);
      })
      .catch(e => {
        console.log(e);
        reject(e);
      });
    });
    }

    removeCouponCode(URL,couponcode,customer_id,card_id)
    {
      return new Promise((resolve, reject) => 
        {
        this.post(URL,{couponcode:couponcode,customer_id:customer_id,card_id:card_id})
        .then(data => {
          resolve(data);
        })
        .catch(e => {
          console.log(e);
          reject(e);
        });
      });
    }
  
    //get product listing in grid form
    getProductListing(URL,category_id,brand,page,input_text,tabs)
    {
      return new Promise((resolve, reject) => 
        {
        this.post(URL,{category_id:category_id,brand:brand,page:page,input_text:input_text,tabs:tabs})
        .then(data => {
          resolve(data);
        })
        .catch(e => {
          console.log(e);
          reject(e);
        });
      });
    }
    //get order detail from order_id
    getOrderdetail(URL,order_id)
    {
      return new Promise((resolve, reject) => 
      {
      this.post(URL,{order_id:order_id})
      .then(data => {
        resolve(data);
      })
      .catch(e => {
        console.log(e);
        reject(e);
       });
      });
    }

    //submit feedback from contact
    submitFeedback(URL,name,email,telephone,comment)
    {
        return new Promise((resolve, reject) => 
        {
        this.post(URL,{name:name,email:email,telephone:telephone,comment:comment})
        .then(data => {
          resolve(data);
        })
        .catch(e => {
          console.log(e);
          reject(e);
         });
        });
    }

    //get checkout data
    getCheckoutData(URL,card_id,customer_id)
    {
      return new Promise((resolve, reject) => 
        {
        this.post(URL,{card_id:card_id,customer_id:customer_id})
        .then(data => {
          resolve(data);
        })
        .catch(e => {
          console.log(e);
          reject(e);
         });
        });
    }
    //submitBraintreePayemnt
    submitBraintreePayment(URL,amount,payment_nonce,card_id,customer_id)
    {
      return new Promise((resolve, reject) => 
      {
      this.post(URL,{amount:amount,payment_nonce:payment_nonce,card_id:card_id,customer_id:customer_id})
      .then(data => {
        resolve(data);
      })
      .catch(e => {
        console.log(e);
        reject(e);
       });
      });
    }   
   
  
    //add Address
    addAddress(URL,customer_id,first_name,company,telephone,last_name,fax,street,city,state,postcode,country,billing,shipping,address_id)
    {
      if(billing=="1"&&shipping=="0")
      {
        return new Promise((resolve, reject) => {
          this.post(URL,{customer_id:customer_id,first_name:first_name,company:company,telephone:telephone
          ,last_name:last_name,fax:fax,street:street,city:city,state:state,postcode:postcode
          ,country:country,billing:billing,address_id:address_id})
            .then(data => {
              resolve(data);
            })
            .catch(e => {
              console.log(e);
              reject(e);
            });
          });
      }
      else if(shipping=="1"&&billing=="0")
      {
        return new Promise((resolve, reject) => {
          this.post(URL,{customer_id:customer_id,first_name:first_name,company:company,telephone:telephone
          ,last_name:last_name,fax:fax,street:street,city:city,state:state,postcode:postcode
          ,country:country,shipping:shipping,address_id:address_id})
            .then(data => {
              resolve(data);
            })
            .catch(e => {
              console.log(e);
              reject(e);
            });
          });
      }
      else if(billing=="1"&&shipping=="1")
      {
        return new Promise((resolve, reject) => {
          this.post(URL,{customer_id:customer_id,first_name:first_name,company:company,telephone:telephone
          ,last_name:last_name,fax:fax,street:street,city:city,state:state,postcode:postcode
          ,country:country,billing:billing,shipping:shipping,address_id:address_id})
            .then(data => {
              resolve(data);
            })
            .catch(e => {
              console.log(e);
              reject(e);
            });
          });
      }
      else if(billing=="0"&&shipping=="0") 
      {
        return new Promise((resolve, reject) => {
          this.post(URL,{customer_id:customer_id,first_name:first_name,company:company,telephone:telephone
          ,last_name:last_name,fax:fax,street:street,city:city,state:state,postcode:postcode
          ,country:country,address_id:address_id})
            .then(data => {
              resolve(data);
            })
            .catch(e => {
              console.log(e);
              reject(e);
            });
          });
      }
     
    }

    //filter category
    getfilter_category(URL,cat_id)
    {
      //const path = URL+"&cat_id="+cat_id;
      return new Promise((resolve, reject) => {
        this.post(URL,{cat_id:cat_id})
          .then(data => {
            resolve(data);
          })
          .catch(e => {
            console.log(e);
            reject(e);
          });
        });
    }

    //filter product
    getFilterProductList(URL,category_id,attribute_code)
    {
      return new Promise((resolve, reject) => {
        if(attribute_code==true){
            this.post(URL,category_id)
              .then(data => {
                resolve(data);
              })
              .catch(e => {
                console.log(e);
                reject(e);
              });
        }else{
          this.post(URL,category_id)
              .then(data => {
                resolve(data);
              })
              .catch(e => {
                console.log(e);
                reject(e);
              });
        }
        
      });
    }

     //paypal_paymentcheck
     paypal_paymentcheck(URL,customer_id,card_id,pay_id,client_id,secret_key,amount,currency)
     {
        return new Promise((resolve, reject) => 
        {
        this.post(URL,{customer_id:customer_id,card_id:card_id,pay_id:pay_id,client_id:client_id
          ,secret_key:secret_key,amount:amount,currency:currency})
        .then(data => {
          resolve(data);
        })
        .catch(e => {
          console.log(e);
          reject(e);
        });
      });
     }

     //submit credit card value
    submitCardvalue(URL,customer_id,card_id,amount,currency,creditcard,expiration_year,expiration_month,
      cvv,cards_name)
    {
      return new Promise((resolve, reject) => 
      {
        this.post(URL,{customer_id:customer_id,card_id:card_id,amount:amount,currency:currency,
          creditcard:creditcard,expiration_year:expiration_year,expiration_month:expiration_month,
          cvv:cvv,cards_name:cards_name})
        .then(data => {
          resolve(data);
        })
        .catch(e => {
          console.log(e);
          reject(e);
        });
      });
    }

    getGooglePayResponce(URL,data)
    {
      return new Promise((resolve, reject) => 
      {
        this.post(URL,{data:data})
        .then(data => {
          resolve(data);
        })
        .catch(e => {
          console.log(e);
          reject(e);
        });
      });
    }

    addtestimonial(URL,name,email,message,rating)
    {
      return new Promise((resolve, reject) => 
      {
        this.post(URL,{name:name,email:email,message:message,rating:rating})
        .then(data => {
          resolve(data);
        })
        .catch(e => {
          console.log(e);
          reject(e);
        });
      });
    }

    editcart(URL,customer_id,card_id,card_data)
    {
      return new Promise((resolve, reject) => 
      {
        this.post(URL,{customer_id:customer_id,card_id:card_id,card_data:card_data})
        .then(data => {
          resolve(data);
        })
        .catch(e => {
          console.log(e);
          reject(e);
        });
      });
    }

    clearCart(URL,customer_id,card_id)
    {
      return new Promise((resolve, reject) => 
      {
        this.post(URL,{customer_id:customer_id,card_id:card_id})
        .then(data => {
          resolve(data);
        })
        .catch(e => {
          console.log(e);
          reject(e);
        });
      });
    }

    cutomernewssubscribe(URL,customer_id,status)
    {
      return new Promise((resolve, reject) => 
      {
        this.post(URL,{customer_id:customer_id,status:status})
        .then(data => {
          resolve(data);
        })
        .catch(e => {
          console.log(e);
          reject(e);
        });
      });
    }

 //Newsletter subscription
    newslettersubscription(URL,email)
    {
      return new Promise((resolve, reject) => 
      {
      this.post(URL,{email:email})
      .then(data => {
        resolve(data);
      })
      .catch(e => {
        console.log(e);
        reject(e);
      });
    });
    }

}


export default service = new Service();