import React, { Component } from 'react';
import {StyleSheet,Dimensions,View,Image,TouchableOpacity,Text,BackHandler,NetInfo,Platform,Linking} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import {StackNavigator} from 'react-navigation'
import CrossIcon from 'react-native-vector-icons/Entypo'

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;
const offset = (Platform.OS=="android")?-500:0;

//for header logo manage left and right padding
const HeaderLogo=(value)=>{
  return(
      value==true?
      <View style={Constant.styles.logoViewRightPadding}>
          <Image resizeMode={'contain'}  style={Constant.styles.logoImage} source={Constant.logoImage}/>
      </View>:
      <View style={Constant.styles.logoViewLeftPadding}>
          <Image resizeMode={'contain'}  style={Constant.styles.logoImage} source={Constant.logoImage}/>
      </View>
  );
}

//for center header logo
const CenterHeaderLogo=()=>{
  return(
      <View style={Constant.styles.logoViewCenter}>
          <Image resizeMode={'contain'} style={Constant.styles.logoImage} source={Constant.logoImage}/>
      </View>
  );
}

//action bar with dark green background
const secureChekcoutHeader=(navigation,name)=>{
  return( 
  <View style={Constant.styles.checkoutHeaderStyle}>
      <TouchableOpacity style={Constant.styles.whitecrossIcon} onPress={() => navigation.goBack()}>
      <CrossIcon name={"cross"} size={25} color={'#ffffff'}/>
      </TouchableOpacity>
      <Text style={Constant.styles.headerwhiteboldtext}>{name}</Text>
  </View>
  )
}

//back icon from drawer screen
const BackIconDrawer=({navigate})=>{
  return(
    <TouchableOpacity  onPress={() => navigate('Home')}>
        <Icon name="ios-arrow-back" 
          size={28} color={Constant.color.darkGreen} 
          style={Constant.styles.p15Left}/>
   </TouchableOpacity>
  );
}

//back icon from other screen
const BackIcon=({navigate})=>{
  return(
      <Icon name="ios-arrow-back" 
      size={28} color={Constant.color.darkGreen} 
      onPress={() => navigate('Home')} 
      style={Constant.styles.p15Left}/>
  );
}

// drawer menu icon
const DrawerIcon = ({ navigate }) => {
  return (
      <Icon
          name="md-menu"
          size={28}
          color={Constant.color.darkGreen}
          onPress={() => navigate('DrawerOpen')}
          style={Constant.styles.p15right}
      />
  );
}

//Link to social site
const  openUrl = (url) => {
     Linking.canOpenURL(url).then(supported => {
       if (supported) {
         Linking.openURL(url);
       } else {
         console.log("Don't know how to open URI: " + this.props.url);
       }
     });
   };
 
//all string using in app
const StringText={
  WELCOME:"WELCOME",
  READMORE:"Read More",
  CARDHOLDERNAME:"Card Holder Name",
  paypalClient_id:"ASgsMTwoBHyhZFkIkYKE7Iz3yqvx75ZjS0mfW1So1Jg6D3zRYDdj4EsSx16I9DZ8ZBCMetfnRdqZAvvm",
  paypal_secret_key:"EBTSbjBkvGNZwXktxb6QPz9YcwYRxJCb-dAAtERjNLQBd-JL0mSdIr9ibtP942a3I8HwJRLnQWxuT_6Y",
  QTY:"Qty: ",
  Home:"Home",
  Search:"Search",
  Cart:"Cart",
  Wishlist:"WishList",
  Account:"Account",
  BRANDS:"Brands",
  Products:"Products",
  SHOPBYBRAND:"Shop By Brand",
  SHOPBYPEODUCTS:"Shop By Products",
  WhatAreLookingFor:"What are you looking for ?",
  GIFTANDNVS:"Gifts @ NVS Pharmacy",
  FREESAMPLEWITHORDER:"FREE SAMPLES WITH EVERY ORDER",
  ONBEAUTY:"10% OFF ON BEAUTY",
  GIFTFORHER:"10% OFF GIFT FOR HER",
  GIFTFORHIM:"UPTO 20% OFF GIFT FOR HIM",
  ONCLARINS:"EXTRA 10% OFF ON CLARINS",
  POPULARBRANDS:"Popular Brands",
  FEATUREPRODUCT:"Featured Products",
  FEATUREBRADNS:"Featured Brands",
  FACEBOOK:"Facebook",
  TWITTER:"Twitter",
  GOOGEL:"Google +",
  FACEBOOKLINK:"https://www.facebook.com/NVSPharmacy/",
  TWITTERLINK:"https://twitter.com/#!/nvspharmacy/",
  GOOGLELINK:"https://plus.google.com/u/0/+NvspharmacyUk",
  BRANDATOZ:"Brands A to Z",
  CATEGORYATOZ:"Categories A to Z",
  SPECIALOFFER:"Special Offers",
  YOURORDER:"Your Order",
  ORDERDETAIL:"ORDER DETAIL",
  YOURWISHLIST:"Your Wish List",
  SETTING:"Settings",
  INFORMATION:"Information",
  CUSTOMERSERVICE:"Customer Service",
  NEWSLETTER:"Newsletter",
  BLOG:"Blog",
  MBT:"MBT",
  ACCOUNT:"Account",
  FEEDBACK:"Feedback",
  ABOUTUS:"About Us",
  CONTACTUS:"Contact Us",
  ADDTOBAG:"Add to Bag",
  BRAND:"Brand: ",
  SIZE:"Size: ",
  NOW:"Now £",
  WAS:"Was £",
  DONE:"Done",
  ITEMS:"items",
  TOTAL:"Total",
  SUBTOTAL:"Subtotal",
  CLEARALL:"Clear All",
  CLEARCART:"Clear Cart",
  SECURECHECKOUT:"Secure Checkout",
  SELECTQUANTITY:"Select Quantity",
  ENTERQUANTITY:"Enter Quantity",
  ADDTOCART:"Add to Cart",
  FEATURED:"Featured",
  NEWEST:"Newest",
  RATING:"Rating",
  PRICE:"Price",
  FILTERS:"Filters",
  ASTERISK:"*",
  COLOR:"Color ",
  AVALABILITY:"Availability: ",
  DESCRIPTION:"Description",
  INGREDIENTS:"Ingredients",
  HOWTOUSE:"How to use",
  EXTRAINFO:"Extra Info",
  PRODUCTREVIEWS:"Product's Review",
  REVIEWBY:"Review by",
  WRITEYOUROWNREVIEW:"Write Your Own Review",
  YOUAREREVIEWING:"You're reviewing:",
  HOWDOYOURATE:"How do you rate this product?",
  NICKNAME:"Nickname",
  SUMMARYYOURREVIEW:"Summary of Your Review",
  REVIEW:"Review",
  SUBMITREVIEW:"SUBMIT REVIEW",
  SUBMITYOURTESTIMONIAL:"SUBMIT YOUR TESTIMONIAL",
  VALUE:"Value",
  QUALITY:"Quality",
  SIGNINTITLE:"Sign in to continue or Sign up for new account",
  SIGNINYOURACCOUNT:"Sign in to your account",
  EMAIL:"Email",
  EMAILPLACHOLDER:"eg:Yourname@gmail.com",
  ENTERCOUPONCODE:"please enter your coupon code",
  PASSWORD:"Password",
  REMEMBERME:"Remember me",
  SECUREINSECURLY:"Secure in Securly",
  CREATENEWCCOUNT:"Create a new account with us , please signup.",
  FORGOTPASSWORD:"Forgot Your Password?",
  FIRSTNAME:"First Name",
  LASTNAME:"Last Name",
  TELEPHONE:"Telephone",
  COMPANY:"Company",
  EMAILADDRESS:"Email Address",
  ADDRESS:"Address",
  FAX:"Fax",
  CITY:"City",
  STREETADDRESS:"Street Address",
  PINCODE:"Zip/Postal Code",
  STATEPROVIENCE:"State/Province",
  COUNTRY:"Country",
  POSTALCODE:"Post Code",
  CREATACCOUNTLATERUSE:"Create an account for later use",
  DELIVERTHISADDRESS:"Deliver to this address",
  CREATANACCOUNT:"Create an account",
  COUNTIASAGUEST:"Countinue as a guest",
  CONTINUESECURILY:"Continue Securily",
  EDITCART:"Edit Cart",
  UPDATECART:"Update Cart",
  REGISTEREDCUSTOMERS:"Registered Customers",
  IFYOUHAVEANACCOUNT:"If you have an account with us, please log in.",
  NEWCUSTOMERS:"New Customers",
  RETRIVEYOURPASSWORDHERE:"Please enter your email address below. You will receive a link to reset your password.",
  SUBMIT:"Submit",
  LOGOUT:"Logout",
  IAMNEW:"I'm New",
  SIGNIN:"Sign In",
  SIGNINTO:"Signin to",
  YOURACCOUNT:"Your Account",
  TEXTACCOUNTBANNER:"By creating an account with our store,\nyou will be able to move through the\ncheckout process faster,store multiple\nshipping addresses,view and track your\norders in your account and more.",
  MYDASHBOARD:"My Dashboard",
  ACCINFORMATION:"Account Information",
  CONTACTINFORMATION:"Contact Information",
  ADDRESSBOOK:"Address Book",
  MANAGEADDRESS:"Manage Addresses",
  EDIT:"Edit",
  CHANGEPASSWORD:"Change Password",
  DEFAULTBIILINGADD:"Default Billing Address",
  CHANGEBILLINGADDRESS:"Change Billing Address",
  CHANGESHIPPINGADDRESS:"Change Shipping Address",
  DEFAULTSHIPPINGADD:"Default Shipping Address",
  FORGOTMESSAGE:"Please check your mail box to reset password.",
  MYBAG:"My Bag",
  PROCEDTOCHECKOUT:"PROCEED TO CHECKOUT",
  YOUHAVE:"You have",
  ITMEINYOURBAG:"item in your bag",
  APPLY:"APPLY",
  REMOVE:"Remove",
  DASHBOARDTEXT:"From your My Account Dashboard you have the ability to view a snapshot of your recent account activity and update your account information. Select a link below to view or edit information.",
  ADDRESSBOOK:"Address Book",
  DELETEACCOUNTTEXT:"If you proceed to delete your account, all data we hold about you will be destroyed or anonymised. This operation cannot be un-done.",
  DELETMYACCOUNT:"Delete my account",
  NODATAAVAILBLE:"No data avalible",
  EDITACCINFORM:"Edit Account Information",
  SAVE:"Save",
  CURRENTPASSWORD:"Current Password",
  NEWPASSWORD:"New Password",
  CONFIRMNEWPASWORD:"Confirm New Password",
  ADDITIONALADDRESSENTRY:"Additional Address Entries",
  NOADDTIONALADDENTRY:"No Additional Address Entries Avaliable",
  EDITADDRESS:"Edit Address",
  DELETEADDRESS:"Delete Address",
  USEMYDEFAULTBILLING:"Use as my default billing address",
  USEMYDEFAULTSHIPPING:"Use as my default shipping address",
  DELIVERYMETHOD:"Delivery Method",
  PAYMENTMETHOD:"Payment Method",
  NAMENADDRESS:"Name & Address",
  CREDETCARDS:"Credit/Debit Cards",
  CARTTYPE:"Card Type",
  EXPIRYYEAR:"Expiry Year",
  CARDNUMBER:"Card Number",
  CVVNUMBER:"CVV Number",
  EXPIRYMONTH:"Expiry Month",
  PAYPALCREADIT:"Paypal Credit",
  PAYPAL:"PayPal",
  REVIEWYOURORDER:"Review Your Order",
  PRODUCTNAME:"Product Name",
  GRANDTOTAL:"Grand Total",
  PLACEORDERSECURLY:"Place Order Securily",
  ADDNEWADDRESS:"Add New Address",
  //validation value
  ENTERNAME:"Enter your name",
  ENTERCOMMENT:"Enter your comment",
  ENTERFIRSTNAME:"Enter your first name",
  ENTERLASTNAME:"Enter your last name",
  ENTERTELEPHONE:"Please enter telephone",
  ENTERSTREET:"Please enter street",
  ENTERCITY:"Please enter city",
  ENTERSTATE:"Please enter state",
  ENTERPOSTALCODE:"Please enter postal code",
  SELECTCOUNTRY:"Please select country",
  ENTEREMAIL:"Enter your email",
  ENTERADDRESS:"Enter your address",
  ENTERPASSWORD:"Enter your password",
  ENTERCURRENTPASSWORD:"Enter your current password",
  ENTERNEWPASSWORD:"Enter your new password",
  ENTERCONFIRMPASSWORD:"Enter your confirm password",
  PASSWORDNOTMATCH:"New password and confirm password should be same",
  SUCESSFULLYADDEDADDRESS:"Sucessfully added address.",
  RESETPASSWORD:"Please check your mail box to reset password.",
  SELECTVALUE:"Please select value",
  ENTERNICKNAME:"Please enter your nickname",
  ENTERSUMMARYREVIEW:"Please enter Summary review",
  ENTERREVIEW:"Please enter review",
  RATINGFORPRICE:"Please give rating for price",
  RATINGFORVALUE:"Please give rating for value",
  RATINGFORQUATITY:"Please give rating for quantity",
  REQUIREFILED:"This is required filed",
  MAXIMAMQUANTITY:"*The maximum quantity allowed for purchase is 50.",
  DATANOTAVAILABLE:"Data Not Available",
  MAILADDRESS:"Mail Address",
  NAME:"Name",
  COMMENT:"Comment",
  STORELOCATION:"Store Location",
  HOWTOORDER:"How to order",
  TESTIMONIAL:"Testimonials",
  SHIPPINGDELIVERY:"Shipping & Delivery",
  GPHCMHRA:"GPhC & MHRA",
  //customer service
  FAQS:"FAQ's",
  SHAKEFORFEEDBACK:"Shake for feedback",
  TRACKINGORDER:"Tracking Orders",
  SITEMAP:"Sitemap",
  TERMANDCONDITION:"Terms & Conditions",
  PRIVACYPOLICY:"Privacy Policy",
  RETURNPOLICY:"Returns Policy",
  DATAPROTECTIONPOLICY:"Data Protection Policy",
  PROBLEMSORCOMPLAINS:"Problems or Complaints",
  MYWISHLIST:"My Wishlist",
  RECENTVIEWITEM:"Recently Viewed Items",
  MYPRODUCTREVIEW:"My Product Reviews",
  YOURORDERHISTORY:"Your Order History",
  MYORDER:"My Orders",
  DISCOUNTCODES:"Discount Codes",
  FORGOTANITEM:"Forgot an item?",
  EDITYOURCART:"Edit your cart",
  PAYPALSITE:"You will be redirected to the PayPal website.",
  ORDERID:"OrderId#",
  DATE:"Date:",
  SHIPTO:"Ship To:",
  ORDERTOTAL:"Order Total:",
  CHECKINTERNETCONNECTIVITY:"Please check your internet connectivity",
  NEEDLOGIN:"Please Login First",
  cartValue:"",
  SORTING:"Sort By",
  NEWSLETTERSUBSCRIPTION:"Newsletter Subscription",
  ENTERCARDNUMDER:"Please enter card number",
  SELECTMONTH:"Please select expiry month",
  SELECTYEAR:"Please select expiry year",
  ENTERCVV:"Please select cvv number",
  ENTERCARDHOLDERNAME:"Please enter card holder name",
  DELETEACCOUNT:"Delete Account",
  GENERALSUBSCRIPTION:"General Subscription",
}

//IOS picker style
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    width:120,       
    fontSize: 12,        
    paddingLeft:5, 
    paddingTop:5,        
    backgroundColor:'white',      
    color:'black',
  }
});


const pickerStylebox = StyleSheet.create({        
  inputIOS: {       
     borderColor: 'lightgrey',
     fontSize: 14,    
     borderWidth:1,   
     height:40,   
      paddingHorizontal: 10,    
       fontSize: 14,            
        paddingTop: 13,            
         paddingBottom: 12,            
          backgroundColor: 'white',          
             color: 'black',},     
});

//IOS picker style
const monthpickerSelectStyles = StyleSheet.create({
  inputIOS: {
      width:155,
      fontSize: 14,
      paddingTop: 13,
      paddingHorizontal: 10,
      paddingBottom: 12,
      borderWidth: 1,
      borderColor: 'lightgrey',
      backgroundColor: 'white',
      color: 'black',
  },
});

//IOS picker style
const addresspickerSelectStyles = StyleSheet.create({
  inputIOS: {
      width:'100%',
      fontSize: 14,
      paddingTop: 13,
      paddingHorizontal: 10,
      paddingBottom: 12,
      borderWidth: 1,
      borderColor: 'lightgrey',
      backgroundColor: 'white',
      color: 'black',
  },
});
 
  var base_url="https://nvspharmacy.co.uk/webservice.php?function_name=";
// constant for style , image , method , webservices
  const Constant = {
    offset:offset,
    monthpickerSelectStyles:monthpickerSelectStyles,
    addresspickerSelectStyles:addresspickerSelectStyles,
    pickerSelectStyles:pickerSelectStyles,
    pickerStylebox:pickerStylebox,
    StringText:StringText,
    SecureCheckoutHeader:secureChekcoutHeader,
    DrawerIcon:DrawerIcon,
    HeaderLogo:HeaderLogo,
    centerLogo:CenterHeaderLogo,
    BackIconDrawer:BackIconDrawer,
    BackIcon:BackIcon,
    openUrl:openUrl,
    filter:require('../resource/img/filter.png'),
    acending:require('../resource/img/acending.png'),
    decending:require('../resource/img/decending.png'),
    accountBanner:require('../resource/img/accountbanner.jpg'),
    decreaseqty:require('../resource/img/remove.png'),
    increaseqty:require('../resource/img/add.png'),
    greentickmark:require('../resource/img/tick.png'),
    QuantityImageArrow:require('../resource/img/arrowqty.png'),
    RatedStar:require('../resource/img/rated_star.png'),
    InRatedStar:require('../resource/img/inrated_star.png'),
    roundedHeartImage:require('../resource/img/hearth.png'),
    LeftArrow:require('../resource/img/left_arrow.png'),
    RightArrow:require('../resource/img/right_arrow.png'),
    middleBanner:require('../resource/img/middle_banner.jpg'),
    facebookIcon:require('../resource/img/fb.png'),
    twitterIcon:require('../resource/img/twitter.png'),
    google_plus:require('../resource/img/google_plus.png'),
    horizontaldotImgae:require('../resource/img/dotted_line_horizontal.png'),
    verticaldotImage:require('../resource/img/dotted_line.png'),
    logoImage : require('../resource/img/logo.png'),
    checkout1Logo:require('../resource/img/logos.jpg'),
    checkout2Logo:require('../resource/img/pay_logo.png'),
    slideImagefilter:require('../resource/img/slider_button.png'),
    downpickericon:require('../resource/img/dwnarrow.png'),
    //webservices
    baseUrl:base_url, 
    home_banner_image:base_url+"home_banner",
    home_blocks_gift:base_url+"home_blocks_gift",
    feachured_product:base_url+"feachured_product",
    popular_item:base_url+"top_brand",
    getBrandCategories:base_url+"getBrandCategories",
    getProductCategories:base_url+"getProductCategories",
    get_categories_products:base_url+"get_categories_products",
    product_detail:base_url+"product_detail",
    submit_review:base_url+"add_product_review",
    getSpecialOffer:base_url+"getOffers",
    addToCart:base_url+"addtocart",
    getCartDetail:base_url+"getcartdetails",
    loginservice:base_url+"login_authentication",
    deleteItemsincart:base_url+"deleteItemsincart",
    registerservice:base_url+"createcustomer",
    resgisterWithcard:base_url+"createcustomer_cart",
    countryList:base_url+"countrylist",
    credit_cardValue:base_url+"creditcard_payment_wp",
    couponApply:base_url+"checkoutprocess",
    getUserProfile:base_url+"getcustomerdetails",
    deleteAccount:base_url+"deletecustomer",
    getwishlist:base_url+"getcustomerwishlist",
    addwishListitem:base_url+"addtowishlist",
    addAddress:base_url+"addcustomer_address",
    deletewishlistitem:base_url+"deletecustomerwishlistitem",
    clearallwishlist:base_url+"delete_customer_allwishlistitems",
    deletecustomer_address:base_url+"deletecustomer_address",
    editProfile:base_url+"edit_customerprofile",
    getPagecontent:base_url+"get_aboutpagecontent",
    forgotPassword:base_url+"forgetpassword",
    getFaqscontent:base_url+"faq_content",
    deletrecentviewitem:base_url+"deleterecentlyviewedproductbyid",
    contactus:base_url+"contactinformation",
    contact_submit:base_url+"contactform",
    getCartId:base_url+"getcardid_bycustomerid",
    checkoutprocess:base_url+"checkoutprocessstep1",
    checkoutprocess2:base_url+"checkoutprocessstep2",
    getOrderListing:base_url+"my_order",
    updatecart:base_url+"updatecart_items",
    clearcart:base_url+"clearcartItems",
    getorderdetail:base_url+"get_customer_orderinformation",
    save_responce:base_url+"response_gpay",
    getResponse:base_url+"getresponse_gpay",
    getallWPposts:base_url+"getallWPposts",
    getpost_details:base_url+"getpost_details",
    removecouponcode:base_url+"removecouponcode",
    layered_navigation1:base_url+"layered_navigation1",
    testimonial_list:base_url+"testimonial_list",
    add_testimonial:base_url+"testimonial_add",
    emailsubscribe:base_url+"emailsubscribe",
    customer_emailsubscribe:base_url+"customer_emailsubscribe",
    //webservice end
    windowWidth: WINDOW_WIDTH,
    windowHeight: WINDOW_HEIGHT,
    imageHeight:150,
    imagewidth:150,
    Currency: '£',
    color:{
      darkGreen:'#006d50',
      lightGreen:'#98c954',
      lightGrey:'lightGrey',
      white:'#ffffff',
      black:'#000000',
    },
    styles: StyleSheet.create({
      detailpickerstyle:{
        flexDirection:'row',justifyContent:'center',alignItems:'center',width:150,
        height:30,borderColor:'lightgrey',borderWidth:1,borderRadius:20
      },
      htmlStyle:{
        fontWeight:'300',
        color: '#006d50', // pink links
      },
      backWhite:{
        backgroundColor:'#ffffff'
      },
      p15right:{
        paddingRight: 15
      },
      m10Right:{
        marginRight:10
      },
      filedViewsignup:{flex:1,flexDirection:'column',marginRight:10},
      mTop10mB40:{
        marginTop:10,marginBottom:40
      },
      p10Top:{
        paddingTop: 10,flex: 1
      }, 
      m10Top:{
        marginTop:10
      },
      p15Left:{
        paddingLeft: 15
      },
      settingPannelStyle:{
        flexDirection:'row',padding:10,alignSelf:'center',alignItems:'center'
      },
     logoViewLeftPadding:{
        flex:1,alignSelf:'center',alignItems:'center',paddingLeft:Platform.OS=='android'?50:0
      },
      logoViewRightPadding:{
        flex:1,alignSelf:'center',alignItems:'center',paddingRight:Platform.OS=='android'?50:0
      },
      logoViewCenter:{
       alignSelf:'center',alignItems:'center',justifyContent:'center',flex:1
      },
      logoImage:{
        width:"60%",height:Platform.OS=='android'?40:40,alignSelf:'center'
      },
      tabBarstyle:{
        backgroundColor:'#ffffff',margin:0,borderTopColor:'#000000',borderTopWidth:1,paddingTop:2
      },
      tabBarstyleWithoutIcon:{
        backgroundColor:'#ffffff',borderTopColor:'lightgrey',borderTopWidth:1,borderBottomColor:'lightgrey',borderBottomWidth:1
      },
      labelStyle:{
        fontSize:10,fontWeight:'bold',flex:1,margin:0,padding:0
      },
      labelStyleWithoutIcon:{
        fontSize:14,fontWeight:'normal',flex:1
      },
      indicatorStyle:{
        backgroundColor:'transparent'
      },
      IconBadgeStyle: {
          position:'absolute',
          minWidth:15,
          height:15,
          borderRadius:15,
          borderWidth:1,
          borderColor:'#8cc63e',
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#ffffff'
        },
      RedIconBadgeStyle: {
        position:'absolute',
        minWidth:15,
        height:15,
        marginRight:14,
        borderRadius:15,
        borderWidth:1,
        borderColor:'#ea4335',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ea4335'
      },
      containerwithp10:{
        flex:1,backgroundColor:'#ffffff',padding:10
      },
      maincontainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
        width: "100%",
      },
      productImageSlider:{
        height:160,backgroundColor:'#ffffff'
      },
      container:{
        height:'100%',
        flex:1,
        width:'100%',
        backgroundColor:'#ffffff'
      },
      blacknormalcenterText:{
        color:'#000', 
        fontSize:15,
        alignSelf:'center',
        textAlign:'center',
        fontWeight:"500",
        padding:2
      },
      blacknormalleftTextpanel:{
        fontSize:15,
        color:'#000000',
      },
      blacknormalleftText:{
        flex:1.5,
        marginRight:10,
        textAlign: 'left',
        alignSelf:'center',
        color: '#000000',
        fontSize: 14,
        marginLeft:5,
        fontWeight:'normal',
      },
      blacknormalrightText:{
        textAlign: 'right',
        color: '#000000',
        fontSize: 14,
        flex: 1,
        fontWeight:'normal',
      },
      blackboltext:{
        textAlign: 'center',
        color: '#000000',
        fontSize: 15,
        fontWeight:'bold',
        padding:2
      },
      drawertext:{
        flex:1,
        textAlign: 'left',
        color: '#000000',
        fontSize: 14,
        fontWeight:'bold',
        padding:2
      },
      sliderfilterLeftText:{
        fontSize:15,color:'#000000',textAlign:'left'
      },
      sliderfilterRightText:{
        flex:1,fontSize:15,color:'#000000',textAlign:'right'
      },
      pricefilterTextview:{
        flexDirection: 'row',paddingLeft:20,paddingRight:20
      },
      headerfilterText:{
        flexDirection: 'row',paddingLeft:20,paddingRight:20,paddingBottom:10,paddingTop:10
      },
      blackboldlefttext:{
        textAlign: 'left',
        color: '#000000',
        fontSize: 14,
        fontWeight:'bold',
        padding:2
      },
      blackboldrighttext:{
        flex:1,
        textAlign: 'right',
        color: '#000000',
        fontSize: 14,
        fontWeight:'bold',
        padding:2
      },
      greennormelrightText:{
        textAlign: 'right',
        color: '#006d50',
        fontSize: 14,
        paddingRight:5,
        fontWeight:'normal',
      },
      greenboldrightText:{
        color: '#98c954',
        fontSize: 14,
        fontWeight:'bold',
      },
      greenboldleftText:{
        color: '#98c954',
        fontSize: 14,
        fontWeight:'bold',
        textAlign:'right',
        justifyContent:'flex-end'
      },
      simpleBox:{
        alignSelf:'center',borderColor:'lightgrey',borderWidth:1,padding:10,margin:10,flex:1
      },
      whitTextwithbackground:{
        color: '#ffffff',
        fontSize: 12,
        paddingTop:3,
        paddingBottom:3,
        paddingLeft:5,
        paddingRight:5,
        backgroundColor:'#006d50',
        fontWeight:'bold',
        borderRadius:10,
        borderWidth: 1,
      },
      greenTextWithline:{
        color: '#98c954',
        fontSize: 14,
        fontWeight:'bold',
        alignSelf:'center',
        textDecorationLine: 'underline'
      },
      greenTextWithlineLeft:{
        color: '#98c954',
        fontSize: 14,
        fontWeight:'bold',
        alignSelf:'flex-start',
        textDecorationLine: 'underline'
      },
      darkgreenText:{
        color: '#006d50',
        fontSize: 12,
        fontWeight:'normal',
      },
      headerwhiteboldtext:{
        paddingRight:40,
        flex:1,
        textAlign:'center',
        alignSelf:'center',
        color:'#ffffff',
        fontWeight:'bold',
        fontSize: 14,
      },
      whitecrossIcon:{
        marginLeft: 10,
        alignItems:'center',
        alignSelf:'center',
        alignContent:'center'
      },
      orangebutton:{
        width:'90%',
        padding:8,
        alignSelf:'center',
        justifyContent:'center',
        backgroundColor:'#FF7D25',
        borderRadius:20,
        borderWidth: 1,
        borderColor: '#FF7D25'
      },
      applycode:{
        width:'25%',
        padding:8,
        alignSelf:'flex-start',
        marginRight:5,
        justifyContent:'center',
        backgroundColor:'#FF7D25',
        borderRadius:20,
        borderWidth: 1,
        borderColor: '#FF7D25'
      },
      removecode:{
        width:'25%',
        padding:8,
        marginLeft:5,
        alignSelf:'flex-end',
        justifyContent:'center',
        backgroundColor:'#FF7D25',
        borderRadius:20,
        borderWidth: 1,
        borderColor: '#FF7D25'
      },
      darkGreenbutton:{
        marginTop:5,
        marginBottom:5,
        width:'90%',
        padding:8,
        alignSelf:'center',
        justifyContent:'center',
        backgroundColor:'#006d50',
        borderRadius:10,
        borderWidth: 1,
        borderColor: '#006d50'
      },
      testimonialbuttonView:{
        justifyContent:'flex-end',alignItems:'flex-end',
        alignSelf:'flex-end',paddingTop:10,paddingLeft:10,paddingRight:10
      },
      darkGreenDoneButton:{
        padding:10,
        justifyContent: 'flex-start',
        backgroundColor:'#006d50',
        borderRadius:10,
        borderWidth: 1,
        borderColor: '#006d50'
      },
      whitebuttontext:{
        textAlign: 'center',
        color: '#ffffff',
        fontSize: 14,
        fontWeight:'bold',
      },
      whitenormaltext:{
        color: '#ffffff',
        fontSize: 14,
        fontWeight:'normal',
        padding:2
      },
      whitesmallText:{
        color: '#ffffff',
        fontSize: 12,
        fontWeight:'normal',
        padding:2
      },
      whitenormalcentertext:{
        color: '#ffffff',
        textAlign:'center',
        flex:1,
        fontSize: 14,
        fontWeight:'normal',
        padding:2
      },
      whitenormalleftext:{
        color: '#ffffff',
        textAlign:'left',
        flex:2,
        fontSize: 14,
        fontWeight:'normal',
        padding:2
      },
      whitnormalrighttext:{
        color: '#ffffff',
        textAlign:'right',
        flex:1,
        fontSize: 14,
        fontWeight:'normal',
        padding:2
      },
      separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: 'lightgrey',
      },
      listcontainer:{
        flex: 1,
        paddingTop:5,
        paddingBottom:5,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor:'#ffffff'
      },
      listSmallText:{
        color: '#000000',                        
        fontWeight: 'normal',                
        fontSize: 12,
        padding: 2,
      },
      tosmallText:{
        alignSelf:'flex-end',
        color: '#000000',                        
        fontWeight: 'normal',                
        fontSize: 10,
        padding: 2,
      },
      stickybutton:{
        position:'absolute',alignSelf:'flex-end', bottom: 0,padding:10
      },
      stickybuttonstart:{
        position:'absolute',alignSelf:'flex-start', bottom: 0,padding:10
      },
      greenSmallText:{
        color: '#98c954',                        
        fontWeight: 'bold',                
        fontSize: 14,
        paddingTop: 5,
      },
      greenCenterText:{
        color: '#98c954',                        
        fontWeight: 'bold',                
        fontSize: 14,
        paddingTop: 5,
        textAlign:'center'
      },
      tagTextView:{
        marginLeft:5,
        marginRight:5,
        flexDirection:'row',
        paddingBottom:2,
        paddingTop:2,
        paddingLeft:5,
        paddingRight:5,
        alignItems:'center',
        alignSelf:'center',
        fontSize:12,
        fontWeight:'bold',
        backgroundColor:'lightgrey',
        color:'#000000',
        borderRadius:20,
        borderColor:'lightgrey',
        borderWidth:1
      },
      listSmallRedText:{
        alignSelf:'flex-end',
        color: '#ea4335',                      
        fontWeight: 'normal',                
        fontSize: 12,
        padding: 2,
      },
      greenborderbutton:{
        paddingTop:2,
        paddingBottom:2,
        paddingLeft:50,
        paddingRight:50,
        margin:5,
        backgroundColor:'transparent',
        borderRadius:20,
        borderWidth: 2,
        borderColor: '#89c058'
      },
      imageBackgroundcontainer: {
        backgroundColor:'transparent',
        width:'100%',
        height:250,
      },
      viewStyle:{
        width:'100%',
        marginTop:10,
        marginBottom:10,
        height: 1,
        backgroundColor: 'lightgrey',
      },
      verticalviewStyle:{
        marginLeft:10,
        marginRight:10,
        width: 1,
        height:40,
        backgroundColor: 'lightgrey',
      }, 
      redregulartext:{
        textAlign: 'left',
        color: '#e02814',
        fontSize: 14,
      },
      boxwithborder:{
        textAlign: 'left', 
        backgroundColor : "#FFFFFF",
        height:40,
        paddingLeft:5,
        borderWidth: 1,
        flexDirection:'row',
        borderColor: 'lightgrey',
      },
      messageboxwithborder:{
        textAlignVertical: "top",
          textAlign: 'left', 
          alignItems: "center",
          flex: 1,
          backgroundColor : "#FFFFFF",
          height:150,
          paddingLeft:5,
          borderWidth: 1,
          flexDirection:'row',
          borderColor: 'lightgrey',
      },
      boxbordershorting:{
        flex:1,
        textAlign: 'center',
        justifyContent:'center', 
        backgroundColor : "#FFFFFF",
        height:40,
        borderWidth: 1,
        flexDirection:'row',
        borderColor: 'lightgrey',
      },
      addressborderbox:{
        textAlign: 'left', 
        backgroundColor : "#FFFFFF",
        borderWidth: 1,
        flexDirection:'column',
        borderColor: 'lightgrey',
        margin:5,
        padding:2,
      },
      boxQuanity:{
        width:Platform.OS=='android'?'100%':150,
        textAlign: 'left', 
        backgroundColor : "#FFFFFF",
        height:30,
        alignItems: 'center',
        borderWidth: 1,
        borderRadius:20,
        flexDirection:'row',
        borderColor: 'lightgrey',
      },
      quantiyBox:{
        padding:5,
        borderWidth: 1,
        flexDirection:'row',
        borderColor: 'lightgrey',
      },
      borderBox:{
        textAlign: 'center',
        justifyContent:'center',
        alignSelf:'center', 
        height:25,
        width:50,
        borderRadius:5,
        fontSize:14,
        padding:0,
        margin:5,
        backgroundColor : "#FFFFFF",
        borderWidth: 1,
        borderColor: 'lightgrey',
      },
      colorPickerStyle:{
        height:40,width:"100%",margin:0,padding:0,fontSize:12,
      },
      searchSection: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth:1,
        borderRadius:2,
        backgroundColor: '#fff',
        borderColor:"#8E8E8E",
        width:"95%",
    },
    pickerheadertext:{
      fontSize:12,color:'#000000',fontWeight:'bold'
    },
    input: {
        flex: 1,
        padding:8,
        borderColor:"#fff",
        backgroundColor: '#fff',
        color: '#424242',
    },
    darkGreenPanel:{
      height:40,
      alignItems: 'center',
      backgroundColor:'#006d50',
      padding:10, 
    },
    lightGreenPanel:{
      flex: 1,
      width:"95%",
      alignItems: 'center',
      backgroundColor:'#98c954',
      padding:10,
      marginBottom:5
    },
    lightGreenPanel20:{
      flex: 1,
      width:"93%",
      alignItems: 'center',
      backgroundColor:'#98c954',
      padding:10,
      marginBottom:5
    },
    popularitem: {
      flex: 1,
      height: 60,
      margin: 5,
     borderColor:"#8E8E8E",
      backgroundColor:'#fff',
      borderWidth:1,
    },
    giftItem:{
      flex: 1,
      height: 70,
      margin: 5,
      borderColor:"#ffffff",
      backgroundColor:'#fff',
      borderWidth:1,
    },
    discoveritem: {
      flex: 1,
      height: 220,
      margin: 5,
      borderWidth:1,
      borderColor:'lightgrey',
      backgroundColor:'#fff',
    },
    slideImage:{
      width: WINDOW_WIDTH,
      height: 180,
      flex: 1,
      alignSelf: "center",
    },
    panelView:{
      flexDirection: 'row',alignSelf:'center',alignItems:'center',padding:8
    },
    panelViewColumn:{
      flexDirection: 'column',paddingTop:5,paddingBottom:5,paddingLeft:10,paddingRight:10
    },
    searchhomeSection: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth:1,
        backgroundColor: '#fff',
        borderColor:"#8E8E8E",
        width:"94%",
        marginTop:10
    },
    popularitemImage:{
      height:'100%',width:"100%"
    },
    giftitemImage:{
      height:70,width:"100%",resizeMode:'contain'
    },
    discoveritemImage:{
     height:200,flex:1,margin:15
    },
    searchpadding:{
      flexDirection:'row'
    },
    p5:{
      padding:5,
      justifyContent:'center',
      alignSelf:'center'
    },
    slideImageView:{
      margin:10,flex: 1,marginTop:10,marginBottom:10,width:"100%"
    },
    pannelText:{
      color:'#fff', fontSize:14,fontStyle:'italic'
    },
    italicblacktext:{
      color:'#000000', fontSize:14,fontStyle:'italic'
    },
    middlBannerView:{
      flex: 1,marginTop:10,width:"95%",marginBottom:5,height:200 
    },
    middlebannerImage:{
      width:"100%",height:200
    },
    gridView:{
      flex: 1,marginTop:5,width:"95%"
    },
    gridViewStyle:{
      flex: 1,alignItems: 'center'
    },
    homeBottomView:{
      flexDirection: 'row', backgroundColor:'#ffffff',marginTop:10, paddingBottom: 10
    },
    socialView:{
      flexDirection: 'column',flex:1,alignSelf:'center',alignItems:'center', backgroundColor:'#ffffff'
    },
    socialImage:{
      width:45,flex:1, height:45 , resizeMode:'contain'
    },
    signinFormView:{
      flex:1,paddingBottom:20,backgroundColor:'#ffffff',width:'100%',marginTop:10
    },
    radioText:{color: '#98c954',fontSize: 14,textAlign:'left',justifyContent:'flex-start',padding:0,margin:0,
    alignSelf:'flex-start',fontWeight:'bold'},
    radiblackText:{
      color: '#000000',fontSize: 14,textAlign:'left',justifyContent:'flex-start',
    alignSelf:'flex-start'
    },
    h74:{
      height:70
    },
    signinTitle:{
      flex:1,backgroundColor:'#ffffff',paddingTop:10
    },
    slideMenuView:{
      flexDirection:'row',paddingLeft:15, justifyContent: 'center',
      alignItems: 'center',
    },
    slideMenuCenterLogoView:{
      flex:1,alignSelf:'center',alignItems:'center'
    },
    centerLogoImage:{
      width:"50%",height:30
    },
    searchViewGreenBack:{
      flexDirection: 'row',padding: 10,backgroundColor:'#006d50'
    },
    slideMenuItemView:{
      flexDirection: 'row',backgroundColor:'#ffffff',padding:12,borderBottomColor:'white',borderBottomWidth:1
    },
     listitemBlock: {
      fontSize:14,
      color:'#000000',
      flexDirection:'row',
      backgroundColor:'#ffffff',
      borderBottomWidth:1,
      padding:15,
      borderBottomColor:'lightgrey'
    },
    listitemNameParent: {
      flex:1,
      color:'#000000',
      fontWeight:'normal',
      fontSize: 14,
    },
    listitemNameChild:{
      flex:1,
      color:'#000000',
      fontWeight:'normal',
      fontSize: 12,
    },
    settingView:{
      backgroundColor:'#ffffff',flex:1,padding:10
    },
    productItemBlock:{
      flex:1,
      flexDirection: 'column',
      backgroundColor:'#ffffff',
      borderRightWidth:1,
      borderRightColor:'lightgrey',
      borderBottomColor:'lightgrey',
      borderBottomWidth:1,
      padding:5,
    },
    heartImageView:{
      alignSelf:'flex-end',marginRight:5,marginTop:5
    },
    productImageView:{
      width:"100%",height:150,alignSelf:'center',padding:5
    },
    productImageStyle:{
      alignSelf:'center',width:'100%',height:150,padding:5
    },
    productItemStyle:{
      flex: 1,flexDirection: 'column',alignSelf:'flex-start',margin:5
    },
    cartView:{
      flex:1 ,padding:5,backgroundColor:'#ffffff',flexDirection:'column'
    },
    p5CenterRow:{
      margin:10,alignSelf:'center',flexDirection:'row',justifyContent:'center',alignItems:'center'
    },
    m2Left:{
      flex: 1,flexDirection: 'column',alignSelf:'flex-start' ,marginLeft:2
    },
    indicatoreViewsTyle:{
      width:'100%',height:'100%',justifyContent:'center',position:'absolute',backgroundColor:'transparent'
    },
    modalBackground: {
      flex: 1,
      alignItems: 'center',
      flexDirection: 'column',
      justifyContent: 'space-around',
      backgroundColor: '#00000040'
    },
    roundedHeartImageStyle:{
      width:40,height:40
    },
    detailpagePriceStyle:{
      flex: 1,flexDirection: 'column',alignSelf:'flex-end',alignContent:'flex-end'
    },
    RatingStarViewStyle:{
      flex:1,flexDirection:'row',justifyContent:'center'
    },
    popupdialogView:{
      width:'100%',height:'100%'
    },
    popupDialogStyle:{
      width:'100%',height:200,flexDirection:'column'
    },
    dialogStyle:{position: 'absolute', bottom: 20 },
    dialogTitleTextStyle:{
      fontSize: 14,fontWeight :'bold',color:'#000000',textAlign:'center',justifyContent:'center',padding:5
    },
    selectqtycontainer:{
      margin:20,
      flexDirection:'row',
      padding:5,
      backgroundColor:'#ffffff',
      borderRadius:20,
      borderWidth: 2,
      color:'#000000',
      borderColor: 'lightgrey'
    },
    crossText:{
      alignSelf:'flex-end',
      color: '#000000',                        
      fontWeight: 'normal',                
      fontSize: 12,
      padding: 2,
      textDecorationLine: 'line-through', textDecorationStyle: 'solid'
  },
  addbagbutton:{
    marginRight:10,
    width:'75%',
    paddingTop:5,
    paddingBottom:5,
    justifyContent:'center',
    backgroundColor:'#FF7D25',
    borderRadius:20,
    borderWidth: 1,
    borderColor: '#FF7D25'
  },
  checkoutHeaderStyle:{
    flex:1,flexDirection:'row',backgroundColor:'#097456',height:'100%'
  },
  pannelcontainer   : {
    backgroundColor: '#ffffff',
    overflow:'hidden',
    borderBottomWidth:1,
    borderBottomColor:'#8E8E8E',
  },
  listImageView:{
    width:"100%",flex:1,alignItems:'center',height:150
  },
  listImage:{
    width:"100%",height:150,padding:5,resizeMode:'center'
  },
  listRow:{flex: 1,flexDirection: 'row',alignSelf:'flex-start'},
  allMinusImagesize:{
    alignSelf:'center',width:22,height:22
  },
  lightGreenPanelAcc:{
    flex: 1,
    width:"100%",
    backgroundColor:'#98c954',
    padding:10,
    flexDirection:'row',
  },
  ViewAcc:{
    backgroundColor:'#ffffff',flexDirection:'column',justifyContent:'center',padding:5
  },
  ratecardView:{borderWidth:1 ,marginTop:15,flexDirection:'column', borderColor:'#8E8E8E' ,backgroundColor:'#f3f3f3', padding:10,margin:10},
  revieworderView:{borderWidth:1,flexDirection:'column', borderColor:'#8E8E8E' ,backgroundColor:'#f3f3f3',marginBottom:10},
  checkout1LogoView:{marginTop:10, alignSelf:'center',width:Dimensions.get('window').width-50},
  checkout1logoImage:{width:Dimensions.get('window').width-50,resizeMode:'contain', alignSelf:'center'},
  cardviewStyle:{
    flexDirection:'row',flex:1,width:Dimensions.get('window').width-50
  },
  wishlistimageSize:{
    height:21,
    width:25,
  },
  map: {
    height: 400,
    marginTop: 80
  },
  buttonFilter:{ 
    flex:1,padding:10,borderColor:'#98c954',borderWidth:1,alignSelf:'center'
  },
  buttonCartList:{
    flex:1,padding:5,borderColor:'#98c954',borderWidth:1,alignSelf:'center',borderBottomWidth:1,borderTopWidth:1
  },
  viewinfilter:{
    height:'100%',width:1,backgroundColor:'#98c954',padding:0,alignSelf:'center'
  },
  sliderstylefilter:{
    paddingTop:20,alignSelf:'center',flexDirection:'column'
  }
  }),
  footer: {
    width:'100%',
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
    paddingHorizontal: 10,
    paddingVertical: 5,
},
footerButton: {
    flexDirection: 'row',
    marginLeft: 15,
},
footerText: {
    fontSize: 16,
    color: '#FFF',
    textAlign: 'center',
},

};
  export default Constant;